from flask import Blueprint
from flask import render_template,abort
from flask_login import current_user
from flask_admin import BaseView,expose



adminbp = Blueprint('enteroadmin', __name__)

from . import views
@adminbp.before_request
def restrict_bp_to_admins():
    if not current_user.is_authenticated() and not current_user.administrator:
        abort(403)


