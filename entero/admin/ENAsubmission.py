import xml.etree.cElementTree as ET
import os, subprocess, sys, re
import hashlib
import glob
import datetime
import ftplib
from dateutil import parser

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem1 in elem:
            indent(elem1, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem

def getTaxonIds(strain_data) :
    if not os.path.isfile('names.dmp') :
        subprocess.Popen('wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz'.split()).wait()
        subprocess.Popen('tar -xzf taxdump.tar.gz'.split()).wait()
        for fname in ('taxdump.tar.gz', 'citations.dmp', 'delnodes.dmp', 'division.dmp', 'gencode.dmp', 'merged.dmp', 'nodes.dmp', 'gc.prt', 'readme.txt') :
            os.unlink(fname)
    species = { d['species']:'' for d, t in strain_data if d.get('species', None) is not None }
    if len(species) :
        with open('names.dmp') as fin :
            for line in fin :
                part = line.split('\t')
                if part[2] in species :
                    species[part[2]] = part[0]
                    if len([v for v in species.values() if len(v) == 0]) == 0 :
                        break
    for d, t in strain_data :
        d['TAXON_ID'] = species[d['species']]
        #print "Best assembly", d.get('id')
        #print "Best assembly", d.get('best_assembly')
        #print "Tx: ",d['TAXON_ID']
        #print "==============================================="
    return strain_data
def convertData(strain_data, addtional_data ): 
    strain_data = getTaxonIds(strain_data)
    
    dataset = {}
    for d, t in strain_data :
        center = d['contact']
        release_date = str(d['release_date'])
        if release_date <= str(datetime.datetime.now()) :
            release_date = 'released'
        nd = {'TAXON_ID': d['TAXON_ID'], \
              'SAMPLE':d['barcode'], \
              'SCIENTIFIC_NAME': d['species'], \
              'DESCRIPTION':'Whole genome sequencing data uploaded by EnteroBase user', \
              'READ_FILES':t['read_location'].encode().split(','), \
              'broker_name' : 'EnteroBase', \
              'strain': d['strain'], \
              '__TRACE__' : t['barcode']
            }
        if d.get('serotype', None) :
            nd.update({'serovar':d['serotype']})
        if d.get('subspecies', None) :
            nd.update({'subspecies':d['subspecies']})
        if d.get('country', None) :
            location = { v:i for i, v in enumerate([d['city'], d['admin2'], d['admin1'], d['country']]) if v }
            geographic_location = ', '.join(sorted(location.keys(), key=lambda k:-location[k]))
            nd.update({'geographic location':geographic_location})
        if d.get('collection_year', None) :
            collection_date = '-'.join([ '{0:02}'.format(v) for v in [d['collection_year'], d['collection_month'], d['collection_date']] if v ])
            nd.update({'collection_date':collection_date})
        if d.get('source_type', None) or d.get('source_niche', None) or d.get('source_details', None) :
            sources = { v:i for i, v in enumerate([d['source_details'], d['source_niche'], d['source_type']]) if v }
            isolation_source = ', '.join(sorted(sources.keys(), key=lambda k:-sources[k]))
            nd.update({'isolation_source':isolation_source})
        ad_data = addtional_data.get(d['barcode'], {})
        for k, v in ad_data.items() :
            if k != 'Barcode' and v :
                nd[k] = v
        if (center, release_date) in dataset :
            dataset[(center, release_date)].append(nd)
        else :
            dataset[(center, release_date)] = [nd]
    return dataset


class ENAsubmission(object) :
    def __init__(self, local_folder, center_name, release_date, title, abstract, ftp_user_name, ftp_password) :
        self.folder = str(datetime.datetime.now())[:10] + '_' + local_folder
        if not os.path.exists(self.folder) :
            os.makedirs(self.folder)
        
        self.center_name = center_name
        self.study_refname = hashlib.md5(title).hexdigest()
        self.refname = str(datetime.datetime.now())

        self.xml_files = { tag: None for tag in ["study", "sample", "experiment", "run"]}
        self.submission_file = "submission.xml"
        self.ftp_user_name = ftp_user_name
        self.ftp_password = ftp_password
        
        self.title = title
        self.abstract = abstract
        self.release_date = release_date
        
    def run(self, dataset, mode) :
        old_folder = os.path.abspath(os.curdir)
        os.chdir(self.folder)
        
        title, abstract, release_date = self.title, self.abstract, self.release_date
        self.create_checksums_file(dataset)
        # upload reads to ftp server
        self.upload_reads_to_ena_ftp(dataset)
    
        self.accessions = {}
        toSubmission = False
        for ite in xrange(1000) :
            # generate study xml
            self.xml_files['study'] = self.study_xml(title, abstract)
            # generate sample xml
            self.xml_files['sample'] = self.sample_xml(dataset)
            # generate experiment xml
            self.xml_files['experiment'] = self.experiment_xml(dataset,library_strategy='WGS',library_source='GENOMIC',library_selection='RANDOM')
            # generate run xml
            self.xml_files['run'] = self.run_xml(dataset)
            # generate submission xml
            submission = self.submission_xml(release_date=release_date)
            # submit to test
            if self.run_curl_command('test') :
                if mode == 'test' :
                    print 'Everything is fine. '
                    os.chdir(old_folder) 
                    break
                elif mode == 'force' :
                    ret = 'y'
                else :
                    ret = raw_input('Everything is fine. Press Y and return for a real submission. Anything else to exit.')
                if ret.lower().startswith('y') :
                    toSubmission = True
                break
        if not toSubmission :
            print 'Program ends. '
            return toSubmission #sys.exit(0)

        self.accessions = {}
        for ite in xrange(1000) :
            # generate study xml
            self.xml_files['study'] = self.study_xml(title, abstract)
            # generate sample xml
            self.xml_files['sample'] = self.sample_xml(dataset)
            # generate experiment xml
            self.xml_files['experiment'] = self.experiment_xml(dataset,library_strategy='WGS',library_source='GENOMIC',library_selection='RANDOM')
            # generate run xml
            self.xml_files['run'] = self.run_xml(dataset)
            # generate submission xml
            submission = self.submission_xml(release_date=release_date)
            # submit to test
            if self.run_curl_command('prod') :
                self.accessions['study'] = self.accessions.pop(self.study_refname, '')
                os.chdir(old_folder)                
                return self.accessions

    def create_checksums_file(self, dataset):
        try:
            print "creating checksums....."
            for data in dataset :
                for fname in data['READ_FILES'] :
                    (SeqDir,seqFileName) = os.path.split(fname)
                    md5File = seqFileName + '.md5'
                    if not os.path.isfile(md5File) or os.path.getsize(md5File) < 30 :
                        with open(md5File, 'wa') as md5out :
                            checksum_value = hashlib.md5(open(fname, 'rb').read()).hexdigest()
                            md5out.write(checksum_value+" "+seqFileName+"\n")
            print "created checksums files. "
        except IOError:
            print "ERROR: Something has gone wrong with creating the checksums file.  Please check and re-submit."
            
            sys.exit()

    def upload_reads_to_ena_ftp(self, dataset) :
        refname = str(self.study_refname)
        try:
            print "\nconnecting to ftp.webin.ebi.ac.uk...."
            ftp = ftplib.FTP("webin.ebi.ac.uk", self.ftp_user_name, self.ftp_password)
        except IOError:
            print(ftp.lastErrorText())
            print "ERROR: could not connect to the ftp server.  Please check your login details."
            sys.exit()

        print "\nSuccess!\n"

        try:
            if refname in ftp.nlst() :
                print refname, "directory in ftp already exists....OK no problem..."
            else:
                print "\nmaking new directory in ftp called", refname
                ftp.mkd(refname)
            ftp.cwd(refname)

            #print "\nuploading checksum file to ENA ftp server in the", refname, "directory\n"
            #ftp.storbinary('STOR '+refname+'_checksums.md5', open(self.checksum_file, 'rb'))     # send the file
        except IOError:
            print "ERROR: could not find the checksum file. "
            sys.exit()

        print "Now uploading all the data to ENA ftp server in the", refname, "directory\n"

        for i in range(0,3):
            resultedFiles = []
            try:
                for data in dataset :
                    for file in data['READ_FILES'] :
                        (SeqDir,seqFileName) = os.path.split(file)
                        # if the sample has already been uploaded, check the file size number. If its the same as the dir file size then move on otherwise upload it again.
                        for fn, fn2 in ((seqFileName, file), (seqFileName+'.md5', seqFileName+'.md5')) :
                            if fn in ftp.nlst():
                                fileSize_in_ftp = ftp.size(fn)
                                fileSize_in_dir = os.path.getsize(str(fn2))
                                if fileSize_in_dir != fileSize_in_ftp:
                                    print fn+" is uploaded but not all of it so uploading it again now to ENA ftp server.\n"
                                    ftp.storbinary('STOR '+fn, open(fn2, 'rb'))
                                else:
                                    print fn + " has already been uploaded.\n"
                            else:
                                print "uploading", fn2, "to ENA ftp server.\n"
                                ftp.storbinary('STOR '+fn, open(fn2, 'rb'))
                break
            except:
                print "Something went wrong with ftp, lets try again...."
        else:
            print "Oops! something has gone wrong while uploading data to the ENA ftp server!"
            sys.exit()
        ftp.quit()
        return resultedFiles

    def sample_xml(self, dataset):
        sample_set = ET.Element('SAMPLE_SET')

        dataset2 = [ data for data in dataset if data['SAMPLE'] not in self.accessions ]
        if len(dataset2) == 0 :
            return None

        for data in dataset2 :
            sample = ET.SubElement(sample_set, 'SAMPLE', alias=data['SAMPLE'], center_name=self.center_name)
            title = ET.SubElement(sample, "TITLE").text = data['SAMPLE']
            sample_name = ET.SubElement(sample, "SAMPLE_NAME")
            taxon = ET.SubElement(sample_name, "TAXON_ID").text = data['TAXON_ID']
            if len(data.get('SCIENTIFIC_NAME', '')) :
                scientific = ET.SubElement(sample_name, "SCIENTIFIC_NAME").text = data['SCIENTIFIC_NAME']
            if len(data.get('DESCRIPTION', '')) :
                desc = ET.SubElement(sample, "DESCRIPTION").text = data['DESCRIPTION']

            metadata_fields = set([ k for k, v in data.items() if (not k.startswith('__')) and v and len(str(v)) > 0 ]) - set(['SAMPLE','TAXON_ID','SCIENTIFIC_NAME','DESCRIPTION', 'READ_FILES'])
            if len(metadata_fields) :
                sample_attributes = ET.SubElement(sample, "SAMPLE_ATTRIBUTES")
                for field in metadata_fields :
                    value = data[field]
                    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
                    tag = ET.SubElement(sample_attribute, "TAG").text = str(field)
                    value = ET.SubElement(sample_attribute, "VALUE").text = ''.join(value)

        # print sample_id_and_data
        indent(sample_set)
        # create tree
        tree = ET.ElementTree(sample_set)

        # out_dirput to outfile
        with open('sample.xml', 'w') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print "\nSuccessfully created sample.xml file\n"
        return 'sample.xml'

    def experiment_xml(self, dataset, library_strategy, library_source, library_selection): #, instrument_model):
        # set the root element
        experiment_set = ET.Element('EXPERIMENT_SET')

        dataset2 = [ data for data in dataset if data['__TRACE__']+'-EXP' not in self.accessions ]
        if len(dataset2) == 0 :
            return None
        for data in dataset2 :
            experiment = ET.SubElement(experiment_set, 'EXPERIMENT', alias=data['__TRACE__']+'-EXP', center_name=self.center_name)
            study_ref = ET.SubElement(experiment, "STUDY_REF", refname=self.study_refname, refcenter=self.center_name)

            design = ET.SubElement(experiment, "DESIGN")
            design_description = ET.SubElement(design, "DESIGN_DESCRIPTION")
            sample_descriptor = ET.SubElement(design, 'SAMPLE_DESCRIPTOR', refname=data['SAMPLE'], refcenter=self.center_name)
            library_descriptor = ET.SubElement(design, "LIBRARY_DESCRIPTOR")
            library_name = ET.SubElement(library_descriptor, "LIBRARY_NAME")
            library_strategy = ET.SubElement(library_descriptor, "LIBRARY_STRATEGY").text = str(library_strategy)
            library_source = ET.SubElement(library_descriptor, "LIBRARY_SOURCE").text = str(library_source)
            library_selection = ET.SubElement(library_descriptor, "LIBRARY_SELECTION").text = str(library_selection)
            library_layout_dir = ET.SubElement(library_descriptor, "LIBRARY_LAYOUT")
            #indent
            paired = ET.SubElement(library_layout_dir, "PAIRED" )
            # dedent
            platform = ET.SubElement(experiment, "PLATFORM")
            illumina = ET.SubElement(platform, "ILLUMINA")

            #indent
            instrument_model = ET.SubElement(illumina, "INSTRUMENT_MODEL").text = 'unspecified'

            # dedent
            processing = ET.SubElement(experiment, "PROCESSING")

        # use the indent function to indent the xml file
        indent(experiment_set)
        # create tree
        tree = ET.ElementTree(experiment_set)

        # out_dirput to outfile
        with open('experiment.xml', 'w') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print "\nSuccessfully created experiment.xml file\n"
        return 'experiment.xml'

    def run_xml(self, dataset):
        run_set = ET.Element('RUN_SET')
        checksums = {}
        for fname in glob.glob('*.md5') :
            with open(fname, 'rb') as f :
                for line in f :
                    part = line.strip().split()
                    checksums[part[1]] = part[0]

        dataset2 = [ data for data in dataset if data['__TRACE__'] not in self.accessions ]
        if len(dataset2) == 0 :
            return None

        for data in dataset2 :
            run = ET.SubElement(run_set, 'RUN', alias=data['__TRACE__'], center_name=self.center_name, run_center=self.center_name)
            experiment_ref = ET.SubElement(run, 'EXPERIMENT_REF', refname=data['__TRACE__']+'-EXP')
            data_block = ET.SubElement(run, 'DATA_BLOCK')
            files = ET.SubElement(data_block, 'FILES')
            try:
                for fname in data['READ_FILES'] :
                    (SeqDir,seqFileName) = os.path.split(fname)
                    file = ET.SubElement(files, 'FILE', checksum=checksums[seqFileName], checksum_method="MD5", filename=self.study_refname+"/"+seqFileName, filetype='fastq')
            except StopIteration :
                print "ERROR: no second read found for", data['SAMPLE']
                sys.exit()

        # create tree
        tree = ET.ElementTree(indent(run_set))
        # out_dirput to outfile
        with open('run.xml', 'w') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print "\nSuccessfully created run.xml file\n"
        return 'run.xml'

    def study_xml(self, title, abstract):
        if self.study_refname in self.accessions :
            return None
        study_set = ET.Element('STUDY_SET')
        study = ET.SubElement(study_set, 'STUDY', alias=self.study_refname, center_name=self.center_name)
        # indent
        descriptor = ET.SubElement(study, 'DESCRIPTOR')
        # indent
        # center_project_name = ET.SubElement(descriptor, 'CENTER_PROJECT_NAME').text = center_project_name
        study_title = ET.SubElement(descriptor, 'STUDY_TITLE').text = str(title)
        study_type = ET.SubElement(descriptor, 'STUDY_TYPE', existing_study_type="Whole Genome Sequencing")
        study_abstract = ET.SubElement(descriptor, 'STUDY_ABSTRACT').text = abstract

        # create tree
        tree = ET.ElementTree(indent(study_set))

        # out_dirput to outfile
        with open('study.xml', 'w') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print "\nSuccessfully created study.xml file\n"
        return 'study.xml'

    def submission_xml(self, release_date):
        xml_files = ["study", "sample", "experiment", "run"]

        submission_set = ET.Element('SUBMISSION_SET')
        submission = ET.SubElement(submission_set, 'SUBMISSION', alias=self.refname, center_name=self.center_name)
        actions = ET.SubElement(submission, "ACTIONS")
        for xml_tag, xml_file in self.xml_files.items() :
            if xml_file :
                action = ET.SubElement(actions, "ACTION")
                add = ET.SubElement(action, "ADD", source=xml_file, schema=xml_tag)

        # if a hold date is given until releasing publicly
        action = ET.SubElement(actions, "ACTION")
        if release_date != 'released':
            #change the format as it accepts only date not date time format
            #otherwise it will send back an error
            #K.M 3/6/2019
            dt = parser.parse(release_date)                  
            hold = ET.SubElement(action, "HOLD", HoldUntilDate=str(dt.date()))#release_date)
        # else if release is given, i.e. release immediatley to the public
        else :
            release = ET.SubElement(action, "RELEASE")
        # create tree
        tree = ET.ElementTree(indent(submission_set))

        # out_dirput to outfile
        with open(self.submission_file, 'w') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print "\nSuccessfully created submission.xml file\n"

    def run_curl_command(self, mode='test'):
        files = ' '.join([ '-F \"{0}=@{1}\"'.format(tag.upper(), fname) for tag, fname in self.xml_files.items() if fname ])
        if len(files) == 0 :
            print "\nNo new record to submit"
            return True
        
        #print files
        f=open(r"/home/khaled/temp/files.xml", 'w')
        f.write(files)
        f.close()          
        test_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://www-test.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(files) + self.ftp_user_name + "%20" + self.ftp_password
        prod_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(files) + self.ftp_user_name + "%20" + self.ftp_password
        cmd = prod_cmd if mode == 'prod' else test_cmd

        while True :
            print "\nRunning curl command....\n"
            receipt_file = 'receipt.xml'
            with open(receipt_file, 'w') as receipt_xml :
                p = subprocess.Popen(cmd, stdout=receipt_xml, shell=True, stderr=subprocess.PIPE)
                (curl_output, err) = p.communicate()
                print "Outputs: ", curl_output
                print "error: ", err
    
            receipt_xml = open(receipt_file, 'r').read()
            
            #f=open("/home/khaled/temp/s_EBI_SUB.xml", 'a')
            #line=("\n"+receipt_xml+"\n")
            #f.write(line)
            #f.close()                
            
          
            tree= ET.ElementTree(file=receipt_file) 
            receipt = tree.getroot()
            #check the success attribute
            if receipt.get('success')=='false':
            #if "success=\"false\"" in receipt_xml:
                #I am not sure about it so I leave it
                items = re.findall('<ERROR>.+alias:"([^"]+)".+The object being added already exists in the submission account with accession: "(.+)"', receipt_xml)
                if len(items) :
                    print 'WARNING: some items have been registered in ENA before'
                    self.accessions.update(dict(items))
                    return {"WARNING":"some items have been registered in ENA before'"}
                else:
                    #find the real error message from the receipt xml
                    #K.M 03/06/2019
                    errors=receipt.find('MESSAGES').find('ERROR').text
                    print "\nERROR: %s"%errors
                    print items
                    return {"error": errors}
                    #return receipt_xml
                    #sys.exit()
                    
            elif "success=\"true\"" in receipt_xml:
                items = re.findall(r' accession="(.+)" alias="(.+)" ', receipt_xml)
                self.accessions.update({ k:v for v, k in items })
                print "\nSUCCESS!"
                #print "\n NOTE: you must run the above curl command from the output dir which contains all the xml files."
                return {"error": False}
                #return True
            else :
                print "\nERROR: unknown"
                return {"Error":"unknown"}
                sys.exit()
