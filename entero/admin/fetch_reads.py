'''
This is all about fetching sort reads from various places and getting them into EnteroBase
'''
import paramiko
import shutil
import tarfile
from entero import dbhandle, app, get_database
from entero.ExtraFuncs.workspace import CustomView
from admin_tasks import build_or_update_workspace
import csv
from sqlalchemy import create_engine
import os, glob, sys
import subprocess
import re
import datetime, math
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.attributes import flag_modified


class ShortReadSet():
    
    def __init__(self, database, seq_suite_db = '/share_space/NOVELL/USERAREA/Warwick_Lab/scripts/seq_suite.db'):
        # BN database: 'postgresql+psycopg2://admin:ptybG1zQ6@137.205.51.112/SentericaWT' 
        self.seq_suite_db = seq_suite_db
        self.seq_conn = create_engine('sqlite:////%s' %seq_suite_db).connect()
        sesh = sessionmaker(bind=self.seq_conn)
        self.seq_sesh = sesh()
        self.database = database
        self.curr_db = get_database(database)
        self.read_loc = '/share_space/sanger_reads'
        self.user_id=0

    def locate_new_reads(self):
        '''
        Searches set directory for new reads and imports to seq suite db. 
        '''
        Strains = self.curr_db.models.Strains
        # Can't write sqllite to NFS mounts. Copy to a local disk, modify and copy back 
        self.seq_sesh.close()
        self.seq_conn.close()
        temp_db  = '/home/zhemin/test.db'
        shutil.copy(self.seq_suite_db, temp_db)
        temp_seq_conn = create_engine('sqlite:////%s' %temp_db).connect()
        sesh = sessionmaker(bind=temp_seq_conn)
        temp_sesh = sesh()
        
        records = {}
        for root, dirs, files in os.walk(self.read_loc):
            for file_name in files: 
                # Directory with reads exists, and file is a txt file. 
                read_folder = 'pathfind_%s' %file_name.replace('.info.txt','')
                if file_name.endswith('.txt') and os.path.exists(
                    os.path.join(root,'pathfind_%s' %file_name.replace('.info.txt',''))):
                    with open(os.path.join(root, file_name)) as f:
                        app.logger.info('Reading file %s' %file_name)
                        f.readline()
                        for line in f:
                            part = line.strip().split()
                            lane_name, sanger_sample, itemId, _ = part[:4]
                            strain_name = ' '.join(part[4:])
                            itemId = int(itemId[3:])
                            
                            # Check if both read paths exists:
                            # /share_space/sanger_reads/pathfind_22984_7/22984_7#24_1.fastq.gz
                            # /share_space/sanger_reads/pathfind_22984_7/22984_7#24_2.fastq.gz
                            read_1 = os.path.join(root, read_folder, '%s_1.fastq.gz' %lane_name)
                            read_2 = os.path.join(root, read_folder, '%s_2.fastq.gz' %lane_name)
                            
                            if os.path.exists(read_1) and os.path.exists(read_2):
                                records[sanger_sample] = [lane_name, sanger_sample, itemId, strain_name, read_1, read_2]
                            else: 
                                app.logger.error('Read files are MISSING for %s in %s' %(lane_name, file_name))
        # check consistency between sanger_id and itemId
        checking = temp_sesh.execute("select * from samples where sanger_sample_id in ('{0}')".format("','".join(records.keys())))
        idMap = {item_info['sanger_sample_id']:item_info for item_info in [dict(r) for r in checking.fetchall()] }
        itemIds = [ str(info['master_itemid']) for info in idMap.itervalues() ]
        genomes = self.curr_db.session.query(Strains)\
                .filter(Strains.custom_columns['6779'].astext.in_(itemIds), Strains.uberstrain > 0, Strains.uberstrain == Strains.id).all()
        genomeMap = {}
        for genome in genomes :
            itemId = str(genome.custom_columns['6779'])
            if itemId not in genomeMap :
                genomeMap[itemId] = [genome]
            else :
                genomeMap[itemId].append(genome)
        
        for sangerId, (lane_name, sanger_sample, itemId, strain_name, read_1, read_2) in records.iteritems() :
            seq_record = idMap.get(sangerId, {})
            if len(seq_record) == 0 :
                app.logger.error('Missing records: {0}: {1}, {2}'.format(sangerId, itemId, idMap.get(sangerId, 0)))
                continue
        
            if seq_record['seq_lane'] != lane_name :
                temp_sesh.execute("UPDATE samples SET seq_lane = '%s' WHERE sanger_sample_id = '%s'" 
                                      %(lane_name, sanger_sample))
            if seq_record['file_pointer'] != read_1 :
                temp_sesh.execute("UPDATE samples SET file_pointer = '%s' WHERE sanger_sample_id = '%s'" 
                                      %(read_1, sanger_sample))

            # Might as well fetch EnteroBase status
            # Create Item Tracker ID for custom column.
            #6779	ItemTracker ID and 6785
            #6686	Sanger Well
            #6685	ItemTracker Well
            #6684	ItemTracker Plate Barcode
            #6683	Sanger Plate Barcode
            #6689      OK (genome status; Cont, Failed QC, OK)
            #6690      TODO (other status; Reisolate, Forget )
            #14485     Plate Name
            res = genomeMap.get(str(seq_record['master_itemid']), [])
            #res = self.curr_db.session.query(Strains)\
                #.filter(Strains.custom_columns['6779'].astext == str(seq_record['master_itemid']), Strains.uberstrain > 0, Strains.uberstrain == Strains.id).all()
            if not res: 
                res = self.curr_db.session.query(Strains)\
                    .filter(Strains.strain.like('%' + seq_record['sample_name']), 
                            Strains.uberstrain > 0, 
                            Strains.uberstrain == Strains.id,
                            Strains.contact == 'M. Achtman').all()
                if res:
                    if not res[0].custom_columns: 
                        res[0].custom_columns = {} 
                    if not res[0].custom_columns.get('6779'):
                        xx = {str(res[0].id) :{'6779' : int(seq_record['master_itemid']),
                                                   '6785' : int(seq_record['master_itemid'])}}
                        ss = CustomView.update_custom_data('senterica', xx, user_id=self.user_id)
                if not res:
                    app.logger.error('record missing %s' %seq_record['sample_name'])
            if len(res) == 1:
                current_strain = res[0]
                genome_status = current_strain.custom_columns.get('6689')
                if genome_status and genome_status != seq_record['genome_status']:
                    temp_sesh.execute("UPDATE samples SET genome_status = '%s' WHERE sanger_sample_id = '%s'" %(genome_status, sanger_sample))
                genome_todo = current_strain.custom_columns.get('6690')
                if genome_todo and genome_todo != seq_record['genome_todo']:
                    temp_sesh.execute("UPDATE samples SET genome_todo = '%s' WHERE sanger_sample_id = '%s'" %(genome_todo, sanger_sample))
                plate_name = current_strain.custom_columns.get('14485')
                if plate_name and plate_name.startswith('Plate') and plate_name != seq_record['plate_name']:
                    temp_sesh.execute("UPDATE samples SET plate_name = '%s' WHERE sanger_sample_id = '%s'" %(plate_name, sanger_sample))
            #else:
                #temp_sesh.execute("UPDATE samples SET status = '%s' WHERE sanger_sample_id = '%s'" %('Sequenced', sanger_sample))
        temp_sesh.commit()
        # Can't write sqllite to NFS mounts. Copy to a local disk, modify and copy back        
        shutil.copyfile(temp_db, self.seq_suite_db)
        self.seq_conn = create_engine('sqlite:////%s' %self.seq_suite_db).connect()
        sesh = sessionmaker(bind=self.seq_conn)
        self.seq_sesh = sesh()

    def locate_new_reads2(self, folder):
        '''
        Searches set directory for new reads and imports to seq suite db. 
        '''
        Strains = self.curr_db.models.Strains
        # Can't write sqllite to NFS mounts. Copy to a local disk, modify and copy back 
        self.seq_sesh.close()
        self.seq_conn.close()
        
        records = {}
        for file_name in sorted(glob.glob(os.path.join(folder, '*.fastq.gz'))):
            base_name = os.path.basename(file_name)
            itemId = base_name.split('_', 1)[0][3:]
            key = itemId if base_name.find('.2.fastq.gz') < 0 else itemId+'-x'
            if key not in records :
                records[key] = [itemId, file_name]
            else :
                records[key].append(file_name)
        records = {k:v for k, v in records.iteritems() if len(v) == 3}
        return records

    def insert_into_enterobase2(self, workspace, data):
        try:
            Strains, Traces = self.curr_db.models.Strains, self.curr_db.models.Traces
            for itemKey, (itemtracker_id, read1, read2) in data.iteritems() :
                this_strain = self.create_new_record(itemtracker_id, record)
                self.fix_errors(this_strain, itemtracker_id, record)
                strain_ids.append(this_strain.id)
                self.curr_db.session.add(this_strain)
                self.curr_db.session.commit()
            app.logger.info(duplicated)
            app.logger.info(strain_ids)
            build_or_update_workspace(self.database, workspace, 76, strain_ids = strain_ids, append=True)
        except Exception as e:
            app.logger.exception("Error in insert_into_enterobase, error message: %s"%e.message)
            self.curr_db.rollback_close_session()

    def insert_into_enterobase(self, workspace):
        try:
            Strains, Traces = self.curr_db.models.Strains, self.curr_db.models.Traces
            duplicated = []
            strain_ids = []
            res = self.seq_sesh.execute("SELECT * FROM  samples WHERE file_pointer IS NOT NULL AND genome_status  IS NULL")
            results = [dict(r) for r in res]
            genomes = self.curr_db.session.query(Strains).filter(Strains.custom_columns['6779'].astext.in_([str(r['master_itemid']) for r in results]), Strains.uberstrain > 0).all()
            genomeMap = {}
            for genome in genomes :
                itemId = str(genome.custom_columns['6779'])
                if itemId not in genomeMap :
                    genomeMap[itemId] = [genome]
                else :
                    genomeMap[itemId].append(genome)
            for record in results:
                if record['sample_name'].startswith('SAR'):
                    app.logger.info('Skipping %s' %record['sample_name'])
                    continue
                itemtracker_id = record['master_itemid'] 
                this_strain = None
                res = genomeMap.get(str(itemtracker_id), [])
                # Handle multiple existing records
                if len(res) > 1:
                    app.logger.warning('Duplicated strain - %s' %len(res))
                    for rese in res:
                        duplicated.append(rese.strain)
                    continue
                # Create new record if none exists. otherwise fetch from results.
                if len(res) == 0 :
                    this_strain = self.create_new_record(itemtracker_id, record)
                if not this_strain:
                    this_strain = res[0]
                self.fix_errors(this_strain, itemtracker_id, record)
                strain_ids.append(this_strain.id)
                self.curr_db.session.add(this_strain)
                self.curr_db.session.commit()
            app.logger.info(duplicated)
            app.logger.info(strain_ids)
            build_or_update_workspace(self.database, workspace, 76, strain_ids = strain_ids, append=True)
        except Exception as e:
            app.logger.exception("Error in insert_into_enterobase, error message: %s"%e.message)
            self.curr_db.rollback_close_session()
        

    def fix_errors(self, strain, itemtracker_id, record):
        # Pair legacy records
        try:
            Traces = self.curr_db.models.Traces
            Strains = self.curr_db.models.Strains
            if strain.antigenic_formulas != record['antigenic_formula']:
                app.logger.warning("Antigenic formula mixup %s: %s is not %s " %(strain.strain, strain.antigenic_formulas, record['antigenic_formula']))
                strain.antigenic_formulas = record['antigenic_formula']
                self.curr_db.session.add(strain)
            legacy = self.curr_db.session.query(Strains)\
                    .filter(Strains.strain == strain.strain)\
                    .filter(Strains.uberstrain > 0)\
                    .filter(Strains.id != strain.id)\
                    .all()
            for leg in legacy:
                if leg.uberstrain != strain.id:
                    leg.uberstrain = strain.id
                    self.curr_db.session.add(leg)
            # Create Item Tracker ID for custom column.
            #6779	ItemTracker ID and 6785
            #6686	Sanger Well
            #6685	ItemTracker Well
            #6684	ItemTracker Plate Barcode
            #6683	Sanger Plate Barcode
            #6689      OK (genome status; Cont, Failed QC, OK)
            #6690      TODO (other status; Reisolate, Forget )
            #14485     Plate Name
            
            xx = {str(strain.id) :{'6779' : str(itemtracker_id),
                                       '6785' : str(itemtracker_id)}, }
            if strain.custom_columns.get('14485', '') == '' and str(record.get('plate_name', '')) != '' :
                xx[str(strain.id)]['14485'] = str(record['plate_name'])
            ss = CustomView.update_custom_data('senterica', xx, user_id=self.user_id)
            # Check file pointer
            traces = self.curr_db.session.query(Traces)\
                    .filter(Traces.strain_id == strain.id)\
                    .all()
            for trace in traces:
                if trace.read_location.startswith('{'):
                    trace.read_location = trace.read_location.replace('{', '').replace('}', '')
                    self.curr_db.session.add(trace)
        except Exception as e:
            app.logger.exception("Error in fix_errors, error message: %s"%e.message)
            self.curr_db.rollback_close_session()

        
    def create_new_record(self, itemtracker_id, record):
        try:
            Strains, Traces = self.curr_db.models.Strains, self.curr_db.models.Traces
            new_strain = Strains(strain = record['sample_name'], 
                                 contact = 'M. Achtman',
                                 owner = 76, 
                                 lastmodifiedby = 76,
                                 created = datetime.datetime.now(),
                                 release_date = datetime.datetime.now() +  datetime.timedelta(6*365/12),
                                 country = record['country'],
                                 serotype = record['serovar'],
                                 antigenic_formulas = record['antigenic_formula'],
                                 source_type = record['host'],
                                 source_details = record['host_details'],
                                 comment = record['source']
                                 )
            if record.get('collection_date'):
                new_strain.collection_year = int(record['collection_date'])
            self.curr_db.session.add(new_strain)
            self.curr_db.session.commit()
            # Pair legacy records
            legacy = self.curr_db.session.query(Strains).filter(Strains.strain == record['sample_name'], Strains.uberstrain > 0)\
                      .all()
            if len(legacy) > 0 :
                for leg in legacy:
                    if leg.uberstrain != new_strain.id:
                        leg.uberstrain = new_strain.id
                        self.curr_db.session.add(leg)
            # Create Item Tracker ID for custom column.
            xx = {str(new_strain.id) :{'6779' : str(itemtracker_id),
                                       '6785' : str(itemtracker_id)}}
            ss = CustomView.update_custom_data('senterica', xx, user_id=self.user_id)
            # Need to update with Read 1 and read 2 location
            new_traces = Traces(strain_id = new_strain.id, status= 'Uploaded',
                                seq_platform = 'ILLUMINA', seq_library = 'Paired',
                                read_location= '%s,%s' %(record['file_pointer'], 
                                                         record['file_pointer'].replace('1.fastq.gz', '2.fastq.gz')),
                                lastmodifiedby=76)
            new_strain.uberstrain = new_strain.id
            new_strain.barcode = self.curr_db.encode(new_strain.id, 'senterica', 'strains')
            self.curr_db.session.add(new_traces)
            self.curr_db.session.add(new_strain)
            self.curr_db.session.commit()
            new_traces.barcode = self.curr_db.encode(new_traces.id, 'senterica', 'traces')
            self.curr_db.session.add(new_traces)
            self.curr_db.session.commit()
            return new_strain
        except Exception as e:
            app.logger.exception("Error in create_new_record, error message: %s"%e.message)
            self.curr_db.rollback_close_session()
            return None

def insert_standard_into_database(database, data, user_id=69) :
    curr_db = get_database(database)
    Strains, Traces = curr_db.models.Strains, curr_db.models.Traces
    dp = curr_db.models.DataParam
    convert={k:v.lower().strip() for k, v in curr_db.session.query(dp.label, dp.name).filter(dp.tabname=='strains').all()}
    
    existing_reads = {rr for r in curr_db.session.query(Traces.read_location).filter(Traces.read_location != None).all() for rr in r[0].split(',')}
    try:
        for itemtracker_id, record in data.iteritems() : 
            # if read file present ?
            if sum([int(fn in existing_reads) for fn in record['read_files']]) > 0 :
                continue
            read_files = record.pop('read_files')
            record = {convert[k] : v for k, v in record.items()}
            record.update(dict(owner=user_id, 
                               lastmodifiedby=user_id, 
                               created = datetime.datetime.now(),
                               release_date = datetime.datetime.now(),
                               ))
            new_strain = Strains(**record)
            curr_db.session.add(new_strain)
            curr_db.session.commit()
                
            new_traces = Traces(strain_id = new_strain.id, status= 'Uploaded',
                                seq_platform = 'ILLUMINA', seq_library = 'Paired',
                                read_location= ','.join(read_files),
                                lastmodifiedby=user_id)
            curr_db.session.add(new_traces)
            new_strain.uberstrain = new_strain.id
            new_strain.barcode = curr_db.encode(new_strain.id, database, 'strains')
            curr_db.session.add(new_strain)
            curr_db.session.commit()
            new_traces.barcode = curr_db.encode(new_traces.id, database, 'traces')
            curr_db.session.add(new_traces)
            curr_db.session.commit()
    except Exception as e:
        app.logger.exception("Error in insert_into_enterobase, error message: %s"%e.message)
        self.curr_db.rollback_close_session()
    

def insert_into_enterobase(database, data, workspace=None):
    curr_db = get_database(database)
    Strains, Traces = curr_db.models.Strains, curr_db.models.Traces
    existing_reads = {rr for r in curr_db.session.query(Traces.read_location).filter(Traces.read_location != None).all() for rr in r[0].split(',')}
    try:
        for itemtracker_id, record in data.iteritems() : 
            # if read file present ?
            if sum([int(fn in existing_reads) for fn in record['read_files']]) > 0 :
                continue
            if database == 'senterica' :
                new_strain = Strains(strain = record['STRAIN'], 
                                     contact = 'M. Achtman',
                                     owner = 69, 
                                     lastmodifiedby = 69,
                                     created = datetime.datetime.now(),
                                     release_date = datetime.datetime.now(),
                                     country = record['COUNTRY'],
                                     serotype = record['SEROTYPE'],
                                     antigenic_formulas = record['ANTIGENIC_FORMULAS'],
                                     source_type = record['HOST'],
                                     collection_year = int(record['YEAR']) if record['YEAR'].isdigit() else None,
                                     source_details = record['HOST_DETAILS'],
                                     comment = record['SOURCE']
                                     )
                curr_db.session.add(new_strain)
                curr_db.session.commit()
                
                xx = {str(new_strain.id) :{'6779' : str(itemtracker_id),'6785' : str(itemtracker_id)}}
                if (not new_strain.custom_columns or new_strain.custom_columns.get('14485', '') == '') and str(record.get('PLATENAME', '')) != '' :
                    xx[str(new_strain.id)]['14485'] = str(record['PLATENAME'])
                    # I have added 0 as user_id as I think this method is called internally
                    # to be chaked deeper later
                    #should be checked
                ss = CustomView.update_custom_data(database, xx, user_id=0)
                
                legacy = curr_db.session.query(Strains)\
                    .filter(Strains.strain == new_strain.strain)\
                    .filter(Strains.uberstrain > 0)\
                    .filter(Strains.id != new_strain.id)\
                    .all()
                for leg in legacy:
                    if leg.uberstrain != new_strain.id:
                        leg.uberstrain = new_strain.id
                        curr_db.session.add(leg)
                
            elif database == 'helicobacter' :
                new_strain = Strains(strain = record['Strain'], 
                                     contact = 'M. Achtman',
                                     owner = 69, 
                                     lastmodifiedby = 69,
                                     created = datetime.datetime.now(),
                                     release_date = datetime.datetime.now(),
                                     country = record['Country'],
                                     continent = record['Continent'],
                                     admin2 = record['Region'],
                                     latitude = record['Latitude'] if record['Latitude'] != '' else None,
                                     citations = record['Publication'],
                                     longitude = record['Longitude'] if record['Longitude'] != '' else None,
                                     source_type = 'Human',
                                     source_niche = 'Human',
                                     source_details = record['Sex'] + '; ' + record['Patient Age'] + '; ' + record['ethnic origin'],
                                     comment = record['StrainSource'] +'; ' + record['Population'] +'; ' + record['DNANum']
                                     )
                curr_db.session.add(new_strain)
                curr_db.session.commit()
                
            new_traces = Traces(strain_id = new_strain.id, status= 'Uploaded',
                                seq_platform = 'ILLUMINA', seq_library = 'Paired',
                                read_location= ','.join(record['read_files']),
                                lastmodifiedby=69)
            curr_db.session.add(new_traces)
            new_strain.uberstrain = new_strain.id
            new_strain.barcode = curr_db.encode(new_strain.id, database, 'strains')
            curr_db.session.add(new_strain)
            curr_db.session.commit()
            new_traces.barcode = curr_db.encode(new_traces.id, database, 'traces')
            curr_db.session.add(new_traces)
            curr_db.session.commit()
    except Exception as e:
        app.logger.exception("Error in insert_into_enterobase, error message: %s"%e.message)
        self.curr_db.rollback_close_session()

def prepare_standard_reads(database, samplesheet, fastqfolder) :
    import pandas as pd
    # get all reads
    reads = {}
    for read in sorted(glob.glob(os.path.join(fastqfolder, '*.fastq.gz'))) :
        fname = os.path.basename(read)
        sname = fname.rsplit('_R', 1)[0]
        if sname != 'Undetermined' :
            if sname not in reads :
                reads[sname] = []
            reads[sname].append(read)

    data = pd.read_csv(samplesheet, sep='\t', header=0)
    read_prefix = data['prefix'].values
    data = data.drop('prefix', axis=1)
    columns = list(data)
    data = data.values
    
    metadata = {}
    for prefix, d in zip(read_prefix, data) :
        if prefix in reads :
            dd = {'read_files': reads.pop(prefix)}
            for c, p in zip(columns, d) :
                if p is not None and (not isinstance(p, float) or not math.isnan(p)) :
                    dd[c] = p
            metadata[prefix] = dd
        else :
            print '{0} is not found'.format(prefix)
    for key in reads :
        print '{0} does not have metadata'.format(key)
    return metadata

def prepare_sanger_reads(database, fastqfolder) :
    uri = {'senterica':'SentericaWT', 'helicobacter':'HpyloriBN7'}
    bn = create_engine('postgresql+psycopg2://admin:ptybG1zQ6@137.205.51.112/' + uri[database], client_encoding='latin1', echo=True).connect()
    data = {}
    for file_name in sorted(glob.glob(os.path.join(fastqfolder, '*.fastq.gz'))):
        base_name = os.path.basename(file_name)
        itemId = base_name.split('_', 1)[0][3:]
        key = itemId if base_name.find('.2.fastq.gz') < 0 else itemId+'-x'
        if key not in data :
            data[key] = [itemId, file_name]
        else :
            data[key].append(file_name)
    data = {k:{'itemtracker ID':v[0], 'read_files':v[1:]} for k, v in data.iteritems() if len(v) == 3}
    if database == 'senterica' :    
        BNdata = {r['ItemTracker ID']:r for r in [dict(r) for r in bn.execute('''select * from "ENTRYTABLE" where "ItemTracker ID" in ({0}) '''.format(','.join(["'{0}'".format(v['itemtracker ID']) for v in data.values()]))).fetchall()]}
    for itemId, d in data.iteritems() :
        bnd = BNdata[d['itemtracker ID']]
        d.update(bnd)
    # return data
    return data    

def prepare_nextseq_reads(database, samplesheet, fastqfolder) :
    uri = {'senterica':'SentericaWT', 'helicobacter':'HpyloriBN7'}
    bn = create_engine('postgresql+psycopg2://admin:ptybG1zQ6@137.205.51.112/' + uri[database]).connect()
    #bn_session = sessionmaker(bind=bn)
    
    # get all reads
    reads = {}
    for read in sorted(glob.glob(os.path.join(fastqfolder, '*.fastq.gz'))) :
        fname = os.path.basename(read)
        sname = os.path.basename(read).rsplit('_', 4)[0]
        if sname != 'Undetermined' :
            if sname not in reads :
                reads[sname] = []
            reads[sname].append(read)
    # compare to samplesheet
    data = {}
    with open(samplesheet) as fin :
        for line in fin :
            if line.startswith('Sample_ID') :
                break
        for line in fin :
            part = line.strip().split(',')
            if part[1] not in reads :
                print 'NOTE: reads for sample "{0}" is not found'.format(part[1])
                continue
            if database == 'senterica' :
                data.update({part[0]:{'itemtracker ID':part[0], 'STRAIN':part[1], 'read_files':reads[part[1]], 'cell':part[3]}})
            elif database == 'helicobacter' :
                if not part[0].startswith('DHP_') :
                    part[0] = part[0].replace('DHP', 'DHP_')
                
                data.update({part[0]:{'STRAIN':part[1], 'itemtracker ID':part[0], 'read_files':reads[part[1]], 'cell':part[3]}})
    # get info from BN
    if database == 'senterica' :    
        BNdata = {r['ItemTracker ID']:r for r in [dict(r) for r in bn.execute('''select * from "ENTRYTABLE" where "ItemTracker ID" in ({0}) '''.format(','.join(["'{0}'".format(k) for k in data]))).fetchall()]}
    else :
        BNdata = {r['DNANum']:r for r in [dict(r) for r in bn.execute('''select * from "ENTRYTABLE" where "DNANum" in ({0}) '''.format(','.join(["'{0}'".format(k) for k in data]))).fetchall()]}
        for rest in [k for k in data if k not in BNdata] :
            d = [dict(d) for d in bn.execute('''select * from "ENTRYTABLE" where "DNANum" like '%%{0},%%' or "DNANum" like '%%{0}' '''.format(rest)).fetchall()]
            if len(d) < 1 :
                print 'NOTE: Bionumerics data for sample "{0}" is not found'.format(rest)
                data.pop(rest, None)
                continue
            elif len(d) > 1 :
                d.sort(key=lambda dd:[len(dd['Strain']), len(dd['KEY'])], reverse=True)
                print ''
            BNdata[rest] = d[0]
    for itemId, d in data.iteritems() :
        bnd = BNdata[itemId]
        d.update(bnd)
    # return data
    return data

def update_BN(database, data) :
    uri = {'senterica':'SentericaWT'}
    bn = create_engine('postgresql+psycopg2://admin:ptybG1zQ6@137.205.51.112/' + uri[database]).connect()
    #bn_session = sessionmaker(bind=bn)
    
    bn.execute('''UPDATE "ENTRYTABLE" set "GENOME_STAT"='In EnteroBase' where "ItemTracker ID" in ({0}) '''.format(','.join(["'{0}'".format(k) for k, d in data.iteritems() if d['GENOME_STAT'] not in {'Genome Complete', 'Needs Repeating', 'In EnteroBase'} ])))
    bn.execute('commit')


def fetch_from_standard_sheet(database, samplesheet, fastqfolder, user_id) :
    data = prepare_standard_reads(database, samplesheet, fastqfolder)
    insert_standard_into_database(database, data, user_id)
    

def fetch_from_nextseq(database, samplesheet, fastqfolder) :
    data = prepare_nextseq_reads(database, samplesheet, fastqfolder)
    insert_into_enterobase(database, data)
    update_BN(database, data)
    
def fetch_from_sanger2(database, folder):
    data = prepare_sanger_reads('senterica', folder)
    insert_into_enterobase('senterica', data)


def fetch_from_sanger(database, password):
    read_data = ShortReadSet(database)
    # Files should already be in set location. /share_space/sanger_reads
    #read_data.fetch_from_sanger(password)
    read_data.locate_new_reads()
    read_data.insert_into_enterobase('WT-Achtman Salmonella')
    
## TODO: This is now broken with use of seq_suite. 
#def fetch_from_nextseq(database):
    #read_data = ShortReadSet(database)
    #read_data.get_nextseq_reads()
    #read_data.update_from_BN()
    #read_data.find_legacy_records()
    #read_data.insert_into_enterobase('WT-Achtman Salmonella')
    
    ## TODO: This is now broken with use of seq_suite. 
    #def fetch_from_sanger(self, password=None):
        #database = self.database
        #if password:

            #Strains = self.curr_db .models.Strains
            #Traces = self.curr_db .models.Traces
            #server = 'sftpsrv.sanger.ac.uk'
            #user = 'n-f.alikhan'
            #t = paramiko.Transport((server,22))
            #t.connect(username =user, password=password)
            #sftp = paramiko.SFTPClient.from_transport(t)
            #sftp.chdir('data4463')
            #doc_list = {}
            #strain_ids = [] 
            #final_dir = '/share_space/sanger_reads/'
            #temp_dir = '/scratch/sanger_temp/'
            #for dat in sftp.listdir('.'):
                #local_path = os.path.join(temp_dir, dat)
                #if not os.path.exists(local_path):
                    #try:
                        ##pass 
                        #app.logger.info('downloading %s ' %dat)
                        ##sftp.get(dat, local_path) 
                    #except:
                        #pass
            #duplicated = [] 
            #line_count = 0 
            #strain_count = 0
            #strain_names = [] 
            #not_found =  [] 
            #for root, dirs, files in os.walk(os.path.dirname(temp_dir)):
                #for dat in files:
                    #if dat.endswith('.txt'):
                        #if not os.path.exists(os.path.join(final_dir, dat)): 
                            #shutil.copy(os.path.join(root, dat), os.path.join(final_dir, dat))
                        #with open(os.path.join(final_dir, dat), 'r') as f:
                            #for line in f.readlines()[1:]:
                                ## Extract reads
                                #if dat.startswith('pathfind') and dat.endswith('tar.gz'):
                                    #tarball = os.path.join(root, dat)
                                    #if not os.path.isdir(os.path.join(final_dir, os.path.basename(tarball.split('.')[0]))):
                                        #try:
                                            #tfile = tarfile.open(tarball, 'r:gz')
                                            #tfile.extractall(final_dir)
                                        #except:
                                            #pass

                                ## # Create Database records
                                #dirty = False
                                ## Get Strain name 
                                #line_count += 1 
                                #strain_name = re.search('.+MA\.(.+)$', line).group(1).strip()
                                #strain_names.append(strain_name)
                                
                                ## TODO: This is now broken with use of seq_suite. 
                                # sample_sheet='/share_space/next_seq/180228_NB501709_0045_AH2VTFBGX5/run_22_sam_sheet.csv', 
                                # source='/share_space/next_seq/180228_NB501709_0045_AH2VTFBGX5/Data/Intensities/BaseCalls', 
                                #def get_nextseq_reads(self, 
                                #sample_sheet='/share_space/next_seq/180228_NB501709_0045_AH2VTFBGX5/run_22_sam_sheet.csv', 
                                #source='/share_space/next_seq/180228_NB501709_0045_AH2VTFBGX5/Data/Intensities/BaseCalls',
                                #target='/share_space/local_reads/'):        
                                #import glob, subprocess
                                ## Read run info
                                #data_lines = open(sample_sheet, 'rb').read().split('[Data],,,,,,,\n')
                                #data_dict = csv.DictReader(data_lines[1].split('\n'), delimiter=',')
                                #item_ids = [j['Sample_ID'] for j in data_dict]
                                #self.records = {}
                                ## Read short read files
                                #for fname in sorted(glob.glob(os.path.join(source, '*.gz'))) :
                                #prefix = re.split(r'_S\d+_L', fname.rsplit('/', 1)[-1])[0]
                                #if prefix in item_ids :
                                #if self.records.get(prefix):
                                #self.records[prefix].append( fname )
                                #else:
                                #self.records[prefix] = [ fname ]
                            
                                ## Concatenate short read files
                                #if not os.path.isdir(target) :
                                #os.makedirs(target)
                                #for item_id, reads in self.records.iteritems() :
                                #if len(reads) and len(reads) % 2 == 0 :
                                #target_files = [ os.path.join(target, '{0}_summarised_R{1}.fastq.gz'.format(item_id, r_id)) for r_id in range(1,3) ]
                                #for reads, target_file in zip((reads[:-1:2], reads[1::2]), target_files) :
                                #if not os.path.isfile(target_file) :
                                #subprocess.Popen('cat {0} > {1}'.format(' '.join(reads), target_file), shell=True)
                                #self.records[item_id] = {'reads':target_files, 'metadata':{}, 'substrains':[]}
                                #else :
                                #assert len(reads), 'record "{0}" does not find corresponding short reads.'.format(item_id)
                                #assert len(reads) % 2 == 0, 'record "{0}" has odd number of read files. Is this an SE library?'.format(item_id)
if __name__ == '__main__' :
    fetch_from_nextseq(*sys.argv[1:])