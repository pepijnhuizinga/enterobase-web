UploadReadsValidationGrid.prototype = Object.create(StrainValidationGrid.prototype);
UploadReadsValidationGrid.prototype.constructor = UploadReadsValidationGrid;

//constructor
function UploadReadsValidationGrid(name,config,species){
	//call super constructor
    StrainValidationGrid.call(this,name,config,species);   
    var self = this;
    this.editMode=true;
	//a custom validator for the reads
	this.addCustomCellValidator("Accession",function(colName,rowID){
		var rowIndex = this.getRowIndex(rowID);
		var colIndex= this.getColumnIndex(colName);
		//check basics e.g. platform,library present and correct
		if (!this.validateCompoundField(colName,rowID)){
            msg = "One or more values in this field are incorrect";
			return this.updateErrors(colName,rowID,msg);
        }
		
		//Next check that reads are correctly paired
		var ids = this.comp_values.getRowIDs("Accession",rowID);
		var pair =0;
		var insertSize=0;
		var msg="";
		for (var index in ids){
			row = this.comp_values.getRow("Accession",rowID,ids[index]);
			if (row[2]=="Paired" && pair ===0){
				pair=1;
				insertSize=row[3];
				continue;
			}
			if (row[2]!=="Paired" && pair===1){
				msg = "Paired reads not adjacent";
				break;
			}
			if (row[2]==="Paired" && pair===1){
				pair=0;
				if (row[3]!==insertSize){
					msg = "Paired reads have different insert size";
					break;
				}	
				insertSize=0;
			}
		}
		if (pair ===1){
			msg = "Reads are not paired"
		}
		if(msg){
			return this.updateErrors(colName,rowID,msg);
		}
		this.updateErrors(colName,rowID,msg);
		//check that no reads have been duplicated
		this.duplicateRecord();
		//this.refreshGrid();
    }); 
};

UploadReadsValidationGrid.prototype.addUploadedReads = function(){ 
    new_data=[];
    var fileList = this.val_params["accession"]["values"];
	
	if (fileList.length ===0){
		Enterobase.modalAlert("There are no uploaded reads to process"+
		" Please upload reads first","Warning",false);
		return;
	}
    var filesUsed = [];
	//will look for these anywhere in the file and pair those with similar names
    var endings = [['_R1','_R2'],['_pe1','_pe2']];
    for (var index in fileList){
		//file already paired - continue
		if (filesUsed.indexOf(fileList[index])>-1){
			continue;
		}
		//go through all endings (patterns)
		for (var index2 in endings){
			var suffix = endings[index2]
			var i = fileList[index].indexOf(suffix[0])
			//if found then check fo mate 
			if (i >0){
				read1 = fileList[index];
				read2 = read1.replace(suffix[0],suffix[1])
				listIndex = fileList.indexOf(read2);
				//put both read 1 and read 2 into grid and add to used files
				if (listIndex > -1){
					new_data.push({id:this.idTotal,values:this.default_values})
					this.comp_values.addValue("Accession",this.idTotal,"accession",read1,true);
					this.comp_values.addValue("Accession",this.idTotal,"accession",read2,true);
					this.idTotal++;
					filesUsed.push(read1);
					filesUsed.push(read2);		    
				}		
			}
		}
    }
    //now add all singletons
    for (var index in fileList){
		if (filesUsed.indexOf(fileList[index])>-1){
			continue;
		}
		new_data.push({id:this.idTotal,values:this.default_values})
		var rowID = this.comp_values.addValue("Accession",this.idTotal,"accession",fileList[index],true);
		this.comp_values.setValue("Accession",this.idTotal,rowID,"seq_library","Single");
		this.idTotal++;
    }
	//load the data into the grid
    this.load({data:new_data});
    for (var r=0;r<this.getRowCount();r++){
		var rowID = this.getRowId(r);
		var c = this.getColumnIndex("Accession");
		this.setValueAt(r,c,this.comp_values.getDisplayValue("Accession",rowID),false);
    }
    //this.newDataLoaded();

}

UploadReadsValidationGrid.prototype.setReadDefaults = function(){
    var comp = this.comp_values;
    var cn = 'Accession'
    for	(var i =0;i<this.getRowCount();i++){
		var rowID = this.getRowId(i);	
		var ids= comp.getRowIDs(cn,rowID);
		var type= 'Single';
		if (ids.length ==2){
			type = 'Paired';
		}
		for (var i2 in ids){
			if(!comp.getValue(cn,rowID,ids[i2],'seq_platform')){
				comp.setValue(cn,rowID,ids[i2],'seq_platform',"ILLUMINA");	
			}
			if(!comp.getValue(cn,rowID,ids[i2],'seq_library')){
				comp.setValue(cn,rowID,ids[i2],'seq_library',type);	
			}
		}
    }
}


UploadReadsValidationGrid.prototype.processMetaData = function(callback){
    var self = this;
	//change the labels for accession
    this.labels_to_names["Read Files"] = "Accession";
    this.labels_to_names['Read File'] = "accession";
    for (var i in this.col_list){
        var col = this.col_list[i];
        if (col['name']==="Accession"){
            col['label']="Read Files";
			col['editable']=true;     
        }
		//Sample and Project are not required
		if (col['name']==='Sample' || col['name'] === 'Project'){
			col['required']=false;
		}
    }

    //hide the columns
    this.hiddenColumns['Sample']=true;
    this.hiddenColumns['Project']=true;
	
    var exp = self.val_params['experiment_accession'];
	exp['required'] =false;
	var list = this.ignoreList;
	for (var index in list ){
		var column = self.val_params[list[index]];
		column['editable']= false;
		column['required']= false;
		column['default_value'] = 'ND';

	}
    
	//get the name of all the uploaded reads  
    Enterobase.call_restful_json("/upload/get_upload_reads_info"+"/"+this.species,"GET","",callback).done(function(data){      
             //data  is a list of file names
			 //update the validation parameters for accession (reads)
             read = self.val_params['accession'];
             read['values'] = data;
             read['label'] = "Read File";
             read['datatype'] = "combo"
             var msg = "Cannot find read name in uploads"
             read['values_msg'] = msg;
             callback("OK");
                     
     }); 
    
};

UploadReadsValidationGrid.prototype.validateStrains=function(){
    StrainValidationGrid.prototype.validateStrains.call(this);
    this.checkDuplicates= false;
    if (this.getCompleteRowCount()>0){
         this.duplicateRecord();
    }
    this.checkDuplicates = true;	
    this.errorListener(this.errorNumber,this.numberOfRowsWithErrors);
    this.refreshGrid();  
}

UploadReadsValidationGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
    var rowID = this.getRowId(rowIndex);
    var colName = this.getColumnName(colIndex);
   
    var self = this;
    var extramenu =[
        {
        name: 'Help',
        title: 'Help',
        fun: function () {
           window.open(Enterobase.wiki_url+"edit-strain-metadata-"+colName,'newwindow','width=700, height=500');
        }
		},      
        {
			name: 'Delete Row',
			title: 'Delete Row',
			fun: function () {
				self.removeRow(rowID);
			}
		},
		{
			name: 'Insert Row',
			title: 'Insert Row',
			fun: function () {    
				self.insertRowAfter(rowIndex,self.default_values);
			}
		}
      
    ];
    return extramenu;
};



UploadReadsValidationGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Metadata_Fields-"+msg,'newwindow','width=1000, height=600');
}

