BuddyGrid.prototype = Object.create(ValidationGrid.prototype);
BuddyGrid.prototype.constructor = ValidationGrid

function BuddyGrid(name,config,species){
   this.species=species
   this.user_to_id={};
   //set of ids removed
   this.ids_removed={};
   this.all_names=[];
   this.names_in_grid = {}
   ValidationGrid.call(this,name,config);
   this.editMode=true;
   
   
  
   var self = this;
   this.addCustomColumnHandler("remove",function(target,rowIndex,rowID){
		self.removeRow(rowID);
		self.refreshGrid();
		self.ids_removed[rowID]=true;
   
   });
   this.addExtraRenderer("remove",function(cell,value,rowID){
		$(cell).append($("<span  class ='glyphicon glyphicon-remove'></span>").css("cursor","pointer").attr("alt","delete buddy"))  
   });
   
	   
   
};

BuddyGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Buddies#"+msg,'newwindow','width=1000, height=600');
};




BuddyGrid.prototype.getDataToSubmit = function(){
    this.filter("");
	var buddy_list=[];
	var remove_list=[];
	for (var id in this.ids_removed){
		remove_list.push(id);
	}
    for (var n=0;n<this.getRowCount();n++){
        var rowID = this.getRowId(n);
		
        //new row to add
        var values = this.getRowValues(n);
		
		delete values['remove'];
		delete values['name'];
		for (var field in values){
			if (values[field] === true){
				values[field]="True";
			}
			else if (values[field]===false){
				values[field]="False";
			}
			
		}
		values['id']=rowID;
		buddy_list.push(values);
	}
    var retVal = null;
    try{
	 retVal =  JSON.stringify({"remove":remove_list,"buddy_list":buddy_list,"species":this.species}); 
    }catch(e){
	 console.log(e)
    }          
    return retVal;
}

BuddyGrid.prototype.loadNewData = function(data){
    if (data.length===0){
		return;
	}
	for (var index in data){
		this.names_in_grid[data[index]['name']]=true;
		
		
	}
	ValidationGrid.prototype.loadNewData.call(this,data,false,true);
}

BuddyGrid.prototype.addBuddy= function(name){
	var msg="";
	if (this.names_in_grid[name]){
		msg="Record aleady Exists";
	}
	var id = this.user_to_id[name];
	if (!id){
		msg="The user "+name+" cannot be found in the database";
	}
	if (msg){
		return msg
	}
	var values = {name:name}
	var index = this.getRowCount()-1;
	index = index<0?0:index;
	this.insertRowAfter(index,values,false,id);
	this.names_in_grid[name]=true;
	if (this.ids_removed[id]){
		delete this.ids_removed[id];
	}
	this.refreshGrid();
}



BuddyGrid.prototype.setMetaData= function(data){
	for (id in data['id_to_name']){
		var user = data['id_to_name'][id]
		this.user_to_id[user]=id
		this.all_names.push(user)
	}
    var columns= []

	var bud = {
		name:"name",
		label:"Buddy Name",
		datatype:"text",
		editable:false,
		display_order:1,
		required:true
	}
	columns.push(bud);
	var order =2
	for (index in data['all_permissions']){
		var permission = data['all_permissions'][index];
		if (permission==='shared_workspaces'){
			columns.push({
				name:permission,
				label:"Share Work Spaces",
				datatype:"combo",
				allow_multiple_values:true,
				vals:data['work_spaces'],
				display_order:order,
				editable:true
			});
		
		}
		else{
			columns.push({
				name:permission,
				label:permission.replace(/_/g," "),
				datatype:"boolean",
				display_order:order,
				editable:true,
				default_value:true
			});	
		}
		order++;     
	}
	
	columns.push({
		display_order:order,
		name:"remove",
		label:"<span  class ='glyphicon glyphicon-remove'></span>",
		editable:false,
		not_write:true
	});
	ValidationGrid.prototype.setMetaData.call(this,columns);
	this.columnWidths['name']=200;
	this.columnWidths['remove']=50;
}