/* global Enterobase */

//extend the class
SRAValidationGrid.prototype = Object.create(StrainValidationGrid.prototype);
SRAValidationGrid.prototype.constructor= SRAValidationGrid;

 
//constructor
function SRAValidationGrid(name,config,species){
	this.ignoreMetaData = ['Project','barcode','release_date','created','Sample','uberstrain',"checkselect",
		"edit_individual","seq_library","seq_insert","collection_year","collection_month","collection_date","collection_time",
		"sample_accession","secondary_sample_accession"];
    this.assemblyURL = Enterobase.assembly_url;
    this.nservURL = Enterobase.nserv_url
    this.download_url = '/upload/download';
    this.hasAssembly={};
    this.best_assembly={};
    this.assemblyCallback = function(){};
    this.cgmlstCallback = function(){};
    this.mlstCallback = function(){};
    this.uberStrain={};
    StrainValidationGrid.call(this,name,config,species);
	this.comp_values.setCustomDisplayValue('Accession',function(list){
		if (!list){
			return "";
		}	
		if (list.length>0){
			if (list[0][0].indexOf('traces') !== -1){
				if (list[0][1]){
					if (list[0][1].indexOf("7G") !== -1){
						return "MLST(Legacy)";
					}
					else{
						return "Uploaded Reads";
					}
				}
				
					
			}
			return list[0][0];		
		}
		else{
			return "Many("+list.number+")";
		}        
	});
	
	this.addCustomCellValidator("Accession",function(colName,rowID){
			return true;
	});
	
	this.addRowRemovedListener(function(rowID){
		var rowIndex = this.getRowIndex(rowID);
		var colIndex = this.getColumnIndex("Accession");
		var val1 = this.getValueAt(rowIndex,colIndex);  
		if (val1){
			 this.duplicateRecord(rowID);
		}
	});
	
	this.addExtraRenderer("Project",function(cell,value){
		if (!value || value == "NA"){
			$(cell).html("NA");
		}
		else{
			$(cell).html("<a style = 'whitespace:nowrap;' target='_blank' href = 'http://www.ncbi.nlm.nih.gov/sra/?term="+value+"'>"+value+"</a>");
		}
	});
	
	this.addExtraRenderer("Sample",function(cell,value){
		if (! value ||value=='NA' ){
			$(cell).html("NA")
		}
		else{
			$(cell).html("<a style = 'whitespace:nowrap;' target='_blank' href = 'http://www.ncbi.nlm.nih.gov/biosample/?term="+value+"'>"+value+"</a>");
		}
	});
	   

	   
};



SRAValidationGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Metadata_Fields-"+msg,'newwindow','width=1000, height=600');
}


SRAValidationGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
    var rowID = this.getRowId(rowIndex);

	
	//added by yue, 20190307
	//get not_editable map
	/* changed at 2019-03-18, changed as if login can see the ownder info
	var not_editableMapA={};//id:true
	var not_editableMapB={};//id:true
	
	//check is_downloadable 
	if(temp_load_data){
		for(var indexA in temp_load_data['strains']){
			not_editableMapA[ temp_load_data['strains'][indexA]['id'] ] = temp_load_data['strains'][indexA]['not_editable'];
		
		}
	}
	
	if(temp_query_data){
		for(var indexB in temp_query_data['strains']){
			not_editableMapB[ temp_query_data['strains'][indexB]['id'] ] = temp_query_data['strains'][indexB]['not_editable'];
		
		}
	}   
	*/
	//end    
    var not_assemble = true;
    if (this.editMode && !this.nonEditableRows[rowID] && this.hasAssembly[rowID]!=="Assembled" && this.hasAssembly[rowID]!=='Failed QC'){
	not_assemble=false;
    }
    var upload_disable = false;
    var colName = this.getColumnName(colIndex);
    if (! this.rowsWithChanges[rowID]){
	upload_disable = true;
    }
    var self = this;
    var extramenu =[
        {
        name: 'Help',
        title: 'Help',
        fun: function () {
           window.open(Enterobase.wiki_url+"Metadata_Fields-"+colName,'newwindow','width=1000, height=600');
        }
	},
        
        
        {
        name: 'Upload Changes in Row',
        title: 'Upload Changes',
	disable:upload_disable,
        fun: function () {
            self.updateAllChangedRecords(false,rowID);
        }
    },
    {
        name: 'Assemble Strain',
        title: 'Assemble Strain',
	disable:not_assemble,
        fun: function () {
	        
	    self.sendRecordsForAssembly([rowID],self.displayMessage);
        }
    },
	{
        name: 'Show Ownership',
        title: 'Show Ownership',
	
        fun: function () {
	        
	    self.showOwnerShip(rowIndex)
        }
    }
	
      
    ];
    
    
    /*
    if( not_editableMapA[rowID] == true || not_editableMapB[rowID] == true ){
	extramenu[3]['disable']=true;
    }   
    */
 
    if( logged_in=="False" || logged_in=="false" || logged_in==false){
	extramenu[3]['disable']=true;
    }   
    
    
    return extramenu;
};

SRAValidationGrid.prototype.showOwnerShip = function(row_index){
	var id = this.getRowId(row_index)
	var database=this.species;
	var promise=Enterobase.call_restful_json("/show_ownership_for_strain?strain_id="+id+"&database="+database,"GET","",
	function(msg){
		Enterobase.modalAlert("The call failed "+msg)
	});
	promise.done(function(data){
		if (data['failed']){
			Enterobase.modalAlert(data['msg'],"Warning")
		}
		else{
			var name = data.firstname+" "+data.lastname+"("+data.username+")";
			var html = "<label>Name</label><br>"+name+"<br>";
			html+="<label>Email</label><br>"+data.email+"<br>";
			html+="<label>Country</label><br>"+data.country+"<br>";
			html+="<label>Institution</label><br>"+data.institution+"<br>";
			html+="<label>Department</label><br>"+data.department+"<br>";
			Enterobase.modalAlert(html,"Information");
		}
	});
}


SRAValidationGrid.prototype.resetGrid = function(){
	this.hasAssembly={};
	this.best_assembly={};
	ValidationGrid.prototype.resetGrid.call(this);
	
	
}

SRAValidationGrid.prototype.sendRecordsForcgmlst= function(rowIDs,callback){
    var info="<ul>";
    var com = this.comp_values;
    var col = "Accession";
    var accNums= [];
    var barcodes = [];
    var cgmlstRows= [];
    if (rowIDs.length===0){
        callback("There are no strains selected. Please select strains by checking the left hand column.\
                   In addition, a strain can be assembled by right clicking on a row and selecting Assemble");
        return;
    }
    for (var index in rowIDs){
	var rowID = rowIDs[index];
	var accIDs =this.comp_values.getRowIDs(col,rowIDs[index]);
	var best_assembly = this.best_assembly[rowID]
	    var xd = this.getRowIndex(rowID );
	    var yd = this.getColumnIndex('strain');
	    var strain_name = this.getValueAt(xd,yd);
	    var xd = this.getRowIndex(rowID );
	    var yd = this.getColumnIndex('barcode');
	    var barcode = this.getValueAt(xd,yd);	    
	if (this.hasAssembly[rowID] !== "Assembled" ){
		info+=("<li>"+ strain_name+": has not been Assembled</li>");
		continue;
	    }
	    if (this.nonEditableRows[rowID]){
		info+=("<li>"+strain_name+": You do not have permission to edit this strain</li>");
		continue;
	    }    
	    else{
		info+=("<li>"+strain_name+": Has been sent for cgMLST (v1)</li>");
	        cgmlstRows.push(rowID);
	        accNums.push(best_assembly);
		barcodes.push(barcode);
	    }	    	
    }
    if (accNums.length !==0){
	var toSend = {'accessions':accNums.join(','),'priority':0,'barcodes':barcodes.join(',')}
	//callback only on error
	this.cgmlstCallback(cgmlstRows);
	Enterobase.call_restful(this.cgmlstURL+"/"+this.species,"PUT",toSend,callback);
	info+="</ul><br>"+accNums.length +" records have been sent - You can check progress  under 'Show my Jobs' ";
    }
    else{
	info+="</ul><br>No records have been sent"
	
    }
   
    callback(info);
    return accNums.length;


};
SRAValidationGrid.prototype.sendRecordsToNserv =function(rowIDs,scheme,schemeLabel,callback){
    var info="<ul>";
    
    var assemblies= [];
    var mlstRows= [];
    if (rowIDs.length===0){
	callback("There are no strains selected. Please select strains by checking the left hand column");
	return;
    }
    for (var index in rowIDs){
		var rowID = rowIDs[index];
		var best_assembly = this.best_assembly[rowID];
		var xd = this.getRowIndex(rowID );
		var yd = this.getColumnIndex('strain');
		var strain_name = this.getValueAt(xd,yd);
			    
		if (this.hasAssembly[rowID] !== "Assembled" ){
			info+=("<li>"+ strain_name+": has not been sent - There is no valid assembly</li>");
			continue;
		}
		if (this.nonEditableRows[rowID]){
			info+=("<li>"+strain_name+": You do not have permission to send this strain</li>");
			continue;
		}    
		else{
			info+=("<li>"+strain_name+": Has been sent for "+schemeLabel+"</li>");
			mlstRows.push(rowID);
			assemblies.push(best_assembly);			
		}	    	
    }
    if (assemblies.length !==0){
		var toSend = {priority:0,assembly_ids:assemblies.join(","),scheme:scheme};	
		Enterobase.call_restful(this.nservURL+"/"+this.species,"PUT",toSend,callback);
		info+="</ul><br>"+assemblies.length +" records have been sent - You can check progress  under 'Show my Jobs' ";
    }
    else{
		info+="</ul><br>No records have been sent";
    }
    callback(info);
    return assemblies.length;
};




SRAValidationGrid.prototype.sendRecordsForMLST= function(rowIDs,callback){
    var info="<ul>";  
    var assemblies= [];
    if (rowIDs.length===0){
        callback("There are no strains selected. Please select strains by checking the left hand column.\
                   In addition, a strain can be assembled by right clicking on a row and selecting Assemble");
        return;
    }
    for (var index in rowIDs){
		var rowID = rowIDs[index];
		var best_assembly = this.best_assembly[rowID]
		var xd = this.getRowIndex(rowID );
		var yd = this.getColumnIndex('strain');
		var strain_name = this.getValueAt(xd,yd);
			    
		if (this.hasAssembly[rowID] !== "Assembled" ){
			info+=("<li>"+ strain_name+": has not been Assembled</li>");
			continue;
		}
		if (this.nonEditableRows[rowID]){
			info+=("<li>"+strain_name+": You do not have permission to edit this strain</li>");
			continue;
		}    
		else{
			info+=("<li>"+strain_name+": Has been sent for MLST </li>");
			assemblies.push(best_assembly);			
		}	    	
    }
    if (assemblies.length !==0){
		var toSend = {'priority':0,"assembly_ids":assemblies.join(",")}
		this.mlstCallback(mlstRows);
		Enterobase.call_restful(this.mlstURL+"/"+this.species,"PUT",toSend,callback);
		info+="</ul><br>"+assemblies.length +" records have been sent - You can check progress  under 'Show my Jobs' ";
    }
    else{
		info+="</ul><br>No records have been sent"
    }
    callback(info);
    return assemblies.length;
}

SRAValidationGrid.prototype.sendRecordsForST = function(rowIDs,scheme_name,callback){
    var info="<ul>";
    var com = this.comp_values;
    var col = "Accession";
    var assemblies= [];
    var mlstRows= [];
    if (rowIDs.length===0){
        callback("There are no strains selected. Please select strains by checking the left hand column.\
                   In addition, a strain can be assembled by right clicking on a row and selecting Assemble");
        return;
    }
    for (var index in rowIDs){
		var rowID = rowIDs[index];
		var best_assembly = this.best_assembly[rowID]
		var xd = this.getRowIndex(rowID );
		var yd = this.getColumnIndex('strain');
		var strain_name = this.getValueAt(xd,yd);
			    
		if (this.hasAssembly[rowID] !== "Assembled" ){
			info+=("<li>"+ strain_name+": has not been Assembled</li>");
			continue;
		}
		if (this.nonEditableRows[rowID]){
			info+=("<li>"+strain_name+": You do not have permission to edit this strain</li>");
			continue;
		}    
		else{
			info+=("<li>"+strain_name+": Has been sent for MLST </li>");
			mlstRows.push(rowID);
			assemblies.push(best_assembly);			
		}	    	
    }
    if (assemblies.length !==0){
		var toSend = {'priority':0,"assembly_ids":assemblies.join(",")}
		this.mlstCallback(mlstRows);
		Enterobase.call_restful(this.stapiURL+"/"+this.species+"/" +  scheme_name,"PUT",toSend,callback);
		info+="</ul><br>"+assemblies.length +" records have been sent - You can check progress  under 'Show my Jobs' ";
    }
    else{
		info+="</ul><br>No records have been sent"
    }
    callback(info);
    return assemblies.length;
}



/*
Method to call assemblies 
rowIDs - a list of row IDs to send - all accessions associated with that row will be sent
callback - method to call when the ajax is successful or there is an error requires a text message
*/
SRAValidationGrid.prototype.sendRecordsForAssembly= function(rowIDs,callback){
    var com = this.comp_values;
    var col = "Accession";
    var accNums= [];
    var rows_to_assemble= [];
    var info="<ul>";
    if (rowIDs.length===0){
        callback("There are no strains selected. Please select strains by checking the left hand column.\
                   In addition, a strain can be assembled by right clicking on a row and selecting Assemble");
        return;
    }
    for (var index in rowIDs){
	var rowID = rowIDs[index];
	var accIDs =this.comp_values.getRowIDs(col,rowIDs[index]);
	for (var i in accIDs){
	    var accNum = com.getValue(col,rowID,accIDs[i],"accession");
	    var platform = com.getValue(col,rowID,accIDs[i],"seq_platform");
	    if (platform !== 'ILLUMINA'){
		info+=("<li>"+accNum+":"+platform+" reads cannot be assembled yet</li>");
		continue;
	    }
	    	   
	   // if (this.hasAssembly[rowID]=="Assembled" || this.hasAssembly[rowID]=="Failed QC"){
		//info+=("<li>"+accNum+": has already been Assembled</li>")
		//continue;
	   // }
	    if (this.nonEditableRows[rowID]){
		info+=("<li>"+accNum+": You do not have permission to edit this strain</li>");
		continue;
	    }
	    if (this.hasAssembly[rowID]=="Queued"){
		info+=("<li>"+accNum+": is queued but has now been given higher priority</li>");
		
	    }
	    
	    
	    else{
		info+=("<li>"+accNum+": Has been sent for Assembly</li>");
	    }
 	    accNums.push(accNum);
	    rows_to_assemble.push(rowID);
	   
	    this.hasAssembly[rowID]="Queued";
	    	
	}
    }
    if (accNums.length !==0){
	 var toSend = {'accessions':accNums.join(','),'priority':0}
	//callback only on error
	this.assemblyCallback(rows_to_assemble);
	Enterobase.call_restful(this.assemblyURL+"/"+this.species,"PUT",toSend,callback);
	info+="</ul><br>"+accNums.length +" assemblies have been sent - You can check progress by going to 'Show My Jobs'"
    }
    else{
	info+="</ul><br>No assemblies have been sent"
	
    }   
    callback(info);
    return accNums.length;
}

/*
Overides base method gives owner user name and email address 
rowID - The id of the row that the user was attempting to edit
*/
SRAValidationGrid.prototype.rowNotEditableWarning= function(rowID){
    var info = this.extraRowInfo[rowID];
    if (!this.editMode){
	Enterobase.modalAlert("You have to be in edit mode in order to edit the metadata. Please check the edit mode box above (you have to be logged in for it to appear)"
	,"Warning",false);
	return;
    }
    
    var msg = "You do not have permission to edit this record or run an asembly job on it. ";
    if (info){
	msg += "The record is owned by "+info+ ". If you want to edit the record, you can ask\
		them to make you buddy or you can ask for curation privelages from an administrator."    
    }
    else{
	msg+= "The record has no owner please contact the administrator to obtain ownership."
    }
    Enterobase.modalAlert(msg,"Warning",false)
}




SRAValidationGrid.prototype.processMetaData =  function(callback){
	var acc = this.val_params['accession'];
	if (acc){
		acc['required']=false;	
	}
	var exp = this.val_params['experiment_accession'];
	if (exp){
		exp['required']=false;
	}
  
   callback("OK");
};
/*
Method to retreive metadata from NCBI from accession numbers in the grid
link - url to restful call
species - the species the accession belongs to
callback the function called when the request has finished (or returned an error) 
username and password are optional
*/
SRAValidationGrid.prototype.getDataFromAccession = function(callback){
    //loop through all accesionNumbers and link to rowId a
   
    var listAc=[];
    var self = this;
    for(var n=0;n<this.getRowCount();n++){
        var rowID = this.getRowId(n);
        var compIDs = this.comp_values.getRowIDs("Accession",rowID);
        if (compIDs.length===0){
            continue;
        }
        for (var i in list){
            var acc = this.comp_values.getRow("Accession",rowID,list[i])[0];
            if (this.isValueValid('accession',acc) !== ""){
                continue;
            }
           
            listAc.push(acc);
        }
        
    }
    if (listAc.length === 0){
	callback("No Valid Accession Numbers in The Table")
	return;
    
    }
    toSend = {
            recs:"accessions",
	    non_verified:false,
	    query:listAc.join(),
	    no_edit:false
    
    
    }
    
    Enterobase.call_restful_json("/get_strains"+"/"+this.species,"POST",toSend,callback).done(function(data){
        
        self.loadNewData(data,false,true);
        callback("OK");
        });
    
};


SRAValidationGrid.prototype.loadNewData= function(data,validate,use_own_ids){
    
    ValidationGrid.prototype.loadNewData.call(this,data,validate,use_own_ids);
	for (index in data){
		var ass = data[index]['assembly_status'];
		if  (ass){
			this.hasAssembly[data[index]['id']]= ass;
		
		}
		var best_assembly = data[index]['best_assembly'];
		if  (best_assembly){
			this.best_assembly[data[index]['id']] = best_assembly;
		}
	}

}

SRAValidationGrid.prototype.isGenomePrivate=function(rowID){
	var rowIndex = this.getRowIndex(rowID);
	var releaseDate = this.getValueAt(rowIndex,this.getColumnIndex("release_date"));
	if (new Date(releaseDate).getTime() > new Date().getTime() && !this.isDownloadable[rowID]){
		return true;		
	}
	return false;
}


SRAValidationGrid.prototype.downloadAssemblies= function(Ids){
	var downloaded = false;
	var msg = "<ul>";
	var downloaded =0;
	for (index in Ids){
		var rowID =Ids[index];
		var rowIndex = this.getRowIndex(rowID);
	    var best_ass = this.best_assembly[rowID];
		
		var strainName = this.getValueAt(rowIndex,this.getColumnIndex("strain"));
	    if (this.hasAssembly[rowID] === "Assembled" || this.hasAssembly[rowID] ==="Failed QC"|| this.hasAssembly[rowID] ==="Poor Quality"|| this.hasAssembly[rowID] ==="Contaminated"){
			var releaseDate = this.getValueAt(rowIndex,this.getColumnIndex("release_date"));
		      //if (new Date(releaseDate).getTime() > new Date().getTime() && this.nonEditableRows[rowID]){
			if (new Date(releaseDate).getTime() > new Date().getTime() && ! this.isDownloadable[rowID]){
				msg+="<li>"+strainName+" is private until "+releaseDate+"</li>";
			}
			
			else if (! this.isDownloadable[rowID]){
			msg+="<li>You are not allowed to download this strain</li>";
			}
			else{
				Enterobase.downloadURL(this.download_url+"?assembly_id="+best_ass+"&database="+this.species);
				downloaded++;
			}					
		}
		else{
			msg+="<li>"+strainName+" Has no Assembly</li>";
		}
	}
	msg+="<ul>"
	msg = downloaded + " assemblies have been downloaded<br>"+msg;
	Enterobase.modalAlert(msg);
}
		
SRAValidationGrid.prototype.getAllMetadata=function(){
	var isolates=[];
	var fields=['Collection Date'];
	
	var data = this.dataUnfiltered?this.dataUnfiltered:this.data;
	for (var name in this.val_params){
				 if (this.ignoreMetaData.indexOf(name) === -1 && !this.val_params[name]['not_write']){
						fields.push(this.val_params[name]['label']);
				 }
		
	}
	var field_has_value={};
	field_has_value['Longitude']=true;
	field_has_value['Latitude']=true;
	
	for (var i in this.data){
				var vals = this.data[i]['values'];
				
				var id =  this.data[i].id;
				var isolate = {"StrainID":id};
				for (var name in vals){
						if (this.ignoreMetaData.indexOf(name) !== -1 ){
								continue;
						}
						if (this.compound_columns[name]){
								
								if (name ==="Collection Date"){
									 var list= this.comp_values.getRow("Collection Date",id,1);
									 var dateText="";
									 if (list){                                     
										if (list[2]){dateText+=list[0]+"-"+list[1]+"-"+list[2]}
										else if (list[1]){dateText+=list[0]+"-"+list[1]+"-01"}
										else if (list[0]){dateText+=list[0]+"-01-01"}
									 }
									 if (dateText){
										field_has_value['Collection Date']=true;
									 
									 }
									 isolate["Collection Date"]=dateText;
									 continue;
								}
								var c_vals =this.comp_values.getRowAsDict(name,id,1);
								for (var v in c_vals){
								   var label = 	this.val_params[v]['label'];
								   if (c_vals[v]){
									field_has_value[label]=true
								   }
								   isolate[label]=c_vals[v]     
								}
						}
						else if (!this.val_params[name]['not_write']){
								var label =this.val_params[name]['label'];
								if (vals[name]){
									field_has_value[label]=vals[name];
								}
								isolate[label]=vals[name];
								
						}
				}
											   
				isolates.push(isolate);
		}
		var non_empty_fields=[];
		for (var field in field_has_value){
			non_empty_fields.push(field)
		}
		
		return {metadata:isolates,fields:non_empty_fields};

}	




/**
*Data loaded into the table will be synced with the database. Database records are 
*retrieved according to the accession number in the grid and new IDs assigned to
*records in the grid from the the database record IDs. Any rows in the grid which
*are not editable by the user are removed as well as rows where the accession number
*does not match any database records. Any data in a editable column which differs
*from the database will be classed as edited and shown in yellow
*@param {string} The current experiment that is linked to the strain data
*@param {callback} callback - The method called when the table has been synced. the method
* should accept the experiment data which needs to be loaded into the right grid
*/

SRAValidationGrid.prototype.syncGridWithDatabase= function(current_experiment,callback){
    //loop through all accesion Numbers and link to index
    var nonEditableItemId=[];
    var self = this;
    var accToRow= {};
    var rowToAcc= {};
    var listAc = [] ;
    if ( current_experiment=="error" || typeof self.data[0]== 'undefined' ) {
	//  No Data loaded at all
	Enterobase.modalAlert("No metadata could be loaded from your file, please check. There maybe blank rows");				
	return ; 	  
    
    }
    var div = $("<div>");
	
	div.append($("<div style='font-size:16px'> <b>Please a column in your file to associate records on the webpage</b></div><br>"));
	
	div.append($("<div> <label>Column Name</label></div>"));
	div.append($('<select class = "table-input" id ="field-select" ><option value ="Accession">Accession (SRSXXX)</option><option value ="barcode">Barcode (Strain)</option><label>Upload Metadata</label>'));
	div.append($("<div> Column headers must match EnteroBase's standard column headers exactly<br>Valid values include 'Barcode' or 'Accession'</div><br>"));

	div.dialog({
	    autoOpen: true,
	    modal: false,
	    title: "Choose metadata field ",
	    width: 'auto',
	    height:'auto',
	    buttons: {
		 "OK": function () {//start here, end at line928
			var ids = [] ;
			var fieldname = $('#field-select').find(":selected").val();
			if (typeof self.data[0].values[fieldname.toLowerCase()] == 'undefined' && typeof self.data[0].values[fieldname] == 'undefined' ) {
				     //  Key value is missing. 
				    Enterobase.modalAlert("Can not find a column labelled " + fieldname + "  in your file, please check.");				
				    return ; 			
			}
			for(var n=0;n<self.getRowCount();n++){
			    var rowID = self.getRowId(n);    
			    
			    if (self.isCompoundColumn(fieldname)){
				var list = self.comp_values.getRowIDs(fieldname,rowID);	
				if (list.length===0){
				     //  No valid values. Error message. Abort.
				    Enterobase.modalAlert("No valid data for  " + fieldname + " exists in your file, please check you have data.");				
				    return ; 
				}
			    
				for (var i in list){
					var acc = self.comp_values.getRow(fieldname,rowID,list[i])[0];
					if (self.isValueValid(fieldname.toLowerCase(),acc) !== ""){
					    continue;
					}
					rowToAcc[n]=acc;
					accToRow[acc]=n;
					listAc.push(acc);
			    }			    
				// Fieldname is not a compound value (like accessions) and does not resolve. Just fetch the value directly. 
			    }else {
			        acc = self.getRowValues(n)[fieldname]
				if (self.isValueValid(fieldname,acc) !== ""){
				    
				    continue;
				}								
				rowToAcc[n]=acc;
				accToRow[acc]=n;					    
				listAc.push(acc);			    
			    
			    }

			}	

    //send query to get strain and experimental data from database
    if (fieldname === 'Accession') {
    var toSend = {
		database:self.species,
		strain_query:listAc.join(","),
		strain_query_type:"accessions",
		experiment:current_experiment,
		show_non_assemblies:"true",
		show_substrains:"true"
    };
    }else { 
    var toSend = {
		database:self.species,
		strain_query: 'strains.' + fieldname.toLowerCase() + " IN  ('" + listAc.join("','") +"')" ,
		experiment:current_experiment,
		show_non_assemblies:"true",
		show_substrains:"true"
    };    
    
    }
	
    Enterobase.call_restful_json(Enterobase.main_query_url,"POST",toSend,callback).done(function(all_data){
		data = all_data['strains'];
		var acc_to_index= {};
		//link accession the index of the new data
		for (var index in data){
			var record = data[index];
			for (var ind in record[fieldname]){
			        if (self.isCompoundColumn(fieldname)){
				     var acc = record[fieldname][ind][fieldname.toLowerCase() ];
				}else{
				     var acc = record[fieldname];				
				}
				acc_to_index[acc]=index;
			}
			
			
		}
		//remove rows if do not exist in the incoming data or are not editable
		var rows_to_remove=[];
		var total_rows= self.getRowCount();
		var tempMessage="";
		
		var totalRow=total_rows;
		
		for(var n=0;n<totalRow;n++){
			//get the incoming row index
			 var index = acc_to_index[rowToAcc[n]];
			 var oldID = self.getRowId(n);
			 if (!index || data[index]['not_editable']){
				rows_to_remove.push(oldID)
				total_rows--;//which is used in delete selft.indexId
				tempMessage="barcode is "+ rowToAcc[n];
				if(data.length>0 && data.length>=index){
					tempMessage=tempMessage+", strain name is "+data[index]['strain'];
				}
				
				nonEditableItemId.push(tempMessage);
			 }
		}
		for (var index in rows_to_remove){
			self.removeRow(rows_to_remove[index]);
		}
		
		//need to change all the IDS for the incoming data and update compvalues 
		var new_comp_dict={};
		var old_comp_dict= self.comp_values.dict;
		
		for (var col in old_comp_dict){
			new_comp_dict[col]={};
			new_comp_dict[col]['fields'] = old_comp_dict[col]['fields'];
		}
		
		/*
		for(var n=0;n<total_rows;n++){
			//get the new ID
			 var index = acc_to_index[rowToAcc[n]];
			 
			var oldID=self.getRowId(n);
			delete self.IDIndex[oldID];
			var record = data[index];
			try{
			       var newID = parseInt(record['id']);
			       self.data[n].id=newID;
			       self.IDIndex[newID]=self.data[n];
			       for (col in old_comp_dict){
				      new_comp_dict[col][newID]=old_comp_dict[col][oldID];
			       }
			}catch(e){
			       console.log(e);
			}
			 
			 

		}*/
		
		for(var n=0;n<total_rows;n++){
			var oldID=self.getRowId(n);//e.g., 1
			var filenameTemp=self.getRowValues(n)[fieldname];//e.g.,SAL_VA2395AA
			var dataIndex=acc_to_index[filenameTemp];			
			delete self.IDIndex[oldID];
			var record = data[dataIndex];
			try{
			       var newID = parseInt(record['id']);
			       self.data[n].id=newID;
			       self.IDIndex[newID]=self.data[n];
			       for (col in old_comp_dict){
				      new_comp_dict[col][newID]=old_comp_dict[col][oldID];
			       }
			}catch(e){
			       console.log(e);
			}			
		}
		
		
		self.comp_values.dict=new_comp_dict		
		//compare incoming data with existing data and change if necessary
		var nonEditableColumns = {};
		for (var ci=0;ci<self.col_list.length;ci++){
			var colu = self.col_list[ci]
			if (!colu['editable']){
				nonEditableColumns[colu['name']]=true;
			}
		}
		for (var index in data){
			var record = data[index];
		        if (self.isCompoundColumn(fieldname)){

			var accession = record[fieldname][0][fieldname.toLowerCase()];
			}else {
			var accession = record[fieldname];			
			
			}
		
			/*
			var rowIndex = accToRow[accession];//might be wrong index when some filename(such a bad barcode)
			if (!rowIndex && rowIndex !==0){
				continue;
			}
			var rowID = self.getRowId(rowIndex);
			*/
			
			var rowID= data[index]['id'];//correct the row id
			var rowIndex=self.getRowIndex(rowID);//correct the row index
			
			var comp =self.comp_values;
			for (var field in record){
				
				//Cannot edit Acession
				if (field === 'Accession' || field ==='assembly_status' || field ==="id" || field === "best_assembly" || field == 'owner' || field == 'not_editable'){
					continue;
				}
				//special_field
				if (field === 'extra_row_info'){
					self.extraRowInfo[rowID]=field['extra_row_info'];
					continue;
				}
	    
		
				//deal with compound fields
				var compColName = self.compoundNamesToColumn[field];
				if (compColName){
					
					var colIndex = self.getColumnIndex(compColName);
					var gridValue = comp.getValue(compColName,rowID,1,field);
					//field not editable there always be database value
					if (!self.val_params[field]['editable'] || !self.col_list[colIndex]['editable']){
						if (data[field]){
							comp.setValue(compColName,rowID,1,field,record[field],true);
							self.setValueAt(rowIndex,colIndex,comp.getDisplayValue(compColName,rowID),false);						
						}
						continue;
					}
					
					//nothing in either
					if (!gridValue && ! record[field]){
						continue;
					}
					//nothing in database but in file therefore mark as changed
					if (gridValue && !record[field]){
						self.compoundValueChanged(compColName,rowID,1,field,"",gridValue);
					}
					 //no value in grid but in database - display it but
					 //it has not changed
					else if (!gridValue && record[field]){
						comp.setValue(compColName,rowID,1,field,record[field],true);
						self.setValueAt(rowIndex,colIndex,comp.getDisplayValue(compColName,rowID),false);
					}
					
					//different value than database - mark as changed
					else if (gridValue !== record[field]+""){
						self.compoundValueChanged(compColName,rowID,1,field,record[field],gridValue);
					}
				}
			
				//non compund values
				else{
					var colIndex= self.getColumnIndex(field);
					//checck index,
					if (colIndex==-1)
					    continue;
					var gridValue = self.getValueAt(rowIndex,colIndex);
					if (!gridValue && ! record[field]){
						continue;
					}
					//field not editable there always be database value
					if (!self.val_params[field]['editable']){
						if (record[field]){
							self.setValueAt(rowIndex,colIndex,record[field],false);
						}
						continue;
					}
					//nothing in database but in file therefore mark as changed
					if (gridValue && !record[field]){
						self.modelChanged(rowIndex,colIndex,"",gridValue,null,true);
					}
					//no value in grid but in database - display it but
					//it has not changed
					else if (!gridValue && record[field]){
						self.setValueAt(rowIndex,colIndex,record[field],false);
					}
					//different value than database - mark as changed
					else if (gridValue !== record[field]+""){
						self.modelChanged(rowIndex,colIndex,record[field],gridValue,null,true);
					}
				}
			}
		}
	//	$(this).dialog('destroy').remove();

		self.newDataLoaded(false);
		//callback(all_data['experiment']);
		callback(all_data['experiment'],nonEditableItemId,fieldname);
		if (self.expandAllSubstrains){
			self.expandAllSubstrains();
		
		}
		
		
     });			
			
		$(this).dialog('destroy').remove();
		 },
		Close: function () {
		    $(this).dialog('destroy').remove();
		}
	    }
	    
	});
					
			
	
	

};

       
