/* global ValidationGrid */

AssemblyGrid.prototype = Object.create(BaseExperimentGrid.prototype);
AssemblyGrid.prototype.constructor= AssemblyGrid;


function AssemblyGrid(name,config,species){
    BaseExperimentGrid.call(this,name,config,species,"assembly","Assembly Stats");
    this.assemblySentCallback = function(){}
    this.download_url= '/upload/download';
    this.windows_opened=1;
  
    
    var self = this;
    this.assemblySent = function(rowIDs){
        for (var index in rowIDs){
            var rowID = rowIDs[index];
            var rowIndex = this.getRowIndex(rowID);
            var vis = self.isRowVisible(rowID);
            self.setValueAt(rowIndex,0,"Queued",vis);
            var a = self.getValueAt(rowIndex,0);
        }

    }
    
    
    this.addExtraRenderer("status",function(cell,value,rowID){
        if (value === 'Assembled' || value === 'Failed QC' || value === 'Poor Quality'|| value === 'Contaminated'){
            $(cell).html($("<span>"+value+"<img src='/static/img/upload/download.gif' width='"+self.fontSize+"' height='"+self.fontSize+"'></img></span>")).css("cursor","pointer");
        }  
        if (value === 'NEW'){          
            var platform = self.strainGrid.comp_values.getValue("Accession",rowID,1,'seq_platform');
            value = "Unassembled("+platform+")";
           
            if (!self.nonEditableRows[rowID] && self.editMode){
                $(cell).html($("<span>"+value+"<img src='/static/img/upload/assembly.gif' width='"+self.fontSize+"' height='"+self.fontSize+"'></img></span>")).css("cursor","pointer"); 
            }
            else{
                $(cell).html(value);
            }                
        }
    
    });
    this.addCustomColumnHandler("status",function(cell,rowIndex,rowID){
        var col = self.getColumnIndex("status");
        var value = self.getValueAt(rowIndex,col);
          if (value === 'Assembled' || value === 'Failed QC' || value === 'Poor Quality'|| value === 'Contaminated'){
               self.strainGrid.downloadAssemblies([rowID]);   
          }
          else if (value === 'NEW' && !self.nonEditableRows[rowID] && self.editMode){
            var no =self.strainGrid.sendRecordsForAssembly([rowID],self.showMessage);
            if (no!=0){
                self.setValueAt(self.getRowIndex(rowID),self.getColumnIndex('status'),"Queued",false);
                $(cell).html("Queued");
            }
          }
    });
    //contains [locus][rowID(genomeid)] = [allele_id,]
    this.alleleInfo = {}
    this.loci = {}
    this.ids={};
};

AssemblyGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Assembly#"+msg,'newwindow','width=1000, height=600');
};



AssemblyGrid.prototype.getJBrowseURI=function(barcode){
        var arr = barcode.split("_");
        var uri = "?data=entero_data/"+arr[0]+"/"+arr[1].substring(0,4)+"/"+barcode;
        return Enterobase.jbrowse_url+uri;
}

AssemblyGrid.prototype.associateNewReads=function(rowID,names,strain,div){
    var to_send= {
        database:this.species,
        names:names,
        strain_id:rowID,
        strain_name:strain,
        append:$("#associate-reads-check").prop("checked")
    }
    Enterobase.call_restful_json("/associate_new_reads","POST",to_send,this.showMessage).done(function(data){
        if (data['result'] === 'success'){
            $(div).html('The reads names have been submitted<br> Please upload them on the upload reads page')
        }
    });

}


AssemblyGrid.prototype.associateNewReadsDailog= function(rowID){
   var self = this;
   var div = $("<div>");
   var table = $("<table>").appendTo(div);
   var strain = this.strainGrid.getStrainName(rowID);
   for (var i=1;i<3;i++){
        var row = $("<tr>")
        $("<td>").html("Read "+i+" name").appendTo(row);
        $("<td>").append($("<input>").attr("id","new-read-"+i)).appendTo(row);
        table.append(row);
       
   
   }
   row=$("<tr>");
   row.append($("<td>").append($("<input type='checkbox' id='associate-reads-check'><label>Combine with existing reads</label>")));
   table.append(row);
    div.append($("<div> Please enter the names of the new reads below </div><br>"))
   div.append(table);
   var resultDiv = $("<div>");
   div.append (resultDiv);
  
        div.dialog({
            autoOpen: true,
            modal: false,
            title: "New Reads For "+strain,
            width: 'auto',
            height:'auto',
            buttons: {
                "Close": function () {
                   
                    $(this).dialog("close");
                },
                 "Submit": function () {
                    var read1= $("#new-read-1").val()
                    var read2= $("#new-read-2").val()
                    if (!read1 || !read2){
                        return;
                    }
                    self.associateNewReads(rowID,read1+","+read2,strain,resultDiv)
                }
                
            },
            
            
            close: function () {
                $(this).dialog('destroy').remove();
            }
        });

}



AssemblyGrid.prototype.setMetaData= function(data){
    //add the column to display the assembly in jbrowse
    var column = {
        name:"can_view",
        not_write:true,
        dispaly_order:8,
        label:"<span class='glyphicon glyphicon-eye-open'></span>",
    }
    data.push(column)
      
    var self =  this;           
    ValidationGrid.prototype.setMetaData.call(self,data);
    this.columnWidths['can_view']=30;
    //add red colour cells which don't pass QC
    for (var index in data){
        var colname = data[index]['name']
        if (colname=='status'){
            continue;
        }
        
        self.addExtraRenderer(colname,function(cell,value,rowID){
            if (!this.extraRowInfo[rowID]){
                return;
            }
            var failed= this.extraRowInfo[rowID][1]
            if (failed.length === 0){
                return;
            }
            var colName = self.col_list[cell.columnIndex]['name'];
            for (var i in failed){
                if (failed[i]===colName){
                    $(cell).css('background-color','red');
                    break;
                }
            }
         });           
    }
    this.addExtraRenderer("can_view",function(cell,value,rowID){
        if (value === 'true'){
            $(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")                            
            .css("cursor","pointer");
        }
        else{
            $(cell).html("&nbsp");
        }
    });
    this.addCustomColumnHandler("can_view",function(cell,rowIndex,rowID){
        var colIndex= self.getColumnIndex("barcode");
        var colI = self.getColumnIndex("can_view");
        var val = self.getValueAt(rowIndex,colI);
        var strain_name = self.strainGrid.getStrainName();
        var barcode = self.getValueAt(rowIndex,colIndex);
        if (! val){
            return;
        }
        if (self.strainGrid.isGenomePrivate(rowID)){
            Enterobase.modalAlert("This genome is private and cannot be viewed");
        }
        else if (!self.strainGrid.isDownloadable[rowID]){
            Enterobase.modalAlert("You are not allowed to view this genome");
        }
        else{
            var win_name = "jbrowse"+self.windows_opened;
            self.windows_opened++;
            var args ="?barcode="+barcode+"&database="+self.species;
            var win = window.open("/view_jbrowse_annotation"+args,win_name,'width=1000, height=600');
        }
   });
} 




AssemblyGrid.prototype.loadNewData= function(data){
     //get allele info in order that clicking on the can allow retrieval of information
      for (var index in data){
            var row = data[index];
            if (row instanceof Array){
                row = row[0];
                data[index]=row;
            }
            if (!row['status']){
                row['status']="NEW";
            }
            var id = row['id'];
            this.nonEditableRows[id]=true; 
            this.isDownloadable[id]=false; 
       }     
       ValidationGrid.prototype.loadNewData.call(this,data,false,true);
}
AssemblyGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
        var id = this.getRowId(rowIndex);
        var self = this;
        var disable =true;
        if (this.strainGrid.editMode && !this.strainGrid.nonEditableRows[id]){
            disable =false;        
        }
        var extramenu=[	{
                         name: 'Associate New Reads',
                         title: 'Associate New Reads',
                         fun: function () {
                             self.associateNewReadsDailog(id);    
                         },
                         disable:disable
                 }
                
        ];
        
        
        return extramenu;
}