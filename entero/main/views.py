from flask_restful import  marshal
from sqlalchemy.orm.exc import  NoResultFound


from entero.decorators import auth_login_required,auth_login_method
from flask import render_template,current_app,flash,request,send_file, make_response,redirect,url_for,jsonify
import json, ujson, re, tempfile

from . import main
#from entero.utilities import getStrainInfo
from flask_login import current_user
from entero.databases.system.models import User,UserUploads,UserBuddies, check_permission,UserPreferences,UserJobs,query_system_database,BuddyPermissionTags,query_system_database,UserPermissionTags
from entero import dbhandle, db,app, get_database, rollback_close_system_db_session, get_download_scheme_folder
from entero.cell_app.tasks import process_job,make_jbrowse_from_genome
from collections import Counter
from entero import dbhandle
from entero.ExtraFuncs.workspace import LocusSearch,SNPProject,get_analysis_object,CustomView,MSTree,WorkSpace,SNPList, get_shared_folders,get_access_permission, has_edit_permission_analysis_item
import os, requests
from werkzeug.utils import secure_filename
from datetime import datetime, timedelta
from entero.ExtraFuncs.query_functions import query_function_handle,get_alleles_from_strain_ids
from entero.ExtraFuncs.query_functions import combine_strain_and_exp_data,process_strain_query,load_main_workspace,process_custom_view_query
from entero.ExtraFuncs.geocode import geoCoding
from entero.databases.generic_models import decode as decode_barcode
from entero.jobs.jobs import LocusSearchJob,RefMaskerJob,get_crobot_job

import traceback

import numpy as np, pandas as pd
from ete3 import Tree
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


#*********************************Web Pages****************************************************************

@main.route('/')
def index():
    '''Displays the main index 

    * **url** /
    * **template** entero/templates/index.html
    '''
   
    db_info = []
    dbs  = app.config['ACTIVE_DATABASES']
    id = -1 
    if hasattr(current_user, 'id'):
        id = current_user.id
    for db_name in dbs:
        if dbs[db_name][2] == True or check_permission(id, 'view_species', db_name):
            rec = {}
            db = dbhandle[db_name];
            rec['name'] = db_name
            rec['label']= dbs[db_name][0]
             #= db.getStrainNumber()
            rec['total_strains'], rec['assemblies'], rec['queued'], rec['mlst_achtman'] = db.getAssembledNumber()
            db_info.append(rec)
            rec['scheme_info'] = db.getSchemeNumber(public_only= True)
    db_info.sort(key=lambda x: x['total_strains'], reverse=True)
    return render_template('index.html',db_info=db_info, help = app.config['WIKI_BASE'])



@main.route("view_jbrowse_annotation",methods=["GET","POST"])
def view_jbrowse_annotation():
    '''Works out the jbrowse uri and formats (if not already) the sequence for jbrowse
    The web page will keep polling :meth:`check_for_jbrowse_annotation`
    until the genome is ready and then load the jbrowse uri
    
    * **url** /view_jbrowse_annotation
    * **template** entero/templates/jbrowse.html

    GET parameters
        *  **database** - the name of the database
        *  **assembly_id** -the id of the assembly (only needed if barcode not supplied)
        *  **barcode** -the assembly barcode (only needed if assembly_id is not supplied)
        *  **highlight_genes** - a list of loci (comma delimited) to highlight (optional)
        *  **locus** - this locus will be the one initially in view (optional)
    '''
    database=request.args.get("database")
    dbase=get_database(database)
    if not dbase:
        return "No valid database (%s) name is provided"%database
    
    assembly_id=request.args.get("assembly_id")
    if assembly_id:
        barcode=dbhandle[database].encode(int(assembly_id),database,"assemblies")
    else:
        barcode =request.args.get("barcode")        
    #Add access permission check
    user_id=-1
    permission=False
    if current_user.is_authenticated():
        user_id=current_user.id
    Strains=dbase.models.Strains
    Assemblies=dbase.models.Assemblies
    strain=dbase.session.query(Strains).join(Assemblies).filter(Strains.best_assembly==Assemblies.id).filter(Assemblies.barcode==barcode).first()
    if not strain:
        return "No strain is found"
    permission=get_access_permission(database, user_id, strains=[strain.__dict__])
    if not permission:
        return "You are not allowed to view this genome"
        
    loci_to_highlight = request.args.get("highlight_genes")    
    locus=request.args.get("locus")
    if app.config['USE_CELERY']:
        make_jbrowse_from_genome.apply_async(args=[barcode,database],queue='entero')
    else:
        make_jbrowse_from_genome(barcode,database)
    arr = barcode.split("_")
    if not loci_to_highlight:
        loci_to_highlight = locus
    uri =app.config['JBROWSE_URI']+ "?data=entero_data/"+arr[0]+"/"+arr[1][0:4]+"/"+barcode;
    if locus:
        uri=uri+"#loc="+locus+"#tracks=prokka_gff"
    if loci_to_highlight:
        uri=uri+"#highlight_genes="+loci_to_highlight
    return render_template("jbrowse.html",jbrowse_uri=uri,barcode=barcode,database=database)



@main.route("ms_tree",  methods = ['GET','POST'])
def ms_tree():
    '''The MS Tree page

    * **url** /ms_tree
    * **template** entero/templates/ms_tree/MSTree_holder.html

    GET parameters
        *  **tree_id** The ID of the tree
    '''
    tree_id = request.args.get("tree_id", 0)
    template = "ms_tree/MSTree_holder.html"
    return generate_tree_page(tree_id,template,"ms")
    
@main.route("ms_tree/<tree_id>/",  methods = ['GET','POST'])
def ms_tree_page(tree_id):
    '''The MS Tree page with the id in URL - simply redirects to ms_tree
    
        * **url** /ms_tree/<tree_id>
        * **template** entero/templates/ms_tree/MSTree_holder.html
    '''
    try:
        tree_id = int(tree_id)
    except:
        tree_id=0    
    return redirect(url_for("main.ms_tree",tree_id=tree_id))

@main.route("phylo_tree",  methods = ['GET','POST'])
def phylo_tree():
    '''The page rto display phylograms
    
    * **url** /phylo_tree
    * **template** entero/templates/ms_tree/PhyloTree_holder.html

    GET parameters
        *  **tree_id** The ID of the tree
    '''
    tree_id = request.args.get("tree_id",0)
    template = "ms_tree/PhyloTree_holder.html"
    return generate_tree_page(tree_id, template,"phylo")

@main.route("phylo_tree/<tree_id>/",  methods = ['GET','POST'])
def phylo_tree_page():
    '''The PhyloTree page with the id embeded in the URL (just redirects to /phylo_tree)
        
        * **url** /phylo_tree/<tree_id>
        * **template** entero/templates/ms_tree/PhyloTree_holder.html
    '''
    #I am not sure about this method
    #I have comented the the line which is used to get the tree_id and added a new one to get it from the request args
    try:
        #tree_id = int(tree_id)
        tree_id = int(request.args.get("tree_id"))
    except:
        tree_id=0
    return redirect(url_for("main.phylo_tree",tree_id=tree_id))

def generate_tree_page(tree_id,template,tree_type):
    '''Helper method which generates the tree page
    
    :param tree_id: The id of the tree
    :param template: The tamplate to use
    :param tree_type: Either ms or phylo
    '''
    scheme_dict=""
    tree_name = ""
    edit_permissions="false"
    db_code=""
    try:
        wg_scheme= "wgMLST"
        if tree_id and tree_id.isdigit():
            try:
                tree_info = db.session.query(UserPreferences).filter_by(id=tree_id).one()
            except:
                app.logger.exception("%s tree with id: %s not found" % (tree_type,tree_id))
                rollback_close_system_db_session()
                #db.session.rollback()
                return "tree not found",404
            #not logged in and not public
            dbase = dbhandle[tree_info.database]
            if not current_user.is_authenticated():
                if tree_info.user_id <> 0:
                    return redirect(url_for('auth.login',next=request.url))
                #logged in but no permission
            else:
                permission = False

                if current_user.administrator == 1:
                    permission = True
                elif current_user.id == tree_info.user_id or tree_info.user_id ==0:
                    permission = True
                else:
                    tree_data= ujson.loads(tree_info.data)
                    parent_workspace = tree_data.get("parent")
                    query_id =tree_id
                    if parent_workspace:
                        query_id=parent_workspace
                    bud = db.session.query(BuddyPermissionTags).filter_by(buddy_id=current_user.id,value = str(query_id)).all()
                    if len(bud)<>0:
                        permission = True
                if not permission:
                    if not isinstance(query_id, int) and query_id.isdigit():
                        query_id=int(query_id)
                    if not isinstance(query_id, int):
                        raise Exception("%s is not valid id"%query_id)
                    sql = "SELECT buddy_permission_tags.value as ws_id ,buddy_permission_tags.field, buddy_permission_tags.user_id AS ws_owner_id, users.username AS ws_owner_name FROM" + \
                          " buddy_permission_tags INNER JOIN users ON buddy_permission_tags.user_id = users.id WHERE buddy_id =%i AND species='%s' AND field ='workspace_folders' " % (
                              current_user.id, tree_info.database)
                    tags = query_system_database(sql)
                    for tag in tags:
                        if tag.get('field') == 'workspace_folders':
                            shared = json.loads(tag.get('ws_id'))
                            shared_folder = shared.get('shared_folder')
                            budy_shared_foler, work_spaces = get_shared_folders(tree_info.database, tag.get('ws_owner_id'),
                                                                                shared_folder[0], shared_folder[1],
                                                                                str(tag.get('ws_owner_id')))

                            if query_id in work_spaces:
                                permission = True
                                break

                if not permission:
                    flash("You do not have access to this page, please contact an administrator 4.")
                    return redirect(url_for('main.index'))

            db_code = app.config['ACTIVE_DATABASES'][tree_info.database][4]
            tree_name = tree_info.name
            Schemes = dbase.models.Schemes
            scheme_dict = {}
            schemes = dbase.session.query(Schemes).filter(Schemes.param["display"].astext=="public").all()
            for scheme in schemes:
                if scheme.param.get("main_scheme"):
                    wg_scheme= scheme.description
                scheme_dict[scheme.description]=scheme.name
            edit_permissions= "false"

            user_id=0
            if current_user.is_authenticated():
                user_id= current_user.id
                if current_user.administrator or user_id == tree_info.user_id :
                    edit_permissions="true"
            custom_view_records=CustomView.get_records(user_id,tree_info.database)
            custom_views={}
            for record in custom_view_records:
                if record['type']=='custom_view':
                    custom_views[record['id']]=record['name']

            return render_template(template,tree_id=tree_id,
                                   tree_name=tree_name,scheme_dict=scheme_dict,
                                   edit_permissions=edit_permissions,db_code=db_code,
                                   wg_scheme=wg_scheme,js_version=app.config['JAVASCRIPT_VERSION'],
                                   database=tree_info.database,tree_type = tree_type,custom_views=custom_views)
        else:
            return "no tree_id %s is provided"%tree_id, 400
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in generate_tree_page, error message: %s"%e.message)
        flash("Error in generate tree page")
        return redirect(url_for('main.index'))




@main.route("get_tree_data",methods = ['GET','POST'])
def get_tree_data():
    '''Returns the data required to load the tree, a jsonified dictionary of :-

    * **layout_data**  the data which describes the layout of the tree
    * **database** the name of the database
    * **name** the name of the tree
    * **nwk** a string containing the contents of the tree's nwk file
    * **metadata** the metadata suitiable to load into the JavaScript tree object
    * **metadata_options**  A dictionary of field name (category) to a dictionary
    describing that category, see :func:`entero.ExtraFuncs.workspace.TreeAnalysis.save_metadata`
    * **complete** false if the tree job has not yet finished
    * **permission** false - if the user does not have permission to view the tree
    * **failed** true if the tree job failed


    * **url** /get_tree_data

    GET parameters
        * **id** the id of the tree/snp project
        * **tree_type** either ms or phylo
    '''    
    try:
        return_data={}
        tree_id =int(request.args.get("id"))
        tree_type= request.args.get("tree_type")
        tree = get_analysis_object(tree_id)
    
        if not tree:
            return ujson.dumps({"failed":"true","id":tree_id})


        if not tree.has_permission(current_user):
            return ujson.dumps({"permission":"false"})       
      
        data=tree.data    
    
        if data.get("failed"):
            return json.dumps({"failed":"true"})
        
        is_complete = data.get('complete')
        if not is_complete or is_complete=='false':
            return ujson.dumps({"waiting":"true"})
        return_data['database']=tree.database
        return_data['name']=tree.name
        return_data['nwk']=tree.get_nwk_tree()
        #print tree.data.keys()
    
        in_handle = open(data['data_file'])
        tree_data = ujson.loads(in_handle.read())
        in_handle.close() 
        user_id=-1
        if current_user.is_authenticated():
            user_id=current_user.id             
        ids=[]   
        for dict_ in tree_data['isolates']:   
            if dict_.get('StrainID'):                       
                if isinstance(dict_['StrainID'], int) or (dict_['StrainID']).isdigit():
                    ids.append(dict_['StrainID'])                         
        print "Start time: ", datetime.now()
        permission=get_access_permission(tree.database, user_id,strains_ids=ids)
        print "end time: ",  datetime.now(), permission
        
        return_data['permission']=permission
        return_data['layout_data']=tree.get_layout(tree_type)
        #legacy data
        if tree_data.get('identifiers'):
            return_data['data']=tree_data
        else:
            return_data['metadata_options']=tree_data['metadata_options']
            return_data['metadata'] = {}
            for item in tree_data['isolates']:
                return_data['metadata'][item['StrainID']]=item
        return ujson.dumps(return_data)        
        
    except Exception as e:
        app.logger.error("Failed to load MS tree id:%s, error message: %s" % (tree_id, e.message))
        print "Failed",
        print e.message
        return json.dumps({"failed":"true"})


@main.route("autocorrect",methods = ['GET','POST'])
def autocorrect():
    '''Searches for a field in the strains database which contains the supplied text.
    Returns a jsonified list of matched values

    * **url** /autocorrect

    GET Parameters
        * **text** the text used for searching
        * **field** the field to search for
        * **limit** the max number of values to return
    '''
    text = request.args.get('text')
    field = request.args.get('field')
    limit = request.args.get('limit')
    database= request.args.get('database')
    db = dbhandle[database]
    results=db.get_auto_correct_list(field,text,12)
    #This is to solve the issue when the results contains dates objects which are not JSON serializable
    #It tests if it is a date or datetime object, if so, it will convert it to string
    from datetime import date, datetime
    try:
        clean_results = []
        for res in results:
            #test if it is date object
            if isinstance(res, (datetime, date)):
                clean_results.append(res.isoformat())
            else:
                clean_results.append(res)
        return json.dumps(clean_results)
    except Exception as e:
        app.logger.exception("autocorrect failed, results is: %s, error message: %s"%(results, e.message))
        return json.dumps([])

@main.route("autocomplete_locus",methods = ['GET','POST'])
def autocomplete_locus():
    '''Searches for a locus where the label contains the supplied text
    Returns a jsonified list (upto 15) of dictionaries containing

    * **label**  the locus label
    * **value** the locus name (what the database users)
    
    * **url** /autocomplete_user_name

    GET parameters
        * **text** the text used for searching
        * **scheme** the name (description) of the MLST scheme
        * **database** the name of the database 
    '''
    text = request.args.get('text')
    database = request.args.get('database')
    tabname = request.args.get("scheme")
    dbase=get_database(database)
    ret_list=[]
    if not dbase or not tabname or not text:
        app.logger.error("autocomplete_locus failed, databse, table name and query text need to provided, database: %s, table name: %s, query: %s"%(database, tabname, text))
        return json.dumps(ret_list)

    sql = "SELECT name,label FROM data_param WHERE tabname='"+tabname+"' AND group_name='Locus' AND label LIKE '%"+text+"%' LIMIT 15"
    try:
        results = dbase.execute_query(sql)
        for result in results:
            ret_list.append({"label":result['label'],"value":result['name']})
        return json.dumps(ret_list)
    except Exception as e:
        app.logger.exception("autocomplete_locus failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return json.dumps(ret_list)




@main.route("autocomplete_user_name",methods = ['GET','POST'])
@auth_login_method(True)
def autocomplete_user_name(): 
    '''Searches for a user whose first or second names contain the supplied text.
    Returns a jsonified  list (max 10) of dictionaries containing:-

    * **lablel**  consitst of firstname lastname (username)
    * **value** the id of the user
    
    * **url** /autocomplete_user_name

    GET parameters
        * **text** the text used for searching
    '''     
    text = request.args.get('text')
    sql = "SELECT id,firstname,lastname,username FROM users WHERE firstname ILIKE '%"+text+"%' OR lastname ILIKE '%"+text+"%' LIMIT 10"
    results = query_system_database(sql)
    ret_list=[]
    for result in results:
        label = "%s %s (%s) " % (result['firstname'],result['lastname'],result['username'])
        ret_list.append({"label":label,"value":result['id']})
    return json.dumps(ret_list)






@main.route("temp_upload",methods = ['GET','POST'])
def temp_upload():
    if request.method == 'POST':
        files = request.files.getlist('file')
        if files:
            for file in files:
                filename = secure_filename(file.filename)
                file.save(os.path.join("/share_space/temp_upload", filename))
            return "success"   



@main.route("website_callback_debug",methods = ['GET','POST'])
def website_callback_debug():
    '''The callback that crobot user

    * **url** /website_callback_debug

    POST parameters
        * **CALLBACK** the data returned in a json string
        
    '''    
    #callback_info = 'NO REQUEST'
    try:
        callback_info = request.form.get("CALLBACK")
        if callback_info:
            data = json.loads(callback_info)
            jobNo = data.get("tag",0)
            #app.logger.info("Received Call Back on Job %s" % jobNo)
            job = get_crobot_job(data=data)
            #app.logger.info("Job %s has been gotten with object type %s" % (jobNo, type(job)))
            if job:
                return job.update_job()
            else:
                app.logger.error("website_callback_debug failed, could not find Job, the callback_info is: %s" % callback_info)

                return "failed"
        else:
            app.logger.error("website_callback_debug failed, could not find CALLBACK, the form keys are: %s"%request.form)
            return "failed"

        #return "Created"
    except Exception as e:
        app.logger.exception("Call Back on job could not be processed, forms keys are: %s, error message: %s"%(request.form, e.message))
        app.logger.exception(traceback.format_exc())
        return "Failed"





@main.route("get_accessory_genome",methods = ['GET','POST'])
def get_accessory_genome():
    '''Gets (or calculates the accessory genome) of the supplied analysis object (workspace).
    Returns a jsonified dictionary containing the following
    
    * **status** either OK or an error message
    * **annotation_data** the data to load into the JavaScript CanvasHeatMap
    * **table_data** data to display in JavaScript LocusGrid
    * **scheme** the name(description) of the scheme used to calcuate the accessory genome(usually wgMLST)
    * **name** the name of the analysis object processed
   
    * **url** /get_accessory_genome
   
    GET parameters
        * **database** the name of the databsae
        * **analysis_id** the id of the analysis object (workspace)
    '''
    database= request.args.get("database")
    dbase = get_database(database)
    ws_id = request.args.get('analysis_id')
    if not database or not ws_id or not ws_id.isdigit():
        app.logger.exception("get_accessory_genome failed, arguments erros, database: %s, ws_id: %s"%(database, ws_id))
        return ujson.dumps({"status": "Failed"})
    ws_id = int(ws_id)
    ws = get_analysis_object(ws_id)
    sql = "SELECT description FROM schemes WHERE param->>'main_scheme' = 'true'"
    results = dbhandle[database].execute_query(sql)
    scheme = results[0]['description']
    obj={}
    obj['nwk']=ws.get_nwk_tree()
    acc_data= ws.get_accessory_genome()
    if not acc_data:
        if ws.get_strain_number()>2000:
            return  ujson.dumps({"status":"Accessory Genome is Limited to 2000 Strains"})
        acc_data= ws.make_core_accessory()   
    obj['data']=acc_data['accessory'][1]
    obj['loci']=acc_data['accessory'][0]    
    obj['display_format']='presence'
    obj['metadata']=ws.get_metadata()
    obj['metadata_label']='strain'


    DataParam = dbase.models.DataParam
    try:
        descriptions = dbase.session.query(DataParam.param,DataParam.name,DataParam.description,DataParam.label).filter(DataParam.name.in_(obj['loci'])).all()
        table_data= []
        locus_dict={}
        cat_info= app.config['GENE_CATEGORIES'].get(database)
        gene_categories ={}
        if cat_info:
            for cat in cat_info:
                gene_categories[cat]={"loci":[],"color":cat_info[cat]['color']}
        for desc in descriptions :
            locus_dict[desc.name]=[desc.label,desc.description,desc.param]

        for count,locus in enumerate(obj['loci']):
            arr = locus_dict[locus]
            if cat_info and arr[2]:
                for cat in cat_info:
                    info= arr[2].get(cat)
                    if info:
                        gene_categories[cat]['loci'].append(count)

            table_data.append({"extra_row_info":locus,"locus":arr[0],"description":arr[1],"start":count})

        obj["gene_categories"]=gene_categories
        return ujson.dumps({"status":"OK","annotation_data":obj,"table_data":table_data,"scheme":scheme,"name":ws.name})

    except Exception as e:
        app.logger.exception("get_accessory_genome failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return ujson.dumps({"status": "Failed"})

@main.route("get_access_permission",methods = ["POST"])
def get_strains_access_permissions():
    '''
    Input are the either   strains ids list, in addition to database name, gotten from request
    It can be used to return only false or true if return_all_strains_permissions is sent and be False
    Otherwise it will return a dictwhich id is the key and the value is either 
        3: owner, admin, curatior, buddy with edit permission
        2: buddy without edit permission
        1: can download as strain is public strain, released strain
        0: can not download data as strain is private
        
    '''
    return_all_strains_permissions=request.form.get("return_all_strains_permissions")
    if not return_all_strains_permissions:
        return_all_strains_permissions=False
    elif return_all_strains_permissions.lower()=='true':
        return_all_strains_permissions=True
    else:
        return_all_strains_permissions=False            
    try:        
        database=request.form.get("database")
        if not database:  
            raise Exception("No database name is provided")            
        
        strain_ids = request.form.get("strain_ids")
        if not strain_ids:
            raise Exception("No valid strain ids are provided")            
        strains_ids = strain_ids.split(",")        
        if current_user.is_authenticated():
            user_id=current_user.id
            permissions=get_access_permission(database, user_id, strains_ids=strains_ids, return_all_strains_permissions=return_all_strains_permissions)
            return ujson.dumps(permissions)
        else:
            if not return_all_strains_permissions:
                return ujson.dumps(False)
            else:
                permissions={}
                for id in strains_ids:
                    permissions[id]=0 
                return ujson.dumps(permissions)
    except Exception as e:
            app.logger.exception("get_access_permissions error, error message: %s"%e.message)
            if not return_all_strains_permissions:
                return False
            else:
                return {}
                #return ujson.dumps("Error while handling the request!")          
 
    
    
#given a list of comma delimited strains ids (strain_ids in a POST request)
#the method will return a dictionary of strain_id to  key/value 
#experimental data 
@main.route("get_experimental_data",methods = ["POST"])
def get_experimental_data():
    strain_ids = request.form.get("strain_ids")
    database=request.form.get("database")
    scheme =request.form.get("scheme")
    extra_data = request.form.get("extra_data")
    ids = strain_ids.split(",")
    bass_to_sid = {}
    aids = []
    dbase = get_database(database)
    #
    param_dict={}
    try:
        if (scheme=='custom_view'):
            c_view = get_analysis_object(int(extra_data))
            columns=c_view.get_columns()
            for column in columns:
                param_dict[column['name']]=column['label']
        else:
            DataParam = dbase.models.DataParam
            params = dbase.session.query(DataParam).filter(DataParam.tabname==scheme,DataParam.nested_order==0).all()
            for param in params:
                param_dict[param.name]=param.label
        ass_data={}
        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains.id,Strains.best_assembly).filter(Strains.id.in_(ids)).all()
        for strain in strains:
            bass_to_sid[strain.best_assembly]=strain.id
            aids.append(strain.best_assembly)
        exp_data,aids =query_function_handle[database][scheme](database,scheme,','.join(map(str,aids)),"assembly_ids",extra_data)
        for ass_id in exp_data:
            sid = bass_to_sid.get(ass_id)
            if not sid:
                continue
            ass_data[sid]=exp_data[ass_id]
        return ujson.dumps({"data":ass_data,"fields":param_dict})
    except Exception as e:
        app.logger.exception("get_experimental_data failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return ujson.dumps({"data": {}, "fields": {}})



@main.route("get_workspace_children",methods = ['GET','POST'])
def get_workspace_children():
    ws_id= request.args.get("id")
    ws= get_analysis_object(ws_id)
    return ujson.dumps(ws.get_children())

    

@main.route("load_locus_search",methods = ['GET','POST'])
def load_locus_search():
    ls_id =request.args.get("id")
    if not  ls_id or not ls_id.isdigit():
        app.logger.error("load_locus_search failed, no valid ls_id is provided, ls_id: %s"%ls_id)
        return json.dumps({"failed:": "true"})

    ls = get_analysis_object(int(ls_id))
    permission = ls.has_permission(current_user)
    if not permission:
        return json.dumps("no permission")
    if ls.data.get("failed"):
        return json.dumps({"failed:":"true"})
    
    if (not ls.is_complete()):
        return ujson.dumps({"unfinished":"true"})
    l_info = ls.get_locus_info()
    c_info = ls.get_contig_info()
    return ujson.dumps({"loci_info":l_info['loci_info'],"loci":l_info['loci'],
                        "contig_info":c_info,"scheme":ls.data['scheme'],"name":ls.name})

@main.route("get_allele_info_for_ls",methods = ['GET','POST'])
def get_allele_info_for_ls():
    try:
        ws_id = request.args.get("analysis_id")
        ls_id = request.args.get("locus_search_id")
        if not ws_id or not ls_id or not ws_id.isdigit() or not ls_id.isdigit():
            app.logger.warning("get_allele_info_for_ls failed, no or/not proper args provided")
            return json.dumps({})
        ws_id = int(ws_id)
        ls_id=int(ls_id)
        ls = get_analysis_object(ls_id)
        ws= get_analysis_object(ws_id)
        obj = ls.get_allele_info(ws_id)
        obj['display_format']='annotation'
        obj['name']=ws.name
        return json.dumps(obj)
    except Exception as e:
        app.logger.exception("Error in get_allele_info_for_ls, error message: %s"%e.message)
        return json.dumps({})

@main.route("show_ownership_for_strain",methods=["GET"])
def show_ownership_for_strain():
    '''Returns a dictionary of the user details '''
    db_qu = False
    try:
        database=request.args.get("database")       
        dbase =get_database(database)
        if not dbase:
            return ujson.dumps({"failed":True,"msg":"Database '%s' is not valid"%database})        
        strain_id =request.args.get("strain_id")
        if not strain_id or strain_id=='undefined':
            return ujson.dumps({"failed":True,"msg":"Strain id '%s' is not valid"%strain_id})  
        else:
            if not isinstance(strain_id, int) and strain_id.isdigit():
                strain_id=int(strain_id)
        
        if not isinstance(strain_id, int):            
            return ujson.dumps({"failed":True,"msg":"Strain id '%s' is not valid"%strain_id})  
             
                
        #permission=False
        if not current_user.is_authenticated():
            #permissions=get_access_permission(database, current_user.id, strains_ids=[strain_id], return_all_strains_permissions=True)
            #print permissions
            #permission_=permissions.get(strain_id)
            #if permission_==3:
            #    permission=True
        #if not permission:
            return ujson.dumps({"failed":True,"msg":"This information is not available to Anonymous user"})
      
        #dbase =dbhandle[database]
       
        Strains = dbase.models.Strains
        strain = dbase.session.query(Strains.owner).filter_by(id=strain_id).first()
        if not strain.owner:
            return ujson.dumps({"failed":True,"msg":"The strain is not associated with an owner"})
        db_qu=True
        owner = db.session.query(User).filter_by(id=strain[0]).first()
        return jsonify(owner.as_dict())
    except Exception as e:
        app.logger.exception("Cannot show strain ownership database %s, strain id %s, error message: %s"% (database,strain_id, e.message))
        if not db_qu:
            dbase.rollback_close_session()
        else:
            rollback_close_system_db_session()
        return ujson.dumps({"failed":True,"msg":"There was a problem with your request"})


@main.route("create_locus_search",methods = ['GET','POST'])
def create_locus_search():
    try:
        fasta = request.form.get("fasta")
        scheme= request.form.get("scheme")
        database=request.form.get("database")
        name = request.form.get("name")
        if not current_user.is_authenticated():
            return ujson.dumps({"success":"false"})
        user_id = current_user.id
        params={"database":database,"name":name,"user_id":user_id}
        ls= LocusSearch.create_locus_search(params,scheme,fasta)
        ls.send_job()
        return ujson.dumps({"success":"true","id":ls.id})
    except Exception as e:
        app.logger.exception("Error in create_locus_search, request form is %s,  error message: %s"%(request.form,e.message))
        return ujson.dumps({"success":"false"})
    

        

@main.route("get_sts_by_barcode",methods = ['GET','POST'])
def get_sts_by_barcode():
    st_dict = {}
    try:
        barcode = request.form.get("barcode")
        url = app.config['NSERV_ADDRESS']+"/retrieve.api"
        resp = requests.post(url=url,data={"barcode":barcode, "convert": 0, 'fieldnames':'barcode,type_id,value_indices'},timeout=app.config['NSERV_TIMEOUT'])
        try:
            data = ujson.loads(resp.text)
        except:
            raise Exception ("could not load response%s" % resp.text)

        for record in data:
            rec_dict={}
            for allele in record['fieldvalues']:
                rec_dict[allele['fieldname']]=allele['value_id']
            st_dict[record['barcode']]=rec_dict
        return ujson.dumps(st_dict)
    except Exception as e:
        app.logger.error("error in get_sts_by_barcode, error message: %s"%e.message)
        return ujson.dumps({})



@main.route("get_data_for_experiment",methods = ['GET','POST'])

def get_data_for_experiment():
    '''Retrieves strains and experimental data from the database depending on a strain or experimetal query (or both)
    
    POST parameters
    
    * **database** The name of the database (required)
    * **experiment** The name (description) of the experiment/scheme (required)
    * **experiment_query** The  SQL query for retreiving the experimental data
    * **experiment_query_type** The type of query can be one of the following:-
        * query - the default value, the clause in experiment_query will be evaluated
        * strain_ids - only Experimental data associated with the strain IDs (supplied by a  comma delimited list  in experiment_query) will be returned. No strain data is returned
        * st_match - only applicable to MLST schemes . The experiment query should be in the following format
            * st -st number:number of mismatches accepted e.g. st:65:3 
            * alleles - list of alleles (in the same order as DataParams), with 0 as a wild card e.g. for seven gene MLST alleles:34,3,0,0,23,2,2  
    * **strain_query** - SQL WHERE clause e.g. name = 'bug1'  or one of the following:-
        * query -  only those strains which match the query clause are returned (default)
        * all - get all the data
        * my_records - all the strains owned by the current user and those he is buddies with
        * default:int - The latest int records
    * **strain_query_type** The type of strain query
        * accessions - The strain_query should  contain a comma delimited list of accessions - note If the strain has multiple accessions only the specified accessions will be returned
        * main_workspace - A workspace will be returned, the strain_query should contain 'workspace name|user_id'
    
    * **only_with_data**  if true, only strains with associated data will be returned
    * **only_editable**  if true only strains that the user can edit will be returned
    * **no_legacy ** if true legacy data will be excluded
    * **show_substrains** if true then if a query matches an uberstrain or substrain, all substrains and the ubserstrain associated with the match
    will be returned, otherwise only uberstrains are queried
    * **show_non_assemblies** If true then all records will be returned otherwise only Queued,Legacy and Assembled will be returned
    * **operand**  if both experiment and strain queries are supplied than this specifies how they are logically combined. (AND or OR) 
    * **extra_data** Can contain anything. For cusotm views contains the ID of the custom view
    **returns** a jsonfied dictionary in the following format:-
    *{ strains:strain_data,experiment:experiment_data }*
    
    Both strain_data and experiment _data are lists of dictionaries containing key value pairs of all the fields in the record. Both have a field 'id' which 
    corresponds to the strain id so both lists can be linked. Initially both lists are in synch
    If the experimental_query_type is 'strain_ids' only the experiment_data list is returned in the same order as the strain ids
    '''
    # extract all the post requests
    database = request.form.get("database")
    exp = request.form.get("experiment")
    #print "expermintal: ", exp
    temp=exp
    #I have added this to fix issue of recieving wrong attributes from the front end
    # when uploading the modified metadata from a file while loading custom view
    if exp:        
        if not query_function_handle[database].get(exp):
            if isinstance(exp, int) or exp.isdigit():
                exp='custom_view'
           
    try:
        exp_query =request.form.get("experiment_query")
        exp_query_type = request.form.get("experiment_query_type")
        strain_query =request.form.get("strain_query")
        strain_query_type = request.form.get("strain_query_type")
        show_substrains = request.form.get('show_substrains')
        extra_data= request.form.get("extra_data")        
        if not extra_data and  exp!=temp:
            extra_data=temp
        

        options={
            "no_legacy":request.form.get("no_legacy"),
            "only_editable":request.form.get("only_editable"),
            "only_with_data":request.form.get("only_with_data"),
            "show_non_assemblies":request.form.get("show_non_assemblies"),
           
        }
        user_id=-1
        if (current_user.is_authenticated()):
            user_id = current_user.id
        if strain_query_type == "main_workspace":
            options["show_non_assemblies"]="true"
            return load_main_workspace(user_id,database,strain_query,options)
     
        if strain_query=='my_strains':
            options["show_non_assemblies"]="true"
        if strain_query and strain_query.startswith('default'):
            options["show_non_assemblies"]="true"
        #query strain data and tie it to experimental data and return
        if strain_query and not exp_query:
            strain_data,aids = process_strain_query(database,strain_query,strain_query_type,user_id,show_substrains=show_substrains)
            if len(strain_data) ==0:
                return json.dumps({"strains":[],'experiment':[]})
            #no assemblies 
            if  not aids:
                exp_data={}
            else:
                print database,exp,extra_data
                exp_data,aids = query_function_handle[database][exp](database,exp,aids,"assembly_ids",extra_data)
            return combine_strain_and_exp_data(strain_data,exp_data,options)
        
        #just get experimental data based on strain ids and return list in correct order
        if exp_query_type == "strain_ids":
            strain_id_list = exp_query
            sid_to_aid,aids = dbhandle[database].get_assembly_ids(exp_query)
            if len(aids)==0:
                exp_data = {}
            else:
            #a dictionary of assembly id:experiment data
                #print "====================="
                #print query_function_handle[database]
                #print exp
                #print "++>>>",query_function_handle[database][exp]
                exp_data,aids = query_function_handle[database][exp](database,exp,','.join(map(str,aids)),"assembly_ids",extra_data=extra_data)
            #make sure they are in the correct order
            exp_list=[]
            strain_id_list = exp_query.split(",")
            for sid in strain_id_list:
                #get the assembly id
                aid = sid_to_aid.get(int(sid))
                #if none make a blank one
                if aid==None:
                    rec={}            
                else:
                    rec = exp_data.get(aid,{})
                    
                rec['id']=sid
                exp_list.append(rec)            
            return json.dumps(exp_list)       
            
        #no strain query just get experiental data and tie it to strain data
        if not strain_query and exp_query:
            exp_data,aids = query_function_handle[database][exp](database,exp,exp_query,exp_query_type,extra_data)
            if len(exp_data) == 0:
                return json.dumps({"strains":[],'experiment':[]})
            text= "best_assembly IN (%s)" % ','.join(map(str,aids))
            strain_data,saids = process_strain_query(database,text,"query",user_id,show_substrains=show_substrains)

            if show_substrains and exp_query_type <> "assembly_ids":
                #are there any substrains present, these will not have experimental data
                extra_ass_ids=[]
                for item in strain_data:
                    if item['best_assembly'] and not item['best_assembly'] in exp_data:
                        extra_ass_ids.append(str(item['best_assembly']))

                if len(extra_ass_ids)>0:
                    extra_aids_list = ",".join(extra_ass_ids)
                    extra_exp_data,aids = query_function_handle[database][exp](database,exp,extra_aids_list,"assembly_ids",extra_data)
                    exp_data.update(extra_exp_data)

            return combine_strain_and_exp_data(strain_data,exp_data,options)
        
        
        #both strain and experiment data are being queried
        if strain_query and exp_query:
            operand = request.form.get("operand")
            if not operand:
                app.logger.error("Error in main Query Method, no logically combined operator is provided")
                return ujson.dumps("Error")
            exp_data,aids = query_function_handle[database][exp](database,exp,exp_query,exp_query_type,extra_data)
            if len(exp_data) ==0 and operand == "AND":
                return json.dumps({"strains":[],'experiment':[]})
            string_aids  = ','.join(map(str,aids))
            strain_data,strain_aids = process_strain_query(database,strain_query,strain_query_type,user_id,string_aids,operand,show_substrains=show_substrains)
            if len(strain_data)==0:
                    return json.dumps({"strains":[],'experiment':[]}) 
            if operand == "AND":
                return combine_strain_and_exp_data(strain_data,exp_data,options)
            #possibly some assemblies not yet obtained        
            elif operand == "OR":
                extra_aids = set(strain_aids.split(","))-set(aids)
                extra_aids_list= ",".join(map(str,extra_aids))
                extra_exp_data,aids = query_function_handle[database][exp](database,exp,extra_aids_list,"assembly_ids",extra_data)
                exp_data.update(extra_exp_data)
                return combine_strain_and_exp_data(strain_data,exp_data,options)
            else:
                app.logger.error("Error in main Query Method, no valid logically combined operator is provided")
                return ujson.dumps("Error")
        return ujson.dumps("Error")
    except Exception as e:
        app.logger.exception("Error in main Query Method, params (exp: %s, database: %s, exp_query: %s, exp_query_type: %s, strain_query: %s, strain_query_type: %s, show_substrains: %s, extra_data: %s) , error message: %s"%(exp, database, exp_query, exp_query_type, strain_query, strain_query_type, show_substrains, extra_data,e.message))
        return ujson.dumps("Error")

@app.route("/sendToMicroReact", methods=['POST'])
def sendToMicroReact(debug=None) :
    def reviseField(df, regex, fld, default='') :
        columns = list(filter(lambda c: c.find('__') < 0, df.columns))
        field = list(filter(lambda c: re.search(regex, c.lower()), columns))
        if len(field) == 0 :
            df[fld] = default
        elif fld not in field :
            df = df.rename(index=str, columns={field[0]:fld} )
        return df
    try:
        if debug :
            import pickle
            tree, metaString, colors = pickle.load(open(debug, 'rb'))
        else :
            params = dict(request.form)
            tree, metaString, colors, name = params['tree'][0], params['metadata'][0], json.loads(params['colors'][0]), params['name'][0]

        metadata = pd.read_csv(StringIO(metaString), sep='\t', header=[0], dtype=str, na_filter=False)
        for fld, categories in colors.items() :
            if not fld.endswith('__color') :
                metadata[fld+'__color'] = metadata[fld].astype(str).map(lambda v: categories.get(v, '#FFFFFF'))

        metadata = metadata.rename(index=str, columns={'ID':'id'} )
        names = metadata['id'].values.astype(str)


        metadata = reviseField(metadata, r'latitude|^lat$', 'latitude', '')
        metadata = reviseField(metadata, r'longitude|^lon$', 'longitude', '')
        metadata = reviseField(metadata, r'year', 'year', '1')
        metadata = reviseField(metadata, r'month', 'month', '1')
        metadata = reviseField(metadata, r'day', 'day', '1')

        for c in ('year', 'month', 'day') :
            d = metadata[c].values
            sites = metadata[c].str.extract(r'(-*\d+)').astype(float).values.flatten()
            sites[np.isnan(sites)] = -1
            d[sites < 0] = ''
            metadata[c] = d

        columns = list(filter(lambda c: c.find('__') < 0, metadata.columns))
        geo_columns = list(filter(lambda c: re.match(r'village|town|city|locat|state|region|district|^site$|geograph', c.lower()), columns))
        country_column = list(filter(lambda c: re.match(r'country', c.lower()), columns))
        if len(geo_columns) or len(country_column) :
            toRun = []
            for index, strain in metadata.iterrows() :
                if np.any(strain[['latitude', 'longitude']] == '') :
                    address = ' '.join(strain[geo_columns].values[strain[geo_columns] != ''])
                    country = '' if len(country_column) <=0 else strain[country_column][0]
                    if len(address) or len(country) :
                        toRun.append([address, country, index])
            results = geoCoding(toRun)
            for (_, _, index), geocode in zip(toRun, results) :
                if geocode['Longitude'] :
                    metadata.at[index, 'longitude'], metadata.at[index, 'latitude'] = geocode['Longitude'], geocode['Latitude']

        column_ids = {c.lower():id for id, c in reversed(list(enumerate(metadata.columns)))}
        metadata = metadata[metadata.columns[sorted(column_ids.values())]]
        metaString = metadata.to_csv()
        
        f = tempfile.NamedTemporaryFile(dir='.', prefix='geocoding', delete=False)
        f.close()
        with open(f.name, 'w') as fout :
            fout.write(tree)
        
        try :
            tree = Tree(newick=f.name, format=0)
        except :
            tree = Tree(newick=f.name, format=1)
        finally:
            os.unlink(f.name)
        names = set(names)
        for node in tree.traverse(strategy="postorder") :
            if node.is_leaf() :
                if node.name not in names :
                    node.up.remove_child(node)
                    node.up = None
                    node.delete()
            elif len(node.children) < 2 :
                p, c = node.up, node.children[0]
                c.up = p
                if p is not None :
                    p.remove_child(node)
                    p.add_child(c)
                    node.up = None
                else :
                    tree = c
                node.delete()
        tree = tree.write(format=0)
        q = requests.post('https://microreact.org/api/project/', json=dict(tree=tree, data=metaString, name=name))
        return make_response(json.loads(q.text)['url'], 200)

    except Exception as e:
        return make_response(str(e), 500)


@main.route("send_microreact",methods=['GET','POST'])
def send_microreact():
    data = request.form.get("info")
    json = ujson.loads(data)
    snp_id=request.form.get("snp_id")     
    resp = requests.post("https://microreact.org/api/project", json=json)
    if resp.status_code==400:
        return ujson.dumps({"error":"true","log":resp.text})
    try:
        resp_data=ujson.loads(resp.text)
    except Exception as e:
        ser_resp='Error'
        if resp and resp.get('text'):
            app.logger.exception("Error while trying to load server response (%s)"%resp.text)
            ser_resp=resp.text
        else:
            app.logger.exception("Error while trying to load server response (%s)" % resp)

        return ujson.dumps({"error": "true", "log": ser_resp})

    #resp_data= {"url":"http://microreact.org/project/B1SA1huze","shortId":"B1SA1huze"}
    short_id = resp_data.get("shortId")
    url = resp_data.get("url")
    if short_id:
        snp = get_analysis_object(int(snp_id))
        snp.add_data({"microreact_data":
                                    {
                                         "url":url,
                                          "short_id":short_id                 
                                     }
                               })

    return resp.text
    #return ujson.dumps(resp_data)
    

@main.route("my_tree",  methods = ['GET','POST'])
@auth_login_required
def my_tree():
      
    return render_template("species/tree.html")

@main.route("warwick_mlst_legacy",  methods = ['GET','POST'])
def warwick_mlst_legacy():
      
    return render_template("warwick_mlst_legacy.html")

# WVN 22/8/17 - info page for old MLST site related support
@main.route("old_site_support/warwick_mlst_legacy_info",  methods = ['GET','POST'])
def warwick_mlst_legacy_info():
    return render_template("old_site_support/warwick_mlst_legacy_info.html")

# WVN 22/8/17 - allele/ ST page for old MLST site related support
@main.route("old_site_support/warwick_mlst_legacy_allele_st_query",  methods = ['GET','POST'])
def warwick_mlst_legacy_allele_st_query():
    return render_template("old_site_support/warwick_mlst_legacy_allele_st_query.html")

# WVN 22/8/17 - strain page for old MLST site related support
@main.route("old_site_support/warwick_mlst_legacy_strain_query",  methods = ['GET','POST'])
def warwick_mlst_legacy_strain_query():
    return render_template("old_site_support/warwick_mlst_legacy_strain_query.html")

# WVN 22/8/17 - downloads page for old MLST site related support
@main.route("old_site_support/warwick_mlst_legacy_downloads",  methods = ['GET','POST'])
def warwick_mlst_legacy_downloads_query():
    return render_template("old_site_support/warwick_mlst_legacy_downloads.html")

# WVN 30/10/17 - borrowed code from species/views.py
@main.route("get_swagger_ui_api_token",methods = ['GET','POST'])
def get_swagger_ui_api_token():
    try:
        jackUser = User.query.filter_by(username='jack').first()
        api_token = jackUser.generate_api_token(expiration=31536000)
        return json.dumps({"api_token":api_token});
    except Exception as e:
        #dbase.session.rollback()
        #dbase.session.close()
        app.logger.exception("Error in getting api token, error message: %s"%e.message)
        return json.dumps({"api_token":None});




@main.route("download_data", methods = ['GET', 'POST'])
def download_scheme_data():
    print  "From download ..11111."
    species = request.args.get('species', None)
    scheme = request.args.get('scheme',None )
    allele = request.args.get('allele', None)
    if not species or not scheme or not allele:
        return make_response('Field species, scheme, allele not specified ', 404)
    if current_user.is_authenticated():
        #check if there user is not AnonymousUserMixin user (registered user) before checking if the user is administrator (issue 218)
        if scheme == 'rMLST' and getattr(current_user, 'administrator') != 1:
            return make_response('You do not have access to rMLST data. Please visit pubmlst.org/rmlst/ for more information or contact keith.jolley_at_zoo.ox.ac.uk. Copyright  2010-2016, University of Oxford', 403)
    else:
        return make_response(
            'Please log in first as anonymous user does not have access to rMLST data. Please not also that administrator has only access to this section, Please visit pubmlst.org/rmlst/ for more information or contact keith.jolley_at_zoo.ox.ac.uk. Copyright  2010-2016, University of Oxford',
            403)

    
    base = "/share_space/interact/NServ_dump/"
    name = allele+".fasta.gz"    
    location = species+"."+scheme+"/"
    file_name = base+location+name
    
    if scheme is not 'rMLST':
        # K.M. 12/09/2018
        # added to get the poper link with the new improved ones
        # get the folder using the database and scheme
        folder = get_download_scheme_folder(species, scheme)
        if not folder:
            if allele == 'profiles':
                return redirect('http://enterobase.warwick.ac.uk/schemes/%s.%s/profiles.list.gz' %(species, scheme), code=302)
            else:
                return redirect('http://enterobase.warwick.ac.uk/schemes/%s.%s/%s.fasta.gz' %(species, scheme, allele), code=302)
        else:
            if allele == 'profiles':
                return redirect('http://enterobase.warwick.ac.uk/schemes/%s/profiles.list.gz' %folder, code=302)
            else:
                return redirect('http://enterobase.warwick.ac.uk/schemes/%s/%s.fasta.gz' %(folder,allele), code=302)

    if allele == 'profiles':
        name = 'profiles.list.gz'
        file_name = base+location+name
        name = '%s.%s.%s' %(species, scheme, name)
    if not os.path.exists(file_name):
        return "file_not_found"

    response = make_response(send_file(file_name,attachment_filename=name, as_attachment=True) )
#    response.headers['Content-Encoding'] = 'gzip'
    return response

@main.route("download_straindata", methods = ['GET', 'POST'])
def download_strain_data():

    species = request.args.get('species', None)
    if not species or species not in dbhandle.keys() :
        return make_response('Species not specified or species does not exist', 404)
    if check_permission(id, 'view_species', species): 
        return make_response('You do not have access to strain data in %s. Please contact us <enterobase@warwick.ac.uk> for access' %species, 403)

    base = "/share_space/interact/strain_dump/"
    name = species +".strains.gz"    
    file_name = base + name
    if not os.path.exists(file_name):
        return make_response("file_not_found", 404)
    return send_file(file_name,attachment_filename=name, as_attachment=True)
    #response = make_response(send_file(file_name,attachment_filename=name, as_attachment=True) )
#    response.headers['Content-Encoding'] = 'gzip'
    #return response




#Returns a dictionary of file names to status
#Not Uploaded, Uploaded No Metadada, Uploaded, Queued, Assembled

@main.route("get_user_read_info",methods = ['GET','POST'])
@auth_login_required
def get_user_read_info():
    species = request.args.get('species')
    #Processing
    #Changes duo to
    try:
        #file_info =db.session.query(UserUploads).filter(UserUploads.species==species,UserUploads.user_id==current_user.id,UserUploads.type=="reads",UserUploads.status<>"Assembled").all()
        file_info =db.session.query(UserUploads).filter(UserUploads.species==species,UserUploads.user_id==current_user.id,UserUploads.type=="reads").all()
        strain_to_reads = {}
        read_to_status={}
        strain_to_plat = {}
        for info in file_info:
            metadata = json.loads(info.data);
            strain_to_reads[metadata['strain']]=metadata['accession'].split(",");
            read_to_status[info.file_name]=info.status
            strain_to_plat[metadata['strain']]=metadata['seq_platform'].split(",")[0]
        return_list = []

        for strain in strain_to_reads:
            row={}
            row["strain"] = strain
            row['seq_platform']= strain_to_plat[strain]
            count =1
            for read in strain_to_reads[strain]:
                tag = "read_"+str(count)
                row[tag]=read
                row[tag+"_status"]=read_to_status.get(read)
                count+=1
            return_list.append(row)
        return json.dumps(return_list)
    except Exception as e:
        app.logger.exception("get_user_read_info failed for user %s, error message: %s"%(current_user.id, e.message))
        rollback_close_system_db_session()
        return json.dumps([])


@main.route("remove_user_read",methods = ['GET','POST'])
@auth_login_method(False)
def remove_user_read():
    species = request.args.get('species')
    read_names_text = request.args.get('read_names')
    if not species or not read_names_text:
        return "Error", 400
    try:
        read_names  = read_names_text.split(",")
        reads =db.session.query(UserUploads).filter\
        (UserUploads.species==species,UserUploads.user_id==current_user.id,UserUploads.type=="reads",UserUploads.file_name.in_(read_names)).all()
        for read in reads:
            db.session.delete(read)
        db.session.commit()
        return "OK",200
    except Exception as e:
        app.logger.exception("read_names_text failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return "Error", 503

    
#This method stores the metadata supplied by the user and 
#also updates the reads database

@main.route("store_temp_metadata",methods = ['GET','POST'])
@auth_login_method(False)
def store_temp_metadata():
    data = request.get_json();
    database = data.get('database')
    metadata = data.get('data')
    if not database or not metadata:
        return "Error", 400
    try:
        for row in metadata:
            reads = row['accession'].split(",")
            for read in reads:
                upload = UserUploads(user_id=current_user.id,species=database,file_name=read,status="Awaiting Upload",data=ujson.dumps(row),type="reads")
                db.session.add(upload)
        db.session.commit()
        return "OK",200
    except Exception as e:
        app.logger.exception("store_temp_metadata failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return "Error", 503




@main.route("get_experiment_details",methods = ['GET','POST'])
def get_experiment_details():
    """Returns a jsonified dictionary schemes (and experiments)  
       Expects a GET paramater  with the name of the database
    """
    database = request.args.get("database")


    dbase = get_database(database)
    if not dbase:
        app.logger.error("%s database is not found"%database)
        return ujson.dumps({"error": "true"})
    try:

        allowed_schemes=[]
        if current_user.is_authenticated():
            allowed_schemes = current_user.get_allowed_schemes(database)
        else:

            Schemes = dbase.models.Schemes
            schemes = dbase.session.query(Schemes.description).filter(Schemes.param['display'].astext == 'public').all()
            for scheme in schemes:
                allowed_schemes.append(scheme.description)
            
        experiments  = dbase.get_experiment_details(allowed_schemes)
      
        user_id=0
        if current_user.is_authenticated():
            user_id = current_user.id
        view_data = CustomView.get_records(user_id,database)
        view_data_shared_folders= CustomView.get_shared_view_in_shared_folders(user_id,database)
        custom_views=[]        
        for view in view_data:
            if view['type']== 'custom_view':
                custom_views.append([view['id'],view['name']])
        
        for view in view_data_shared_folders:
            if view['type']== 'custom_view':
                custom_views.append([view['id'],view['name']])        
        return ujson.dumps({"experiments":experiments,"custom_views":custom_views})
    except Exception as e:
        app.logger.exception("get_experiment_details failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return ujson.dumps({"error":"true"})
    
@main.route("save_user_preferences",methods = ['GET','POST'])
@auth_login_method(True)
def save_user_preferences():
    """Returns a jsonified dictionary schemes (and experiments)  
       Expects a GET paramater  with the name of the database
       
    """
    database = request.form.get("database");
    data_type = request.form.get("type");
    name = request.form.get("name");
    data = request.form.get("data");
    
    ##ading check to be sure that data is valid jon string and all has valid keys and values
    ## otherwise it will send an error and will not save it to the database
    
   

    if not database or not data_type or not data or not name:
        app.logger.error("save_user_preferences failed, database %s, datatype %s, data %s, and name %s are required"%(database, data_type, data, name))
        return json.dumps({"saved": "false"})
    admin = request.form.get("admin")
    user_id = current_user.id
    try:
        try:
            data_=json.loads(data)        
        except:
            raise Exception("data (%s) is not a vaid json string"%data)

        if not isinstance(data_, dict): 
            raise Exception("data (%s) is not a dict"%data)    

        for key, value in data_.items():
            if key==None or value==None:
                raise Exception ("%s key and/or %s value are not valid"%(key, value))
            if isinstance(value, dict):
                for col, val in value.items():
                    if col==None or val==None:
                        raise Exception("%s col and/or value %s is not valid for key %s"%(col, val, key))        
        
        if admin:
            if not current_user.administrator and not data_type=="public_workspace_folders" :
                return ujson.dumps("permission denied")
            user_id=0
       
        pref= db.session.query(UserPreferences).filter_by(type=data_type,database=database,
                                                          user_id = user_id,name=name).first()
        if not pref:
            pref = UserPreferences(type=data_type,database=database,
                                user_id =  user_id,name=name)
            db.session.add(pref)
        
        pref.data = data
        db.session.commit()
        return json.dumps({"saved":"true","id":pref.id})
    except Exception as e:
        app.logger.exception("save_user_preferences failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return json.dumps({"saved":"false"})


@main.route("synch_analysis_from_enterobase",methods = ['GET','POST'])
def synch_analysis_from_enterobase():
    analysis_id = request.args.get('analysis_id')
    if not analysis_id or not analysis_id.isdigit():
        return ujson.dumps({"msg": "Failed"})
    analysis_id = int(analysis_id)
    try:
        analysis = get_analysis_object(analysis_id)
        if not analysis:
            return ujson.dumps({"msg": "Failed"})
        changed_data = analysis.synch_metadata()
        return ujson.dumps({"msg":"OK","changed_data":changed_data})
    except Exception as e:
        app.logger.exception("synch_analysis_from_enterobase failed, error message: %s"%e.message)
        return ujson.dumps({"msg":"Failed"})        
    

@main.route("save_analysis_layout",methods = ['GET','POST'])
def save_analysis_layout():
    ret_obj={"msg":"OK","columns_updated":[]}
    if current_user.is_authenticated():
        user_id=current_user.id
    else:
        user_id=-1    
    try:
        layout_data=request.form.get("layout_data")
        metadata_options = ujson.loads(request.form.get("metadata_options"))
        current_metadata= request.form.get("current_metadata")
        custom_data  = ujson.loads(request.form.get("custom_data"))
        tree_type=request.form.get("tree_type")
        ms_id= request.form.get("id")
        synch =request.form.get("synch")
        ms = get_analysis_object(ms_id)
        permission=False
        if current_user.is_authenticated():            
            permission=ms.has_edit_permission(current_user)
            if not permission:
                permission = check_permission(current_user.id, 'edit_strain_metadata', ms.database)
        if not permission:
            return json.dumps("You do not have the permission to save the tree..")
        
    
        ms.save_layout(layout_data,tree_type)
        #add the custom to user

        ms.save_metadata(custom_data,metadata_options,current_metadata)

        if synch:
            synch_data={}
            for strain_id in custom_data:
                data = custom_data[strain_id]
                for key in data:
                    if metadata_options[key]['category']=="Custom":
                        synch_dict =synch_data.get(strain_id)
                        if not synch_dict:
                            synch_dict={}
                            synch_data[strain_id]=synch_dict
                        synch_dict[key]=data[key]
            if len(synch_data)>0 and synch:
                #I have added ms_id as the custom view id as the user shared his tree with a buddy
                # and has edit permission so he/she should be able to edit the values inside the metadata table inside the 
                # tree page for sutom column
                #K.M 15/5/2019
                columns_updated=CustomView.update_custom_data(ms.database,synch_data, custom_view_id=ms_id, user_id=user_id)                
                ret_obj["columns_updated"]=columns_updated
    except Exception as e:
        app.logger.exception("save_analysis_layout failed, request is: %s, error message: %s"%(request.form, e.message))
    return json.dumps(ret_obj)
    


@main.route("save_snp_list",methods=['POST'])
@auth_login_method(False)
def save_snp_list():
    try:
        data = ujson.loads(request.form.get("data"))
        headers = ujson.loads(request.form.get("headers"))
        database = request.form.get("database")
        name=request.form.get("name")
        
        params={
            "name":name,
            "user_id":current_user.id,
            "database":database,
            
        }
        snp_list= SNPList.createList(params,data,headers)
        return ujson.dumps({"success":True,"id":snp_list.id})
        
    except Exception as e:
        return ujson.dumps({"success":False})




@main.route("create_ms_tree",methods = ['GET','POST'])
@auth_login_method(False)
def create_ms_tree():
    try:
        #data['ST_list'] is list of assembly_ids,ST__barcodes,ST_ids
        data=request.get_json();
        params= {"name":data['name'],"user_id":current_user.id,"database":data['database']}
        ms =MSTree.create_ms_tree(params,data['ST_list'],data['task'],data['parameters'],data['workspace'])
        print "=>>>>>", params,data['ST_list'],data['task'],data['parameters'],data['workspace']
        ms.send_job()
        ret_value = ms.name+":"+str(ms.id)
    except Exception as e:
        app.logger.exception("Unable to process ms tree job")
        #I am not sure why the following two lines are added?
        rollback_close_system_db_session()
        #db.session.rollback()
        #db.session.close()
        return "Failed"
        
    return ret_value


@main.route("create_snp_project",methods = ['GET','POST'])
@auth_login_method(False)
def  create_snp_project():
    data = request.get_json();
   
    workspace_id =int(data.get("workspace"))
    database = data.get("database")

    user_id=current_user.id
    #check number of assembies
    if len(data['assembly_ids'])<4:
        return "At least 4 sequences are required"
    max_snps=app.config["ANALYSIS_TYPES"]['snp_project']['max_snp_number']
    if current_user.administrator:
        max_snps=100000
    else:
        value =db.session.query(UserPermissionTags).filter_by(user_id=current_user.id,species=database,field="max_snp_number").first()
        if value:
            max_snps=int(value.value)
    if len(data['assembly_ids'])>max_snps:
        return "Only %i sequences allowed. Please contact an administrator if you require more" % max_snps

    dbase = get_database(database)
    success = True
    try:
        #create the new project in user preferences
        params = {
            "name":data.get("name"),
            "user_id":current_user.id,
            "database":database,
        }
        del data['database']
        del data['name']
        del data['workspace']
        snp_project = SNPProject.create_snp_project(data,params,workspace_id)
        return_val = snp_project.name +":" + str(snp_project.id)
        Schemes = dbase.models.Schemes
        Lookup = dbase.models.AssemblyLookup
        Assemblies = dbase.models.Assemblies
        ref = dbase.session.query(Assemblies).filter_by(id=snp_project.ref_id).first()
        
        #check whether the refMasker has been called
        refmasker_scheme = dbase.session.query(Schemes).filter_by(description='ref_masker').first()
        
        refmasker_exists = dbase.session.query(Lookup).filter_by(assembly_id=snp_project.ref_id,scheme_id=refmasker_scheme.id).first()
        if not refmasker_exists:
            job=RefMaskerJob(scheme=refmasker_scheme,
                                          assembly_barcode=ref.barcode,
                                          assembly_filepointer=ref.file_pointer,
                                          workgroup="user_upload",
                                          priority=-9,
                                          database=database)
            job.send_job()
        #send refmapper
        success = snp_project.send_ref_mapper_job()
        #all refmapper gff files already exist
        if success == None:
            success = snp_project.send_ref_mapper_matrix_job()

    except Exception as e:
        app.logger.exception("Unable to process snp job %i" % snp_project.id)
        dbase.rollback_close_session()
        success=False
    if not success:
        return "Unfortunately There was an error creating the SNP Project. Please contact an administrator. "
    else:
        return "OK:"+return_val
    
@main.route("get_snp_profiles",methods = ['GET','POST'])
def get_snp_profiles():
    profiles = ujson.dumps("Error")
    project_id = request.form.get("project_id")
    try:
       
        up =db.session.query(UserPreferences).filter_by(id=project_id).first()
        data = ujson.loads(up.data)
        file_name = data['profiles']
        in_handle = open(file_name,"r")
        profiles = in_handle.read()
    
    except Exception as e:
        rollback_close_system_db_session()
    return profiles


@main.route("associate_new_reads",methods = ['GET','POST'])
def associate_new_reads():
    try:
        database=request.form.get("database")
        strain_id = request.form.get("strain_id")
        strain_name = request.form.get("strain_name")
        names = request.form.get("names")
        names_list = names.split(",")
        user_id = current_user.id
        append=request.form.get("append")
        for name in names_list:
            upload  = UserUploads(user_id=user_id,species=database,file_name=name,status="Awaiting Upload",type='reads')
            params={
                "seq_library": "Paired,Paired",
                "accession": names,
                "seq_platform": "ILLUMINA,ILLUMINA",
                "update_strain":"true",
                "strain_id":int(strain_id),
                "strain":strain_name,
                "append":append
            }
            upload.data =json.dumps(params)
            db.session.add(upload)
        
        db.session.commit()
        db.session.close()
        return json.dumps({"result":"success"})
    except:
        rollback_close_system_db_session()
        return json.dumps({"result":"error"})

@main.route("make_workspace_private",methods = ['GET','POST'])
@auth_login_method(True)
def make_workspace_private():
    if not current_user.administrator:
        return ujson.dumps({"status":"Denied","msg":"Permission Denied"})
    ws_id = request.form.get("ws_id")
    try:
        ws = get_analysis_object(ws_id);
        ws.make_private(True)
    except:
        return ujson.dumps({"status":"Error","msg":"There was an error"})
    return ujson.dumps({"status":"OK","msg":""})



@main.route("make_workspace_public",methods = ['GET','POST'])
@auth_login_method(True)
def make_workspace_public():
    user_id=current_user.id
    ws_id = request.form.get("ws_id")
    try:
        ws = get_analysis_object(ws_id);
        if user_id != ws.user_id:
            return ujson.dumps("Permission Denied")
        ws.make_public()
    except:
        return ujson.dumps("Unable to make workspace public")
    return ujson.dumps("OK")

@main.route("delete_workspaces",methods = ['GET','POST'])
@auth_login_method(True)
def delete_workspaces():
    database = request.form.get("database");
    names_text = request.form.get("names");
    
    names= names_text.split(",")
    ws_info={}
    for name in names:
        try:
            ws = get_analysis_object(name)
            if current_user.id==ws.user_id or current_user.administrator:
                #delete and send email if admin is deleting some other persons workspace
                send_email = current_user.id !=ws.user_id
                ws.delete(send_email)
                ws_info[ws.id]=["deleted",ws.name]
                
            else:
                raise Exception("Permission Denied")
        except Exception as e:
            if ws:
                ws_info[ws.id]=["failed",ws.name]
            else:
                ws_info[str(name)]=["failed"]
            app.logger.exception("Could not delete workspace: %s, error message %s"%(str(name), e.message))
    return ujson.dumps(ws_info)

@main.route("rename_workspace",methods = ['GET','POST'])
@auth_login_method(True)
def rename_workspace():
    '''Renames a workspace.Returns the following jsonified object
    
        * **status**  either OK,Denied or Error
        * **url** /rename_workspace
    
        GET parameters
            * **workspace_id** The id of the workspace
            * **name** The new name of the workspace
        '''
    try:
        ws_id=request.args.get("workspace_id")
        new_name =request.args.get("name")
        ws = get_analysis_object(ws_id)
        if not ws:
            raise Exception("Could not find record for ws with id %s"%ws_id)
        if current_user.id == ws.get_owner() or current_user.administrator:
            ws.rename(new_name)
            return ujson.dumps({"status":"OK","msg":""})
        return ujson.dumps({"status":"Denied","msg":"You do not own this workspace"})
    except Exception as e:
            app.logger.exception("Could not rename workspace:"+str(ws_id))
            return ujson.dumps({"status":"Error","msg":"There was an error, please contact an administrator"})

  
   
    return ujson.dumps(ws_info)

        
@main.route("update_workspace",methods = ['GET','POST'])
@auth_login_method(True)
def update_workspace():
    database = request.form.get("database");
    wsid = int(request.form.get("name"));
    data =request.form.get("data")
    ws = get_analysis_object(wsid)
    if not  ws.has_edit_permission(current_user):
        return ujson.dumps("You do not own this workspace")    
  
    ws.update_view(ujson.loads(data))
    return ujson.dumps("OK")


@main.route("create_workspace",methods = ['GET','POST'])
@auth_login_method(True)
def create_workspace():
    try:
        database = request.form.get("database");
        name  =request.form.get("name")
        data =ujson.loads(request.form.get("data"))
        params={"name":name,"user_id":current_user.id,"type":"main_workspace","database":database}
        ws= WorkSpace(params=params,data=data)
        return ujson.dumps({"saved":"true","id":ws.id})
    except Exception as e:
        return ujson.dumps({"saved":"false"})


@main.route("get_workspace_summary",methods=['GET'])
def get_workpace_summary():
    ws_id = request.args.get("id")
    ws = get_analysis_object(ws_id)
    if not ws:
        return ujson.dumps ({"status": "failed"})
    summary = ws.get_summary()
    summary['edit_permissions']="false"
    if ws.has_edit_permission(current_user):
            summary['edit_permissions']="true"    
    return ujson.dumps(summary)


@main.route("save_workspace_summary",methods=['GET','POST'])
@auth_login_method(True)
def save_workspace_summary():
    try:
        '''Updates information about the analysis object e.g. description, links.
        Returns the following jsonified object
    
        * **status**  either OK,Denied or Error
        * **msg** The message to display to the user

        * **url** /rename_workspace

        POST parameters
            * **ws_id** The id of the workspace
            * **links** A jsonified dictionary of link_name:link_href
            * **trees_to_delete** a jsonified list of 'tree_name:tree_id'
            * **description** the text contaning the description
        '''
        ws_id = request.form.get("ws_id")
        ln = request.form.get("links")
        ws = get_analysis_object(ws_id)
        if not ws:
            raise Exception("Workspace is None for id: %s"%ws_id)
        if current_user.id  != ws.get_owner() and  not current_user.administrator:
            return ujson.dumps({"status":"Denied","msg":"You do not own this workspace"})
        links=None
        if ln:
            links = ujson.loads(ln)
        description= request.form.get("description")
        trees=request.form.get("trees_to_delete")
        trees_to_delete=None
        if trees:
            trees_to_delete = ujson.loads(request.form.get("trees_to_delete"))
        ws.save_summary(description,links,trees_to_delete)
    except Exception as e:
        app.logger.error("save_workspace_summary failed, error message: %s"%e.message)
        return json.dumps({"status":"Error","msg":"Error in Saving Workspace Summary"})
    return json.dumps({"status":"OK","msg":""})
    



@main.route("add_ids_to_workspace",methods = ['GET','POST'])
@auth_login_method(True)
def add_ids_to_workspaces():
    database = request.form.get("database");
    name = request.form.get("name");
    
    ids = request.form.get('strain_ids')
    id_list=[]
    if ids:
        id_list = ids.split(",")
        id_list =  map(int, id_list)
   
    try:
        ws = get_analysis_object(int(name))        
        if not  ws.has_edit_permission(current_user):
            return ujson.dumps("You do not own this workspace")
        if not ws.data.get('data') or not ws.data.get('data').get('strain_ids'):
            return ujson.dumps("no data is provided")
        strain_ids = ws.data['data']['strain_ids']
        for  sid in id_list:
            if not sid in strain_ids:
                strain_ids.append(sid)
        ws.update()
        db.session.commit()
        db.session.close()
        return json.dumps("Success")
        
    except Exception as e:
        app.logger.exception("add_ids_to_workspaces failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        #b.session.rollback()
        #b.session.close()
        return json.dumps("Error")


@main.route("load_user_preferences",methods = ['GET','POST'])
def load_user_preferences():
    """This method is not for workspaces (which should be accessed through their id)
    but for user preferences , which have unique name / type combinations e.g grid_parameters,
    main_query. it returns a jsonified dictionary of data
    Expects  the following  POST paramaters
    
    * name the name of the preference
    * database - the name of the database
    * type - the preference type
    
    The id of the user is taken friom current_user otherwise 'Error' is returned
    """
    database = request.form.get("database");
    data_type = request.form.get("type");
    name = request.form.get("name");    
    if not data_type or not database or not name:
        app.logger.error("Error in load_user_preferences, getting user preferences:%s:%s:%s" % (database, data_type, name))
        return json.dumps("Error")
    try:

        if not current_user.is_authenticated():
            return ujson.dumps("Error") 
        user_id=current_user.id
        pref= db.session.query(UserPreferences).filter_by(type=data_type,database=database,
                                                          user_id = user_id,name=name).first()
        if not pref:
            return json.dumps("Error")
        else:
            return pref.data
    except Exception as e:
        app.logger.exception("Error in load_user_preferences, erro message: %s"%e.message)
        rollback_close_system_db_session()
        return json.dumps("Error")
        
    
    
@main.route("admin_list",methods = ['GET','POST'])
def admin_list():
    database = request.form.get("database");
    dbase=get_database(database)
    try:
        name = request.form.get("name");
        user_id=current_user.id
        Schemes = dbase.models.Schemes
        pref = dbase.session.query(Schemes.pipeline_scheme).all()
        pref.append('QEvaluation')
        if len(pref) < 1:
            return json.dumps("Not Found")
        else:
            return json.dumps(pref)
    except Exception as e:
        app.logger.exception("admin_list failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return json.dumps("Error")
 
 
 #*********Custom View Functions******************************** 
@main.route("get_custom_view_columns",methods=["GET"])
def get_custom_view_columns():
    view_id = request.args.get("view_id")
    custom_only = request.args.get("custom_only")
    if not view_id or not view_id.isdigit():
        return ujson.dumps({"msg": "Error"})
    view_id = int(view_id)

    try:
        view = db.session.query(UserPreferences).filter_by(id=view_id).one()
        data = ujson.loads(view.data)
        database=view.database
        columns= []
        if not custom_only:
            columns=data.get('experiment_columns',[])
        for column in columns:
            column['editable']=False
            column['name']=column['name']+"_"+column['scheme']
            if not column.get("non_locus"):
                column['description']="locus"
        custom_columns = data.get("custom_columns")
        if custom_columns:
            col_ids=[]
            id_to_order={}
            for item in custom_columns:
                col_ids.append(item["id"])
                id_to_order[int(item['id'])]=item['display_order']
            column_info = db.session.query(UserPreferences).filter(UserPreferences.id.in_(col_ids)).all()
            for column in column_info:
                data = ujson.loads(column.data)
                data['name']=column.id
                data['display_order']=id_to_order[column.id]
                data['description']="custom_column"
                columns.append(data)
        return_obj = {"columns":columns,"categories":data.get('categories')}
        #get the saved layout data for this custom grid
        if not custom_only and current_user.is_authenticated():
            layout = db.session.query(UserPreferences).filter_by(user_id=current_user.id,type='grid_layout',name=str(view_id),database=database).all()
            if len(layout)==0:
                return_obj['layout']=None
            else:
                return_obj['layout']=ujson.loads(layout[0].data)
        return ujson.dumps(return_obj)
    except Exception as e:
        app.logger.exception("get_custom_view_columns failed, error message: %s"%e.message)
        db.session.rollback()
        return ujson.dumps({"msg": "Error"})



@main.route("get_custom_view_information",methods=['GET'])
def get_custom_view_information():    
    database=request.args.get("database")
    user_id=0
    if current_user.is_authenticated():
        user_id=current_user.id
           
    data= {"custom_view":{},"custom_column":{}}
    records= CustomView.get_records(user_id,database)
    for record in records:
        data[record["type"]][record['id']]=record['name']
    return ujson.dumps(data)

@main.route("add_custom_column",methods=['GET','POST'])
@auth_login_method(True)
def add_custom_column():
    '''Adds the custom column to the user preferences_table.Returns a dictionary of

    * **id**  the id of the column
    * **msg** OK if the column was added

    * **url** /add_custom_column

    GET parameters
        * **datatype**  integer,text or double
        * **database** the database 
        * **label** the name of the column
    '''
    datatype = request.form.get("datatype")
    database = request.form.get("database")
    if not datatype and not database:
        return ujson.dumps({"id":None, "msg":"Error"})
    try:
        #no check for previous lable, I think it may matter in case of having multible custom column at the same custom view
        label = request.form.get("label")
        column = UserPreferences(name=label,user_id=current_user.id,type="custom_column",database=database)
        column.data = ujson.dumps({"label":label,"datatype":datatype})
        db.session.add(column)
        db.session.commit()
        return ujson.dumps({"id": column.id, "msg": "OK"})
    except Exception as e:
        app.logger.exception("add_custom_column failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return ujson.dumps({"id": None, "msg": "Error"})


@main.route("save_custom_view",methods=['GET','POST'])
@auth_login_method(True)
def save_custom_view():
    '''Saves the custom view
    
    * **id**  the id of the view
    * **msg** OK if the view was saved, Exists if the name already exists

    * **url** /save_custom_view

    GET parameters
        * **id**  If supplied the custom view with this id will be overwritten
        * **database** the database 
        * **data**  the data for the custom view in the following format
            * **experiment_columns** consists of list of entries containing the following
                * **name**: the name of the field (as specified in dataprams)
                * **scheme**: the name of the scheme (the description in the schemes table)
                * **datatype**: either text,integer or double
                * **label**: what the user will see
                * **non_locus**: should be "true". Can be omitted if the column is an allele
                * **display_order**: the order in the table
            * **custom_columns** a list of a the following
                * **id**: the id (in the user_preferences table) of the custom column
                * **display_order** :the order in the table
    '''
    try: 
        data = request.form.get("data")
        if not data:
            return ujson.dumps({"msg": "Error"})

        database = request.form.get("database")
        view_id = request.form.get("id")
        name = request.form.get("name")
        #create new
        if view_id==None or view_id=='0':
            existing = db.session.query(UserPreferences.id).filter_by(type="custom_view",database=database,name=name).all()
            if  len(existing) <> 0:
                return ujson.dumps({"msg":"Exists"});
            view = UserPreferences(name=name,user_id=current_user.id,type="custom_view",data=data,database=database)
            db.session.add(view)
            db.session.commit()
            return ujson.dumps({"id":view.id,"msg":"OK"})
        elif view_id.isdigit():            
            view_id = int(view_id)
            view = db.session.query(UserPreferences).filter_by(id=view_id).one()
            permission=False
            if current_user.is_authenticated():                
                user_id=current_user.id                                                                
                permission=has_edit_permission_analysis_item(view_id, user_id, database)
            if permission:
                view.data = data
                db.session.commit()
                return ujson.dumps({"msg":"OK"})
            else:                          
                return ujson.dumps({"msg": "Permission Error"})                                
        else:
            return ujson.dumps({"msg": "Error"})
    except NoResultFound as e:
        app.logger.error("save_custom_view failed,, error message: %s" % e.message)
        rollback_close_system_db_session()
        return ujson.dumps({"msg": "Error"})
    except Exception as e:
        app.logger.exception("save_custom_view failed,, error message: %s"%e.message)
        rollback_close_system_db_session()
        return ujson.dumps({"msg":"Error"})
 
 
@main.route("get_custom_view",methods=['GET','POST'])
def get_custom_view():
    view_id = request.args.get("view_id")
    if not view_id or not view_id.isdigit():
        return ujson.dumps("Error")
    view_id=int (view_id)
    try:
        view = db.session.query(UserPreferences).filter_by(id=view_id).one()
        data = ujson.loads(view.data)
        columns=[]
        custom_columns = data.get('custom_columns')
        experiment_columns= data.get('experiment_columns')
        if experiment_columns:
            for column in experiment_columns:
                columns.append(column)
        if custom_columns:
            id_list=[]
            custom_column_info={}
            for column in custom_columns:
                id_list.append(int(column['id']))
            column_info = db.session.query(UserPreferences).filter(UserPreferences.id.in_(id_list)).all()
            for info in column_info:
                custom_column_info[info.id]=ujson.loads(info.data)
            for column in custom_columns:
                col_id = int(column['id'])
                info = custom_column_info[col_id]
                col ={"name":col_id,"scheme":"custom_column","label":info['label'],
                      "datatype":info['datatype'],"display_order":column['display_order']}
                columns.append(col)
        return ujson.dumps(columns)
       
    except Exception as e:
        app.logger.exception("get_custom_view failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return ujson.dumps("Error") 
 
 
@main.route("update_custom_data",methods = ['GET','POST'])  
def update_custom_data():
    try:
        
        if current_user.is_authenticated():
            user_id=current_user.id  
        else:
            return ujson.dumps({"msg":"Error"})
        
        database = request.form.get("database")
        data=ujson.loads(request.form.get("data")) 
        custom_view_id=request.form.get("custom_view_id")        
        columns_updated=CustomView.update_custom_data(database,data,custom_view_id=custom_view_id,user_id=user_id)
        return ujson.dumps({"msg":"OK", "updated":columns_updated})
    #There is no a way to check the returned value from the front end, it always checks for 'msg:'OK' in the returned dictionary 
    #Otherwise it will display error "Standard error message which does not indicates everything"
    #A check should be done and display the error message wihich is returned from the backend
    #if nothing is updated
    except:
        app.logger.exception("Error updating custom data")
        return ujson.dumps({"msg":"Error"})
    
#gets data for custom/experimental columns
@main.route("get_custom_column_data",methods = ['POST'])
def get_custom_column_data():
    database = request.form.get("database")
    dbase=get_database(database)
    strain_ids = request.form.get("strain_ids")
    columns= ujson.loads(request.form.get('columns'))
    ret_obj = {}
    if not dbase or not columns or not strain_ids:
        app.logger.error("get_custom_column_data failed, database, columns and strain ids are required, database: %s, columns: %s, strain ids: %s"%(database, columns, strain_ids))
        return ujson.dumps(ret_obj)
    sql = 'SELECT best_assembly,id FROM strains WHERE id IN (%s)' % strain_ids
    try:
        results = dbase.execute_query(sql)
        aid_to_id = {}


        for result in results:
            aid_to_id[result['best_assembly']]=result['id']
        aids = ",".join(map(str,aid_to_id.keys()))

        data,aids= process_custom_view_query(database,None,aids,"assembly_ids",columns)
        for aid in aids:
            ret_obj[aid_to_id[aid]]=data[aid]
        return ujson.dumps(ret_obj)

    except Exception as e:
        app.logger.error("get_custom_column_data failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return ujson.dumps({})

        
        
    
   
    

#http://137.205.123.127/ET/NServ/search.api/clostridium/Griffiths_MLST/alleles?filter=allele_id%20IN%20(2,3)%20and%20locus%20=%20%27recA%27&fieldnames=locus,seq
#locus searching stuff
#loci comma delimited list of loci
#strain_alleles dictionary of strain id to list 
@main.route("get_concatanated_loci",methods = ['GET','POST'])
def get_concatanated_loci():
    database = request.form.get("database")
    loci = request.form.get("loci")
    strain_alleles = ujson.loads(request.form.get("strain_alleles"))
    loci_list = loci.split(",")
    alleles_to_get={}
    for locus in loci_list:
        alleles_to_get[locus]=set()
        
    for strain_id in strain_alleles:
        allele_list = strain_alleles[strain_id]
        

       
@main.route("get_allele_view_info",methods = ['GET'])
def get_allele_view_info():
    database = request.args.get('database')
    allele_id = int(request.args.get('allele_id'))
    locus = request.args.get('locus')
    scheme = request.args.get('scheme')
    if not database or not allele_id or not locus or not scheme:
        app.logger.warning("get_allele_view_info failed, no enough args are provide")
        return ujson.dumps({})
    dbase = get_database(database)
    try:
        scheme_info=dbase.session.query(dbase.models.Schemes).filter_by(description=scheme).one()
        locus_info = dbase.session.query(dbase.models.DataParam).filter_by(tabname=scheme,name=locus).one()
        arr = scheme_info.param['scheme'].split("_")
        url = "/search.api/"+arr[0]+"/"+arr[1]+"/alleles?"
        url =url + "allele_id=%i&locus=%s&fieldnames=fieldname,value" % (allele_id,locus)
        url = app.config['NSERV_ADDRESS'] +url
        resp = requests.get(url=url,timeout=app.config['NSERV_TIMEOUT'])
        data = ujson.loads(resp.text)
        sequence =data[0]['seq']
        return ujson.dumps({
            "locus_label":locus_info.label,
            "locus_description":locus_info.description,
            "scheme_label":scheme_info.name,
            "sequence":sequence

        })
    except Exception as e:
        app.logger.exception("get_allele_view_info failed, error mesage: %s"%e.message)
        dbase.rollback_close_session()
        return ujson.dumps({})
  
  
@main.route("get_allele_details_for_strain",methods=['GET','POST'])
def get_allele_details_for_strain():
    database = request.form.get("database")
    dbase=get_database(database)
    if not dbase:
        app.logger.error("get_allele_details_for_strain failed, database %s is not valid"%database)
        return ujson.dumps({})
    barcode =request.form.get('assembly_barcode')
    scheme = request.form.get("scheme")
    if not barcode or not scheme:
        app.logger.error("get_allele_details_for_strain failed, missing args barcode and/or scheme, barcode: %s, scheme: %s"%(barcode, scheme))
        return ujson.dumps({})

    assembly_id = decode_barcode(barcode)
    sql = "SELECT annotation FROM assembly_lookup INNER JOIN schemes ON assembly_lookup.scheme_id = schemes.id AND schemes.description = '%s' AND assembly_id = %i" %(scheme,assembly_id)
    try:
        loci = request.form.get("loci").split(",")
        ret_value={}
        results = dbase.execute_query(sql)
        lines= results[0]['annotation'].split("\n")
        for line in lines:
            arr = line.split("\t")
            if len(arr)==9:
                locus=arr[8].split("=")[-1]
                if locus in loci:
                    ret_value[locus]=[arr[0],arr[3],arr[4],arr[6]]
                    loci.remove(locus)
                    if len(loci)==0:
                        break
        return ujson.dumps(ret_value)
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("get_allele_details_for_strain failed, error message: %s"%e.message)
        return ujson.dumps({})




@main.route("check_for_jbrowse_annotation/<barcode>",methods=["GET","POST"])
def check_for_jbrowse_annotation(barcode):
    '''Checks to see if the genome of the supplied barcode has a jbrowse annotation
    GET parameters

        * database
        * barcode
        
    Returns (in json format), either success:true or not_ready:true
    '''    
    database = request.args.get("database")
    if not database or not barcode:
        app.logger.warning("check_for_jbrowse_annotation is failed, Database and barcode are neeeded to process")
        return ujson.dumps({"status": "failed"})
    dbase=get_database(database)
    try:
        if barcode.isdigit():
            barcode = int(barcode)
            ass= dbase.session.query(dbase.models.Assemblies).filter_by(id=barcode).one()
            barcode= ass.barcode
        else :
            ass= dbase.session.query(dbase.models.Assemblies).filter_by(barcode=barcode).one()
        if ass.other_data:
            data = ujson.loads(ass.other_data)
            if data.get("can_view"):
                return ujson.dumps({"success":"true"})
        return ujson.dumps({"not_ready":"true"})
    except Exception as e:
        app.logger.exception("check_for_jbrowse_annotation failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        return ujson.dumps({"status": "failed"})


 
@main.route("get_country_info",methods = ['GET','POST'])  
def get_country_info():
    return app.config['COUNTRY_INFO']
   
   

