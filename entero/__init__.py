from flask import Flask, redirect, flash, url_for
import config
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
import  flask_restful
from flask_admin import Admin
from werkzeug.contrib.profiler import ProfilerMiddleware
from flask_sqlalchemy import get_debug_queries
from sqlalchemy import event, exc, select
from sqlalchemy.engine import Engine
import time
import sys
import logging
import os
from celery import Celery
from celery.signals import worker_process_init
from flask import current_app
import yaml
import socket
import ujson




app = Flask(__name__)
db = SQLAlchemy()
login_manager = LoginManager()
bootstrap = Bootstrap()
dbhandle = {}
celery = Celery(__name__)
celery.conf.update(app.config)


@worker_process_init.connect
def celery_worker_init_db(**_):
    db.init_app(current_app)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    for query in get_debug_queries():
        if query.duration >= app.config['DATABASE_QUERY_TIMEOUT']:
            app.logger.error(
                "SLOW ORM QUERY: %s\nParameters: %s\nDuration: %fs\nContext: %s\n"
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response

@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement,
                          parameters, context, executemany):
    context._query_start_time = long(time.clock())
    if app.config['APP_DB_ECHO']:
        app.logger.debug("Start Query (Parameters):\n%s\t%s"
                         % (statement, parameters, ))

@event.listens_for(Engine, "connect")
def connect(dbapi_connection, connection_record):
    connection_record.info['pid'] = os.getpid()

@event.listens_for(Engine, "checkout")
def checkout(dbapi_connection, connection_record, connection_proxy):
    pid = os.getpid()
    if connection_record.info['pid'] != pid:
        connection_record.connection = connection_proxy.connection = None
        raise exc.DisconnectionError(
                "Connection record belongs to pid %s, "
                "attempting to check out in pid %s" %
                (connection_record.info['pid'], pid)
        )


@event.listens_for(Engine, 'invalidate')
#psycopg2.extensions.connection
#_ConnectionRecord
def receive_invalidate(dbapi_connection, connection_record, exception):
    app.logger.exception("invalidate database connection, error message: %s"%exception.message)
    #Re-Connecting to species databases'
    for dbname, db_values in app.config['ACTIVE_DATABASES'].iteritems():
        conn = __import__('entero.databases.%s' % dbname, fromlist=[dbname])
        d = conn.DB(dbname, db_values[3],
                    db_values[1], app.config['SQLALCHEMY_ECHO'])
        dbhandle[dbname] = d
    app.logger.info('Re-Connected to species databases')
    flash("The database is not available now, please try again.")
    return redirect(url_for('main.index'))

@event.listens_for(Engine, "engine_connect")
def ping_connection(connection, branch):
    if branch:
        # "branch" refers to a sub-connection of a connection,
        # we don't want to bother pinging on these.
        return

    try:
        # run a SELECT 1.   use a core select() so that
        # the SELECT of a scalar value without a table is
        # appropriately formatted for the backend
        connection.scalar(select([1]))
    except exc.DBAPIError as err:
        # catch SQLAlchemy's DBAPIError, which is a wrapper
        # for the DBAPI's exception.  It includes a .connection_invalidated
        # attribute which specifies if this connection is a "disconnect"
        # condition, which is based on inspection of the original exception
        # by the dialect in use.
        if err.connection_invalidated:
            # run the same SELECT again - the connection will re-validate
            # itself and establish a new connection.  The disconnect detection
            # here also causes the whole connection pool to be invalidated
            # so that all stale connections are discarded.
            connection.scalar(select([1]))
        else:
            raise


@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement, parameters,
                         context, executemany):
    total = time.clock() - context._query_start_time
    if total >= app.config['DATABASE_QUERY_TIMEOUT']:
        app.logger.error("SLOW ENGINE QUERY: Total Time: %.02fms %s" % (total*1000, statement))

def load_logger(config_name):

    # Config logging and profilers
    log_level = logging.INFO # Change this for global logging level
    app.logger.setLevel(log_level)
    # WVN 23/3/18 Next two lines re-instated since otherwise they break during
    # testing - perhaps in production runs also?
    root = logging.getLogger()
    root.setLevel(log_level)

    log_format = logging.Formatter("%(asctime)s - %(levelname)s - %(funcName)s:%(lineno)d - %(message)s")
    if config_name == 'development' or config_name == 'windows_development':
        production_handler = logging.StreamHandler() #This is when Flask is DEBUG = False
    else:
        production_handler = logging.handlers.RotatingFileHandler(os.path.join('logs',"entero.log"), maxBytes=100000000)
        email_logger = logging.handlers.SMTPHandler(app.config['MAIL_SERVER'], 'enterobase@warwick.ac.uk', 'enterobase@warwick.ac.uk',
                                                   '[EnteroBase] Error %s' %socket.gethostname())
        email_logger.setLevel(logging.ERROR)
        app.logger.addHandler(email_logger)
        app.logger.error('Restarted server %s' %socket.gethostname())
    production_handler.setFormatter(log_format)
    app.logger.addHandler(production_handler)
    app.logger.setLevel(logging.INFO) 
    
    if app.config['PROFILE']:
        app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
    elif app.config['TESTING']:
        # WVN 9/2/18 Change to new path for tests directory
        # file_handler = logging.FileHandler(os.path.join('tests', 'log', "testing.log"))
        # WVN 12/3/18 This causes problems due to running directory used for pytest from within the Wing IDE
        #file_handler = logging.FileHandler(os.path.join('entero', 'tests', 'log', "testing.log"))
        file_handler = logging.FileHandler("testing.log")
        file_handler.setLevel(logging.INFO)

        formatter = logging.Formatter("%(relativeCreated)d - %(funcName)s - %(message)s")
        file_handler.setFormatter(formatter)
        app.logger.addHandler(file_handler)
        root.addHandler(file_handler)
        print ('Writing log to testing.log')
        
def get_country_info():
    continents={

        "AF" : "Africa",
        "AS" : "Asia",
        "EU" : "Europe",
        "NA" : "North America",
        "OC" : "Oceania",
        "SA" : "South America",
        "AN" : "Antarctica"

    }
    try:    
        with open("resources/countries.txt") as country_file :
            countries = country_file.readlines()
        country_cont = {}
        for line in countries:
            arr = line.strip().split("\t")
            country_cont[arr[0]]=continents[arr[1]]
        return ujson.dumps(country_cont)
    except Exception as e:
        app.logger.exception('Could not read countries file')

def generate_database_conns(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER,POSTGRES_PORT ):
    #check if the posrt is configure
    #if so it will added to the postgres server port
    #K.M 06/05/2019
    if POSTGRES_PORT:
        POSTGRES_SERVER=POSTGRES_SERVER+':%s'%POSTGRES_PORT
    #print "===>>>", POSTGRES_SERVER
        
    
    ACTIVE_DATABASES = {
        'miu': ['Dev. Sandbox','postgresql://%s:%s@%s/miu'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Salmonella'], 'MIU'],
        'listeria': ['Listeria','postgresql://%s:%s@%s/listeria'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Listeria'], 'LIS'], 
        'senterica': ['Salmonella','postgresql://%s:%s@%s/senterica'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Salmonella'], 'SAL'],
        'ecoli': ['Escherichia/Shigella','postgresql://%s:%s@%s/ecoli'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Escherichia', 'Shigella'], 'ESC'],
        'yersinia': ['Yersinia','postgresql://%s:%s@%s/yersinia'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True,  ['Yersinia'], 'YER'],
        'mcatarrhalis': ['Moraxella','postgresql://%s:%s@%s/mcatarrhalis'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Moraxella'], 'MOR'],
        #'mycobacterium': ['Mycobacterium','postgresql://%s:%s@%s/mycobacterium'%(USER, PASS, POSTGRES_SERVER), False, 1, 'MYC'],
        'klebsiella': ['Klebsiella','postgresql://%s:%s@%s/klebsiella'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Klebsiella'],'KLE'], 
        'clostridium': ['Clostridioides','postgresql://%s:%s@%s/clostridium'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Clostridioides'], 'CLO'], 
        #'neisseria': ['Neisseria','postgresql://%s:%s@%s/neisseria'%(USER, PASS, POSTGRES_SERVER), False, 1, 'NEI'],
        'photorhabdus': ['Photorhabdus','postgresql://%s:%s@%s/photorhabdus'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Photorhabdus', 'Xenorhabdus'], 'PHO'],
        'vibrio': ['Vibrio','postgresql://%s:%s@%s/vibrio'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Vibrio'], 'VIB'],
        'helicobacter': ['Helicobacter','postgresql://%s:%s@%s/helicobacter'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), True, ['Helicobacter'], 'HEL'],
        'bacillus': ['Bacillus','postgresql://%s:%s@%s/bacillus'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Bacillus cereus','Bacillus thuringiensis', 'Bacillus anthracis' , 'Bacillus weihenstephanensis', 'Bacillus subtilis'], 'BAC'],
        'virus': ['Virus','postgresql://%s:%s@%s/virus'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Herpesviridae'], 'VIR'],
        'treponema': ['Treponema','postgresql://%s:%s@%s/treponema'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Treponema'], 'TRE'],
        'tannerella': ['Tannerella','postgresql://%s:%s@%s/tannerella'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Tannerella'], 'TAN'],
        'porphyromonas': ['Porphyromonas','postgresql://%s:%s@%s/porphyromonas'%(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER), False, ['Porphyromonas'], 'POR']
    }
    return ACTIVE_DATABASES

#return the database from its name
def  get_database(dbname):
    #dbname=dbname.lower()
    if dbhandle.get(dbname):
        return dbhandle[dbname]
    else:
        return None

#rollback and close the connection in case of error
def rollback_close_connection(conn, cursor=None):
     try:
         conn.rollback()
         if cursor:
             cursor.close()
         conn.close

     except Exception as e:
         print "ERROR MESAGE WHILE CLOSING THE CONNECTION IS: %s"%e.message
         pass

 #in case or errors, this method is used to rollback and close the session
def rollback_close_system_db_session():
    try:
        db.session.rollback()
        db.session.close()
    except:
        pass

def create_app(config_name):
    # Load configuration from config.py
    print 'Loading config'
    running_config = config.configLookup[config_name]
    
    #Add country info to config 
    running_config.COUNTRY_INFO = get_country_info()
    
    # Inject instance specific config options
    print 'Injecting config options from %s' %running_config.ENTEROBASE_INSTANCE_CONFIG
    for x, y in yaml.load(open(running_config.ENTEROBASE_INSTANCE_CONFIG)).iteritems():
        setattr(running_config, x, y)
        
    # Format connection strings to System database
    #check if the postgres port os configured
    #if so it will added to the server address
    #K.M. 06/05/2019
    if hasattr(running_config, 'POSTGRES_PORT'):    
        address=running_config.POSTGRES_SERVER+':%s'%running_config.POSTGRES_PORT
    else:
        address=running_config.POSTGRES_SERVER
        
    running_config.SQLALCHEMY_DATABASE_URI =  'postgresql://%s:%s@%s/entero'\
        %(running_config.POSTGRES_USER, \
          running_config.POSTGRES_PASS, \
          address)
          #running_config.POSTGRES_SERVER)
    #print "Adress: ", address
    running_config.SQLALCHEMY_BINDS = dict(user_db = running_config.SQLALCHEMY_DATABASE_URI)
        
    app.config.from_object(running_config)
    
    # Start logging 
    load_logger(config_name)
    app.logger.info('Loaded config')
    
    bootstrap.init_app(app)
    app.logger.info('Loaded bootstrap framework')

    db.init_app(app)
    app.logger.info('Connecting to system database')
    app.app_context().push()

    first_user = db.session.execute('SELECT username from users limit 1').first()
    app.logger.info('Connected to system database, first user is %s' %first_user[0])
    
    login_manager.init_app(app)
    login_manager.session_protection = None
    login_manager.login_view = 'auth.login'

    # Create top level database connections
    import databases
    
    app.config['ACTIVE_DATABASES'] = generate_database_conns(app.config['POSTGRES_USER'], 
                                                            app.config['POSTGRES_PASS'], 
                                                            app.config['POSTGRES_SERVER'],
                                                            app.config.get('POSTGRES_PORT'))
    for dbname, db_values in app.config['ACTIVE_DATABASES'].iteritems():
        conn = __import__('entero.databases.%s' % dbname, fromlist=[dbname])
        d = conn.DB(dbname, db_values[3],
                    db_values[1], app.config['SQLALCHEMY_ECHO'])
        dbhandle[dbname] = d
    app.logger.info('Connected to species databases')
    # Admin module
   
    # WVN 7/3/18 Experimenting with skipping this in testing mode
    # because the extra stuff loaded up for the admin pages appears
    # to be slow and/ or cause some sort of deadlock when testing
    # (except we can still run tests in the usual production mode in
    # order to test the admin pages).  The deadlock is apparently
    # broken by running pytest on multiple processors or running
    # Wing, at least some of the time.
    if not app.config['TESTING']:
        from admin import views, adminbp
        admin = Admin(name='Enterobase-Admin')
        views.load(admin)
        admin.init_app(app)
        app.register_blueprint(adminbp, url_prefix='/admin')
        app.logger.info('Created admin pages')

    # Map blueprint modules
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix='/')
    
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .upload import upload as upload_blueprint
    app.register_blueprint(upload_blueprint, url_prefix='/upload')

    from .species import species as species_blueprint
    app.register_blueprint(species_blueprint, url_prefix='/species')
    app.logger.info('Created blueprint endpoints')
    # Load api
    from .api_1_0 import api_bp
    app.register_blueprint(api_bp, url_prefix='/api/v1.0/')
    import api_2_0
    from .api_2_0 import api_bp
    app.register_blueprint(api_bp, url_prefix='/api/v2.0/')
    app.logger.info('Created API 1 & 2 endpoints')
    
    return app


def get_download_scheme_folder(pipeline_species, pipeline_name):
    '''
    K.M 10/09/2018
    This methos is ued to get more logical folder names and easy to follow instaed of the ones in the databases
    the folder is set using 'ln' command in the physical hard desk
    :param pipeline_species:
    :param pipeline_name:
    :return: folder name
    '''
    #not in use list ===>Salmonella.cgMLSTv1, Salmonella.cgMLSTv4
    schemes_dict={'Salmonella.UoW':'Salmonella.Achtman7GeneMLST',
                  #'Salmonella.rMLST':'Salmonella.rMLST',
                  'SALwgMLST.cgMLSTv1':'Salmonella.cgMLSTv2',
                  'SALwgMLST.wgMLSTv1':'Salmonella.wgMLST',
                  'ESCwgMLST.wgMLSTv1': 'Escherichia.wgMLST',
                  'ESCwgMLST.cgMLSTv1':'Escherichia.cgMLSTv1',
                  'Escherichia.UoW':'Escherichia.Achtman7GeneMLST',
                  'CLOwgMLST.cgMLSTv1':'clostridium.cgMLSTv1',
                  'CLOwgMLST.wgMLSTv1':'clostridium.wgMLST',
                  'YERwgMLST.wgMLSTv1': 'Yersinia.wgMLST',
                  'YERwgMLST.cgMLSTv1' :'Yersinia.cgMLSTv1',
                  'Yersinia.UoW':'Yersinia.Achtman7GeneMLST',
                  'Moraxella.UoW':'Moraxella.Achtman7GeneMLST'
                  }
    folder= schemes_dict.get('%s.%s'%(pipeline_species, pipeline_name))

    return folder
