import pytest
# import flask

from entero.test_utilities import jack_user_id

# All of the tests in this class are slow (because of a fixture they all
# depend on; so the decorator is out here.
# Except it's looking like these are all being blamed for the app fixture
# being slow which pretty much every test depends on.
# @pytest.mark.slow
class TestGetAnalysisObjects():    
    @pytest.fixture(scope = "class")
    def get_analysis_objects_query1_res(self, app):
        from entero.ExtraFuncs.workspace import get_analysis_objects, get_analysis_object
        objs = get_analysis_objects(type = "ms_sa_tree", database = "senterica", user_id = jack_user_id)
        return objs
    
    @pytest.fixture(scope = "class")
    def get_analysis_objects_query1_res_1st_obj_id(self, get_analysis_objects_query1_res):
        return get_analysis_objects_query1_res[0].id
    
    @pytest.fixture(scope = "class")
    def single_object_1st_id_query_res(self, get_analysis_objects_query1_res_1st_obj_id):
        from entero.ExtraFuncs.workspace import get_analysis_object
        return get_analysis_object(get_analysis_objects_query1_res_1st_obj_id)

    # @pytest.mark.slow
    def test_get_analysis_objects_query1_not_none(self, get_analysis_objects_query1_res):
        assert get_analysis_objects_query1_res != None
        
    # @pytest.mark.slow
    def test_get_analysis_objects_query1_len_gt_0(self, get_analysis_objects_query1_res):
        assert len(get_analysis_objects_query1_res) > 0    

    # @pytest.mark.slow
    def test_get_analysis_objects_query1_res_1st_obj_has_id(self, get_analysis_objects_query1_res_1st_obj_id):
        assert get_analysis_objects_query1_res_1st_obj_id is not None
        
    # WVN 2/3/18 Currently a simple test for equality fails; because the equality operator (or rather __eq__ method) is not overloaded in
    # a way that is considered correct
    @pytest.mark.skip(reason = "Currently test of equality with an analysis object with an equivalent analysis object fails because equality operator not overloaded in way considered correct")
    def test_get_analysis_objects_query1_res_1st_obj_eq_to_single_obj_query(self, get_analysis_objects_query1_res, single_object_1st_id_query_res):
        assert get_analysis_objects_query1_res[0] == single_object_1st_id_query_res
        
    # @pytest.mark.slow
    def test_get_analysis_objects_query1_res_1st_obj_dict_eq_to_single_obj_query(self, get_analysis_objects_query1_res, single_object_1st_id_query_res):
        assert get_analysis_objects_query1_res[0].__dict__ == single_object_1st_id_query_res.__dict__
