# import unittest
import pytest
# import flask
#from flask import current_app
#import os

# WVN 12/2/18 Got rid off class related to xunit style tests since
# I am using fixtures

# WVN 12/2/18 - Database query test borrowed from Martin
def test_strainname(eb_dbhandle):
    dbase = eb_dbhandle["senterica"]
    Strains = dbase.models.Strains
    strain = dbase.session.query(Strains).filter_by(id=7464).one()
    assert strain.strain == "S01299_09-sc-2140602"
