from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app,jsonify, Flask
from flask_login import UserMixin
from entero import db, login_manager
from itsdangerous import BadSignature, SignatureExpired
from psycopg2.extras import RealDictCursor
import psycopg2
from sqlalchemy.dialects.postgresql import JSONB
from entero import app, rollback_close_connection, rollback_close_system_db_session


def query_system_database(sql):
        db_conn=None
        try:
                db_conn = psycopg2.connect(current_app.config['SQLALCHEMY_DATABASE_URI'])
                db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)     
             
                db_cursor.execute(sql) 
                results = db_cursor.fetchall()
                db_conn.close()
                return results
    
        except Exception as e:
                if db_conn:
                        db_conn.rollback()
                        db_conn.close()
                current_app.logger.exception("The following query failed:%s" % sql)
                return None
        
def execute_action(query):
        db_conn = None
        try:
                db_conn = psycopg2.connect(current_app.config['SQLALCHEMY_DATABASE_URI'])
                db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)     
                db_cursor.execute(query)                
                db_conn.commit()
                db_conn.close()          
        except Exception as e:
                if db_conn:
                        db_conn.rollback()
                        db_conn.close()
                        current_app.logger.exception("Error in performing query %s"%query)
                return False
        return True   
                
class UserInputFields(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_input_fields'
        id = db.Column("id", db.Integer, primary_key=True)
        name = db.Column("name", db.String)

        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class DatabaseInfo(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'database_info'
        id = db.Column("id", db.Integer, primary_key=True)
        name = db.Column("name", db.String)
        description = db.Column("description", db.String)
        prefix = db.Column("prefix", db.String)
        val = db.Column("val", db.Text)
        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}    

class UserJobs(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_jobs'
        id = db.Column("id",db.Integer, primary_key=True)
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id',ondelete="CASCADE"))
        status = db.Column("status",db.String)
        pipeline = db.Column("pipeline",db.String)
        date_sent = db.Column("date_sent",db.DateTime)
        database = db.Column("database",db.String)
        accession = db.Column("accession",db.String)
        output_location = db.Column("output_location",db.String)
        version = db.Column("version",db.String)
        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}    
        

class UserBuddies(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_buddies'
        id = db.Column("id",db.Integer, primary_key=True)
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id',ondelete="CASCADE"))
        buddy_id = db.Column("buddy_id",db.Integer)
        view_private = db.Column("view_private",db.Integer,default=1)
        edit_private= db.Column("edit_private",db.Integer,default=0)
        delete_private= db.Column("delete_private",db.Integer,default=0)
        edit_public= db.Column("edit_public",db.Integer,default=0)
        species = db .Column("species",db.String(20))

class UserPreferences(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_preferences'  
        id = db.Column("id",db.Integer, primary_key=True)
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id',ondelete="CASCADE"))
        data = db.Column("data",db.String)
        name = db.Column("name",db.String(200))
        type = db.Column("type",db.String(20));
        database = db.Column("database",db.String(20));

class UserPermissionTags(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_permission_tags'
        id= db.Column("id",db.Integer,primary_key=True)
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id',ondelete="CASCADE"))
        field= db.Column("field",db.String(300))
        species = db.Column("species",db.String(100))
        value = db.Column("value",db.String(200))
        user = db.relationship('User', backref=db.backref('permissions', lazy='dynamic'))
    
        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}    

class UserAutoCorrect(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_auto_correct'
        id= db.Column("id",db.Integer,primary_key=True)
        species = db.Column("species",db.String(100))
        field= db.Column("field",db.String(300))
        old_value = db.Column("old_value",db.String())
        new_value = db.Column("new_value",db.String())
        data = db.Column(JSONB)    

        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}    

class BuddyPermissionTags(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'buddy_permission_tags'
        id= db.Column("id",db.Integer,primary_key=True)
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id',ondelete="CASCADE"))
        buddy_id = db.Column("buddy_id",db.Integer)
        field= db.Column("field",db.String(300))
        species = db.Column("species",db.String(100))
        value = db.Column("value",db.String(200))
        user = db.relationship('User', backref=db.backref('buddy_permissions', lazy='dynamic'))
    

    

def check_permission(user_id, valid_perm, species):
        try:
                check = db.session.query(User).filter(User.id == user_id).one()
                if check.administrator == 1:
                        return True
                check = db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id == user_id).filter(UserPermissionTags.field==valid_perm).filter(UserPermissionTags.species==species).one()
                if check.value == 'True':
                        return True
                else:
                        return False
        except Exception as e:
                return False


class UserUploads(db.Model):
        __bind_key__ = "user_db"
        __tablename__ = 'user_uploads'
        id= db.Column("id",db.Integer,primary_key=True) 
        user_id = db.Column("user_id",db.Integer,db.ForeignKey('users.id'))
        species = db.Column("species",db.String(20))
        date_uploaded= db.Column("date_uploaded",db.DateTime, default=db.func.now())
        file_name= db.Column("file_name",db.String(100))
        file_location= db.Column("file_location",db.String(200))
        status= db.Column("status",db.String(60))
        type = db.Column("type",db.String(20))
        data = db.Column("data",db.String(20))

        def as_dict(self):
                return {c.name: getattr(self, c.name) for c in self.__table__.columns}


def get_dict(users,cols=None):

        if cols:
                result = [{col: getattr(d, col) for col in cols} for d in users]
                return result
        else:
                result =[]
                for u in users:
                        result.append(dict([(k, getattr(u, k)) for k in u.__dict__.keys() if not k.startswith("_")]))

                return result

class User(UserMixin, db.Model):
        __bind_key__ = 'user_db'
        __tablename__ = "users"
    
        id = db.Column("id",db.Integer, primary_key=True)
    
        email = db.Column("email",db.String(60), unique=True, index=True)
        username = db.Column("username",db.String(60), unique=True, index=True)
        city = db.Column("city",db.String(60), index=True)
        country = db.Column("country",db.String(60))
        firstname = db.Column("firstname",db.String(60))
        lastname = db.Column("lastname",db.String(60))
        password_hash = db.Column("password",db.String(128))
        confirmed = db.Column("confirmed",db.Integer, default=0)
        institution = db.Column("institution",db.String(60))
        department = db.Column("department",db.String(60))
        administrator = db.Column("administrator",db.Integer)
        active = db.Column("active",db.Boolean)
        buddies= db.relationship("BuddyPermissionTags",backref="user_buddies",uselist=True)
    
        def as_dict(self):
                cols = [] 
                for x in self.__table__.columns:
                        if x.name != 'password': cols.append(x)
                return {c.name: getattr(self, c.name) for c in cols}        

        def get_edit_meta_permissions(self,species):
                '''Get permission whether the user can edit strain metadata
                :param species: The name of the database
                :returns: A set of the ids of people that the user is buddies with and
                has the edit_strain_metadata permission. Also whether the user has 
                the right to curate all the strains in the database (True/False)
                '''
                allowed_users= set()
                permission = None
                my_buddies = BuddyPermissionTags.query.filter_by(buddy_id = self.id,species=species,field="edit_strain_metadata",value="True").all()
                for bud in my_buddies:
                        allowed_users.add(bud.user_id)
                allowed_users.add(self.id)
                permission = check_permission(self.id, 'edit_strain_metadata', species)
                if not permission and self.administrator==1:
                        permission = True
                return allowed_users,permission
        def get_allowed_schemes(self,species):
                allowed_list = []
                allowed =UserPermissionTags.query.filter_by(user_id=self.id,species=species,field='allowed_schemes').all()
                for rec in allowed:
                        allowed_list.append(rec.value)
                return allowed_list
        @property
        def password(self):
                raise AttributeError('password is not a readable attribute')

        @password.setter
        def password(self, password):
                self.password_hash = generate_password_hash(password)

        def verify_password(self, password):
                return check_password_hash(self.password_hash, password)
    

        def generate_confirmation_token(self, expiration=3600):
                s = Serializer(current_app.config['SECRET_KEY'], expiration)
                return s.dumps({'confirm': self.id})
    
        def generate_api_token(self, expiration=86400):
                s = Serializer(current_app.config['SECRET_KEY'], expiration)
                perms = {} 
                for res in UserPermissionTags.query.filter_by(user_id = self.id).all():
                        if res.field == 'api_access':
                                perms['%s_%s' %(res.field, res.species)] = res.value
                        else:
                                perms[res.field] = res.value
                x = self.as_dict().copy()
                x.update(perms)
                return s.dumps(x)
    

        @staticmethod
        def decode_api_token(token):
                s = Serializer(current_app.config['SECRET_KEY'])
                try:
                        data = s.loads(token)
                        return data            
                except SignatureExpired:
                        return None # valid token, but expired
                except BadSignature:
                        return None # invalid token
                return None

        @staticmethod
        def verify_auth_token(token):
                s = Serializer(current_app.config['SECRET_KEY'])
                try:
                        data = s.loads(token)
                except SignatureExpired:
                        return None # valid token, but expired
                except BadSignature:
                        return None # invalid token
                # WVN 5/4/17 "confirm" would be used to look up ID for a confirmation token.
                # However, the utility function used to verify confirmation tokens is getUserFromToken
                # not verify_auth_token which is used for API tokens.
                # user = User.query.get(data['confirm'])
                user = User.query.get(data['id'])
                return user

        @staticmethod
        def check_api_auth(request, database=None):
                try:
                        #print "I am checking the token ..", request.authorization.username
                        if request.headers.has_key('authorization') and request.authorization !=None:
                                data = User.decode_api_token(request.authorization.username)
                                is_authorized=False
                                if data is not None:   
                                        #adding this check to be sure that the api_aceess is not revoked
                                        user=db.session.query(User).filter(User.username==data['username']).first() 
                                        if not user :
                                                is_authorized=False
                                        elif user.administrator==1:
                                                is_authorized=True
                                        elif not database:
                                                is_authorized=True
                                        elif database != None:
                                                res=db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id==user.id).filter(UserPermissionTags.species==database).filter(UserPermissionTags.field=='api_access').filter(UserPermissionTags.value=='True').first()
                                                if res:
                                                        is_authorized=True
                                                                                     
                                if data is not None and is_authorized: 
                                        if data.has_key('api_access_%s' %database) or database == None or data.get('administrator') == 1:
                                                if ('rMLST' in request.url or request.url.startswith('user')) and data.get('administrator') != 1:
                                                        return  'You do not have access to these data.'
                                                return data
                                        else:
                                                return 'You are not authorised to access this database, please request access from us <enterobase@warwick.ac.uk>.'
                                return 'Invalid token, please confirm your token from http://enterobase.warwick.ac.uk/'
                        else:
                                return 'Please authenticate all requests using Basic-Auth and a valid token. Register at http://enterobase.warwick.ac.uk/ and request an API Token from us <enterobase@warwick.ac.uk>'
                except Exception as e:
                        current_app.logger.exception("Error while authenticate user request, error message: %s"%e.message)
                        return "Error while authenticate all request"
       
        def confirm(self):
                self.confirmed = 1
                #make sure confirm is updated in database
                db.session.commit()
                return True

        def generate_reset_token(self, expiration=3600):
                s = Serializer(current_app.config['SECRET_KEY'], expiration)
                return s.dumps({'reset': self.id})

        def reset_password(self, token, new_password):
                s = Serializer(current_app.config['SECRET_KEY'])
                try:
                        data = s.loads(token)
                except:
                        return False
                if data.get('reset') != self.id:
                        return False
                self.password = new_password
                db.session.add(self)
                return True

        def generate_email_change_token(self, new_email, expiration=3600):
                s = Serializer(current_app.config['SECRET_KEY'], expiration)
                return s.dumps({'change_email': self.id, 'new_email': new_email})

        def change_email(self, token):
                s = Serializer(current_app.config['SECRET_KEY'])
                try:
                        data = s.loads(token)
                except:
                        return False
                user_id = data.get('change_email')
                if (user_id <> self.id):
                        return False
                new_email = data.get('new_email')
                if new_email is None:
                        return False
                #does email exist
                if self.query.filter_by(email=new_email).first() is not None:
                        return False
                self.email = new_email
                db.session.add(self)
                return True

        def __repr__(self):
                return '<User %r>' % self.username

        def __str__(self):
                return self.username

   

@login_manager.user_loader
def load_user(user_id):
        try:
                return User.query.get(int(user_id))
        except:
                rollback_close_system_db_session()

