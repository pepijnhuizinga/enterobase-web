from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine, func, select, exists, text as sqlformat
from abc import ABCMeta, abstractmethod
import random, string
import psycopg2
from psycopg2.extras import DictCursor, RealDictCursor
from entero import app, rollback_close_connection
from entero.databases.system.models import User
# from history_meta import versioned_session
import os
import sys
import datetime
import json
import requests
from sqlalchemy.pool import QueuePool
from sqlalchemy.pool import NullPool


class AbstractDatabase(object):
    '''
    These should be private
    '''
    __metaclass__ = ABCMeta
    DATABASE_BIND = ''
    DATABASE_NAME = ''
    session = ''
    engine = ''

    def _conn(self):
        return psycopg2.connect(self.DATABASE_BIND)

    def __init__(self, name, description, bind, echo_db=False):
        self.DATABASE_NAME = name
        self.DATABASE_BIND = bind
        engine = create_engine(bind, convert_unicode=True, echo=echo_db, pool=QueuePool(
            self._conn,
            pool_size=1,
            max_overflow=3,
            timeout=30))
        self.engine = engine
        Session = sessionmaker(bind=engine)
        self.session = Session()
        self.session._model_changes = {}
        self.DATABASE_DESC = description
        self.models = __import__('entero.databases.%s.models' % name, fromlist=[name])
        self.db_code = app.config['ACTIVE_DATABASES'][name][4]
        self.table_codes = app.config['TABLE_CODE']

    def execute_query(self, query):
        conn = None
        cursor=None
        results = {}
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(query)
            results = cursor.fetchall()
            conn.close()
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
            app.logger.exception("Error in performing query %s, error message: %s" % (query,e.message))
            # return None
        return results

    def update_st_cache(self, SERVER, uppertime=5000, lowertime=2000):
        from sqlalchemy.orm.attributes import flag_modified

        try:
            Assemblylookup = self.models.AssemblyLookup
            Schemes = self.models.Schemes
            scheme_list = self.session.query(Schemes).filter(Schemes.param['pipeline'].astext == 'nomenclature').all()
            for scheme in scheme_list:
                if scheme.param['pipeline'] == 'nomenclature':
                    offset = 0
                    nserv_db_name = scheme.param['scheme'].split('_')[0]
                    nserv_scheme = scheme.param['scheme'].split('_')[1]
                    st_barcodes = []
                    for st_barcode, other_data in self.session.query(Assemblylookup.st_barcode,
                                                                     Assemblylookup.other_data).filter(
                            Assemblylookup.scheme_id == scheme.id).filter(Assemblylookup.other_data is not None).all():
                        if other_data:
                            if not other_data.get('results', {}).get('st_id'):
                                st_barcodes.append(str(st_barcode))
                        else:
                            st_barcodes.append(str(st_barcode))
                    chunks = [st_barcodes[x:x + 150] for x in xrange(0, len(st_barcodes), 150)]
                    for chunk in chunks:
                        # ?barcode=%s&fieldnames=ST_id,info,barcode
                        query_list = {}
                        for c in chunk:
                            query_list[c] = True
                        get_string = SERVER + '/search.api/%s/%s/%s?barcode=%s&fieldnames=type_id,info,barcode' % (
                        nserv_db_name, nserv_scheme, 'STs', ','.join(chunk))
                        try:
                            response = requests.get(get_string, timeout=5)
                            st_info = json.loads(response.text)
                            for st_record in st_info:
                                query_list.pop(st_record['barcode'])
                                assembly_recs = self.session.query(Assemblylookup).filter(
                                    Assemblylookup.st_barcode == st_record['barcode']).all()
                                for assembly_rec in assembly_recs:
                                    st_id, altered = assembly_rec.update_st_cache(st_record)
                                    if altered:
                                        self.session.add(assembly_rec)
                                        flag_modified(assembly_rec, 'other_data')
                            query_list.pop('None', None)
                            if len(query_list) > 0:
                                query_list.pop('None', None)
                                app.logger.warning('Barcodes not found %s ' % ','.join(query_list))
                        except requests.exceptions.ReadTimeout:
                            app.logger.error('Slow or no response for NSERV')
                        except Exception as e:
                            app.logger.error('Error updating ST, error message %s ' % e.message)
                    self.session.commit()
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in update_st_cache, error message: %s"% e.message)


    def encode(self, input_number, database, table_name, int_val=10000):
        number = input_number / int_val
        final_char = ''
        while number > 0:
            final_char += chr(number % 26 + 65)
            number = (number / 26)
        final_char = final_char.ljust(4, 'A')  #
        table_id = ''
        # WVN 11/5/17 Attempt at a fix for the strainsversion endpoint.  (I believe we do not want to append anything to the
        # assembly barcode for that either.)
        # if not table_name == 'strains':  table_id = '_%s' %self.table_codes[table_name]
        if not table_name == 'strains' and not table_name == 'strainsversion':  table_id = '_%s' % self.table_codes[
            table_name]
        return self.db_code + '_' + final_char[:2] + str(input_number % int_val).zfill(4) + final_char[2:] + table_id

    def decode(self, barcode, int_val=10000):
        prefix = barcode.split('_')[1][:2]
        suffix = barcode.split('_')[1][6:]
        char_string = (prefix + suffix).rstrip('A')
        int_string = 0
        for idx, cha in enumerate(char_string):
            int_string += 26 ** idx * ((ord(cha) - 65))
        int_string = int_string * int_val
        return int_string + int(barcode[6:10])

    # Database admin functions
    def create_db(self, create_schemes=True):
        self.models.Base.metadata.create_all(self.engine)
        self.create_dataparam()
        if create_schemes:
            self.create_schemes()

    def destroy_db(self):
        self.engine.execute("drop schema if exists public cascade")
        self.engine.execute("create schema public")
        self.models.Base.metadata.drop_all(self.engine)

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()

    def get_param(self, the_list, i):
        try:
            return the_list[i].strip()
        except IndexError:
            return None

    def get_dataparam(self, table_name):
        dat = []
        try:
            res = self.session.query(self.models.DataParam).filter_by(tabname=table_name).all()
            for r in res:
                dat.append(r.as_dict())
            # self.session.close()
            return dat
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_dataparam for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return []

    def clean_data(self, entries):
        if isinstance(entries, str):
            return [entries]
        if isinstance(entries, list):
            return entries
        else:
            raise TypeError

    def get_strain_fields(self):
        SQL = "SELECT name,label FROM data_param WHERE tabname = 'traces' OR tabname='strains'"
        results = self.execute_query(SQL)
        ret_dict = {}
        for result in results:
            ret_dict[result['name']] = result['label']
        return ret_dict

    def get_generic_scheme_breakdown(self, scheme, field, limit=30):
        try:
            param = self.session.query(self.models.DataParam).filter_by(tabname=scheme, display_order=1).first()

            SQL = "SELECT other_data #>> '{results,%s}' as val,count(assembly_lookup.id) AS num FROM assembly_lookup INNER JOIN schemes ON scheme_id = schemes.id AND schemes.description = '%s' GROUP BY val ORDER BY num DESC" % (
            param.name, scheme)
            results = self.execute_query(SQL)
            ret_list = []
            other = 0
            for count, result in enumerate(results, start=1):
                if count > limit:
                    other += result['num']
                else:
                    ret_list.append([result['val'], result['num']])
            ret_list.append(['other', other])

            return {"field_name": param.name, "field_label": param.label, "list": ret_list}

        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_generic_scheme_breakdown for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return {}

    # All the methods which calling the following one, expecting to return two entities, i.e. dict and list
    # but it returns only dict, it is needed to fix this method or the called ones depends on the context
    def get_st_breakdown(self, scheme, limit=30, field=None):
        conn = None
        param = None
        cursor=None
        try:
            if field:
                param = self.session.query(self.models.DataParam).filter_by(tabname=scheme, name=field).first()
            else:
                param = self.session.query(self.models.DataParam).filter_by(tabname=scheme, display_order=1).first()
            nested = param.mlst_field.split(",")

            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            sql = "SELECT st_barcode,COUNT(assembly_id) as num FROM strains INNER JOIN assembly_lookup ON best_assembly = assembly_lookup.assembly_id INNER JOIN schemes ON schemes.id=assembly_lookup.scheme_id AND schemes.description='%s' GROUP BY st_barcode ORDER BY num DESC" % (
                scheme)
            cursor.execute(sql)
            results = cursor.fetchall()
            barcode_list = []
            barcode_to_num = []
            other = 0
            for count, res in enumerate(results, start=1):
                if count > limit:
                    other += res['num']
                else:
                    barcode = res['st_barcode']
                    if not barcode:
                        continue
                    barcode_list.append(barcode)
                    barcode_to_num.append([barcode, res['num']])

            url = app.config['NSERV_ADDRESS'] + "/retrieve.api"
            resp = requests.post(url=url, data={"barcode": ",".join(barcode_list)},timeout=app.config['NSERV_TIMEOUT'])
            try:
                data = json.loads(resp.text)
            except:
                raise Exception("Could not load the data from server response: %s"%resp.text)
            if not type(data) is list:
                raise Exception("Error in getting barcodes from server response: %s " % resp.text)
            rec_dict = {}
            for record in data:
                field_value = record
                for nest in nested:
                    try:
                        nest = int(nest)
                    except:
                        pass
                    field_value = field_value[nest]
                rec_dict[record["barcode"]] = field_value
            ret_list = []
            for item in barcode_to_num:
                ret_list.append([rec_dict[item[0]], item[1]])
            ret_list.append(['other', other])
            conn.close()
            self.session.close()
            return {"field_name": param.name, "field_label": param.label, "list": ret_list}
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            self.session.close()
            app.logger.exception("Error in querying Scheme %s, with error message: %s" % (scheme, e.message))
            return {}

    # if no barcode in record will return the status e.g. COMPLETE,FAILED
    def get_barcodes_from_aids(self, assembly_ids, scheme):
        Schemes = self.models.Schemes
        scheme_info = self.session.query(Schemes).filter(Schemes.description==scheme).first()
        scheme_id = scheme_info.param['master_id'] if 'master_id' in scheme_info.param else scheme_info.id
        conn = None
        cursor=None
        #print "assembly_ids::", assembly_ids
        sql = "SELECT assembly_id,st_barcode,status FROM assembly_lookup WHERE assembly_lookup.assembly_id IN (%s) AND scheme_id=%s " % (
            assembly_ids, scheme_id)
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)

            cursor.execute(sql)
            results = cursor.fetchall()
            barcode_to_aid = {}

            for res in results:
                barcode = res['st_barcode']
                if not barcode:
                    continue
                    # barcode=res['status']
                aid_list = barcode_to_aid.get(barcode)
                if not aid_list:
                    aid_list = []
                    barcode_to_aid[barcode] = aid_list
                aid_list.append(res['assembly_id'])
            return barcode_to_aid

        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in querying Scheme %s, sql %s, error message: %s" % (scheme, sql, e.message))
            return {}

    def get_alleles_by_query(self, scheme, query):
        '''Processes a scheme query
        :param scheme: The description of the scheme
        :param query: An SQL WHERE clause e.g. dnaA =2 OR lineage=2
        :returns: A dictionary of assembly ids to a dictionary of allele information e.g. locus:allele,ST:18,lineage:27
        plus a list of assembly ids
        '''

        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)

            # get a list of the loci and the scheme id
            SQL = "SELECT loci.description as loc,sl.scheme_id AS sid FROM schemes AS sc INNER JOIN scheme_loci AS sl ON  sc.description = '%s' AND " + \
                  "sc.id = sl.scheme_id " + \
                  "INNER JOIN loci ON sl.loci_id = loci.id ORDER BY loc"
            SQL = SQL % scheme
            cursor.execute(SQL)
            results = cursor.fetchall()
            # build the query based on changing the hash into loci columns
            order = 1
            scheme_id = 0
            SQL = "SELECT * FROM (select "
            for row in results:
                item = "CAST (split_part(hash,',',%i) AS integer) AS \"%s\"," % (order, row['loc'])
                order = order + 1
                SQL += item
            scheme_id = row['sid']
            SQL = SQL + "legacy_id AS ST,st_complex,lineage,subspecies,assembly_id FROM sts INNER JOIN st_assembly ON sts.scheme_id=%i AND sts.id=st_assembly.st_id) AS foo WHERE " % scheme_id
            SQL = SQL + query
            cursor.execute(SQL)
            records = cursor.fetchall()
            ass_id_to_row = {}
            aids = []
            for row in records:
                aids.append(row['assembly_id'])
                ass_id_to_row[row['assembly_id']] = row
                del row['assembly_id']
            cursor.close()
            conn.close()
            return ass_id_to_row, aids
        except Exception as ex:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in querying Scheme %s, error message: %s" % (scheme, ex.message))
            return {}, ""

    '''gets the alleles and ST for a given scheme and genome ids
    scheme - The description of the scheme
    ids - a comma delimited list of genome ids
    returns - a dictionary of genome id to a dictionary of allele product:[legacy id,id,accepted(0 or 1)]
    and st:number
    e.g. {12736:{"purA":[33,192,1],"purB":[44,234,0],"st":2},1345:{"purA":[17,345,1]"purB":[21,344,1],"st"5}}

    '''

    def get_alleles(self, scheme, ids=None, query=None):

        if not ids:
            return {}
        con = None
        try:
            con = self.engine.connect()
            SQL = "SELECT loci.description as loc FROM schemes AS sc INNER JOIN scheme_loci AS sl ON  sc.description = '%s' AND " + \
                  "sc.id = sl.scheme_id " + \
                  "INNER JOIN loci ON sl.loci_id = loci.id ORDER BY loc"
            SQL = SQL % scheme
            results = con.execute(SQL)
            loci_list = []
            for res in results:
                loci_list.append(res['loc'])
            SQL = "SELECT hash,legacy_id,st_complex,lineage,subspecies,assembly_id FROM  sts INNER JOIN st_assembly ON assembly_id IN (%s) AND  st_id =sts.id " % ids
            results = con.execute(SQL)
            dict = {}
            for res in results:
                if not res['hash']:
                    continue
                res_dict = {}
                allele_ids = res['hash'].split(",")
                for (allele, loc) in zip(allele_ids, loci_list):
                    res_dict[loc] = [allele, 1, 1]
                res_dict["ST"] = res["legacy_id"]
                res_dict['st_complex'] = res['st_complex']
                res_dict['lineage'] = res['lineage']
                res_dict['subspecies'] = res['subspecies']
                dict[res['assembly_id']] = res_dict
            con.close()
            return dict
        except Exception as ex:
            if con:
                rollback_close_connection(con)
                #con.rollback()
                #con.close()
            app.logger.exception("Error in getalleles for Scheme %s, error message: %s" % (scheme, ex.message))
            return {}

    def get_aids_from_st_barcodes(self, barcodes, exp):
        pass

    def get_assembly_ids(self, strains_ids):
        '''Given a list of strain ids this method will return
        :param species: The name of the database
        :param query: An SQL WHERE clause e.g. name = 'bug1'  or a comma delimited list of accessions or
        ids depending on the query type
        In addition the following special values are allowed:
             - my_records - Only records the user owns (or has buddy permissions to do so will be returned)
             - all - Get all records
             - default -get last 200 records in order

        :param query_type: Can be one of the following:
            - query - A normal SQL WHERE clause
            - accessions - A comma delimited list of accessions
            - ids - A comma delimited list of strain IDs
        :param aids: A comma delimited string of assembly ids which will also be retrieved (optional)
        :returns: A list of metadata for the strain in form of a list of dictionaries

        '''
        con = None
        SQL = "SELECT strains.id as sid, strains.best_assembly AS gid FROM strains WHERE strains.id IN (%s) ORDER BY strains.id" % (
            strains_ids)
        try:
            con = self.engine.connect()
            results = con.execute(SQL)
            adict = {}
            gids = []
            for result in results:
                if result['gid'] == None:
                    continue
                gids.append(str(result['gid']))
                adict[result['sid']] = result['gid']
            con.close()
            return adict, gids
        except Exception as ex:
            if con:
                rollback_close_connection(con)
                #con.rollback()
                #con.close()
            app.logger.exception("Error in get assembly ids for %s, error message: %S" % (strains_ids, ex.message))
            return {}, []

    def get_trace_ids_by_accession(self, accessions):
        acc_ids = []
        for acc in accessions:
            # user_reads
            if "#" in acc:
                acc_ids.append(acc.split(":")[0])
            else:
                acc_ids.append(acc)
        list = []
        try:
            traces = self.session.query(self.models.Traces).filter(self.models.Traces.accession.in_(acc_ids)).all()

            for trace in traces:
                list.append(trace.id)
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_traceids_by_accession for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
        return list


    # Strains
    def exists(self, table, col, value):
        table = self.models.Base.metadata.tables[table]
        return self.session.query(exists().where(getattr(table.c, col) == value)).scalar()

    def get_id(self, table, col, value):
        try:
            table = self.models.Base.metadata.tables[table]
            result = self.session.query(table.c.id).filter(getattr(table.c, col) == value).first()
            if result == None:
                return None
            else:
                return result[0]
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_id for %s database in table %s, error message: %s"%(self.DATABASE_NAME, table, e.message))
            return None



    def update_record(self, currentuser, editRecord, table, new_record, commit=True):
        try:
            if not new_record:
                self.archive_index(table, editRecord.barcode, editRecord.version, commit)
                editRecord.version += 1
            editRecord.lastmodified = datetime.datetime.now()
            editRecord.lastmodifiedby = currentuser
            self.session.add(editRecord)
            if commit:
                self.session.commit()
                return editRecord

        except Exception as ex:
            app.logger.exception("Failed to update record for table %s, error message: %s" % (table, ex.message))
            self.rollback_close_session()
            return False

    def archive_index(self, table_name, unique_id, version, commit=True):
        try:
            table = self.models.Base.metadata.tables[table_name]
            col = []
            for c in list(table.c)[0:]:
                if c.name != 'id':
                    col.append(c.name)
            colname = ','.join(col)

            sql = 'INSERT INTO %s_archive (%s) SELECT %s FROM %s WHERE barcode = \'%s\' AND version = %d RETURNING id;' % (
            table_name, colname, colname, table_name, unique_id, version)
            # delete_sql = 'DELETE FROM %s WHERE index_id = \'%s\' AND version = %d' % (table_name, unique_id, version)
            self.session.execute(sql)
            # self.session.execute(delete_sql)
            if commit: self.session.commit()
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("error archive_index for database %s in table %s, eroor message: %s"%(self.DATABASE_NAME,table_name,e.message))

    def insert_traces(self, entries):
        keys = []
        try:
            entries = self.clean_data(entries)
            table = self.models.metadata.tables['traces']
            # Prlds for entry
            clean_rows = entries
            for row in clean_rows:
                result = self.session.execute(table.insert(), row)
                keys += (result.inserted_primary_key)
                app.logger.info('Inserted Genome ID:%i, accession:%s into %s'
                                % (result.inserted_primary_key[0], row.get('accession'), self.DATABASE_NAME))
            self.session.commit()
            return keys
        except Exception as ex:
            app.logger.exception("Failed to insert_traces for %s database, error message: %s" % (self.DATABASE_NAME, ex.message))
            self.rollback_close_session()
            return []

    def get_assembly_barcode_from_accession(self, accession):
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            SQL = "SELECT assemblies.barcode as bar FROM strains INNER JOIN assemblies ON strains.best_assembly= assemblies.id INNER JOIN trace_assembly ON trace_assembly.assembly_id = assemblies.id INNER join traces ON traces.id = trace_assembly.trace_id WHERE traces.accession= '%s'" % accession
            cursor.execute(SQL)
            result = cursor.fetchone()
            conn.close()
            if not result:
                app.logger.info("accession %s does not have best assembly" % accession)
                return None
            return result['bar']
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception(
                "Error in get assembly barcode for %s for % database %s, error message: " % (accession, self.DATABASE_NAME, e.message))
            return None

    def get_daily_increase(self, limit=3):
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            SQL = "SELECT  to_char(created,'YYYY-MM-DD') as day, COUNT(id) AS amount,contact  FROM strains GROUP BY day,contact ORDER BY day DESC LIMIT %i " % int(
                limit)
            cursor.execute(SQL)
            results = cursor.fetchall()

            ret_list = []
            for res in results:
                dat = datetime.datetime.strptime(res['day'], "%Y-%m-%d")

                day = dat.strftime("%d %B %Y")
                if res['contact']:
                    contact = unicode(res['contact'].decode("utf-8"))
                else:
                    contact = "unknown"
                ret_list.append([day, contact, res['amount']])
            conn.close()

            return ret_list

        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in get daily increase for %s database, error message: %s" %(self.DATABASE_NAME, e.message))

    def execute_action(self, query):
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(query)
            conn.commit()
            conn.close()
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in performing action %s, error message: %s" % (query, e.message))
            return False
        return True



    def get_cumulative_strain_number(self):
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            SQL = "SELECT  to_char(release_date,'YY-MM') as mmyy, COUNT(id) AS amount  FROM strains WHERE release_date < NOW() GROUP BY mmyy ORDER BY mmyy"
            cursor.execute(SQL)
            results = self.execute_query(SQL)
            results = cursor.fetchall()
            total = 0
            ret_list = []
            for res in results:
                total = total + int(res['amount'])
                ret_list.append([res['mmyy'], total])
            conn.close()
            return ret_list[1:]

        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in getting strain data, error message: %s" % e.message)
            return []

    def populate_db(self):
        direct = os.path.dirname(os.path.realpath(__file__))
        popfile = os.path.join(direct, self.DATABASE_NAME, 'populate.sql')
        if os.path.exists(popfile):
            pop_handle = open(popfile, 'r')
            for insert in pop_handle:
                try:
                    if len(insert) > 0 and not insert.startswith('--'): self.session.execute(insert.strip())
                except Exception as e:
                    app.logger.exception( 'Did not insert %s, error message: %s'% (insert, e.message))
                    self.rollback_close_session()
        self.create_dataparam()

    def create_dataparam(self):
        try:
            if self.models.Base.metadata.tables.has_key('data_param'):
                DataParam = self.models.DataParam
                # if self.session.query(DataParam).count() > 0:
                #   self.session.query(DataParam).delete()
                self.session.commit()
                direct = os.path.dirname(os.path.realpath(__file__))
                # first load the generic data params
                param_file = os.path.join(direct, 'data_params.txt')
                param_handle = open(param_file, 'r')
                pos_to_header = {}
                first = True
                for insert in param_handle:
                    if first:
                        headers = insert.strip().split('\t')
                        pos = 0
                        for header in headers:
                            pos_to_header[pos] = header
                            pos += 1
                        first = False
                    else:
                        in_array = insert.split('\t')
                        new_param = DataParam()
                        pos = 0
                        for val in in_array:
                            val = unicode(val.strip())
                            if not val:
                                pos += 1
                                continue
                            field = pos_to_header[pos]
                            setattr(new_param, field, val)
                            pos += 1
                        self.session.add(new_param)
                self.session.commit()
                param_handle.close()
                # Now load the specific data params, assumes that field name is second
                param_file = os.path.join(direct, self.DATABASE_NAME, 'data_params.txt')
                param_handle = open(param_file, 'r')
                pos_to_header = {}
                first = True
                for insert in param_handle:
                    if first:
                        headers = insert.split('\t')
                        pos = 0
                        for header in headers:
                            pos_to_header[pos] = header
                            pos += 1
                        first = False
                    else:
                        in_array = insert.strip().split('\t')
                        if len(in_array) < 2:
                            continue
                        new_param = self.session.query(DataParam).filter_by(name=in_array[1]).first()
                        if not new_param:
                            new_param = DataParam()
                        pos = 0
                        for val in in_array:
                            # don't want to overwrite with nothing
                            val = (val.strip())
                            if not val:
                                pos += 1
                                continue
                            field = pos_to_header[pos]
                            setattr(new_param, field, val)
                            pos += 1
                        self.session.add(new_param)
                self.session.commit()
                param_handle.close()
        except Exception as e:
            app.logger.exception('Error in create_dataparam, error message: %s' % (e.message))
            self.rollback_close_session()

    def create_schemes(self):
        try:
            import csv
            if self.models.Base.metadata.tables.has_key('schemes'):
                Schemes = self.models.Schemes
                direct = os.path.dirname(os.path.realpath(__file__))
                param_file = os.path.join(direct, 'generic_schemes.txt')
                with open(param_file, 'r') as param_handle:
                    data_rows = csv.DictReader(param_handle, dialect=csv.excel)
                    for row in data_rows:
                        row.pop('lastmodified')
                        row.pop('created')
                        row.pop('version')
                        row.pop('lastmodifiedby')
                        row.pop('id')
                        new_scheme = Schemes(**row)
                        new_scheme.param = eval(new_scheme.param)
                        if new_scheme.param.get('params'):
                            new_scheme.param['params'] = {"taxon": self.DATABASE_DESC, "prefix": "{barcode}"}
                        self.session.add(new_scheme)
                    self.session.commit()
                DataParam = self.models.DataParam
                dp = DataParam(tabname="prokka_annotation", name="gff_file", mlst_field="outputs,gff_file,1",
                               display_order=1, nested_order=0, label="GFF File", datatype="text")
                self.session.add(dp)
                dp = DataParam(tabname="prokka_annotation", name="gbk_file", mlst_field="outputs,gbk_file,1",
                               display_order=2, nested_order=0, label="GBK File", datatype="text")
                self.session.add(dp)
                self.session.commit()

        except  Exception as e:
            app.logger.exception('Error in create_schemes, error message: %s' % (e.message))
            self.rollback_close_session()

    def __get_genome_info(self, res, fields, offset):
        dict = {}
        dict['id'] = res[offset]
        for field in fields:
            dict[field] = res[field]
        return dict

    '''
    scheme- sql query
    min matches
    alleles
    st
    returns a dictionary  assembly_ids : {}
    '''

    def get_sts_by_alleles(self, scheme, min_match, alleles=None, st=None):
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)

            if st:
                SQL = "SELECT hash FROM sts  " + \
                      "INNER JOIN schemes ON legacy_id = %s AND schemes.description = '%s' AND sts.scheme_id = schemes.id "
                SQL = SQL % (st, scheme)
                cursor.execute(SQL)
                rec = cursor.fetchone()
                if rec == None:
                    return {},[]
                alleles = rec['hash']

            SQL = "SELECT loci.description as al_name FROM loci INNER JOIN scheme_loci  ON " + \
                  "loci_id =  scheme_id INNER JOIN schemes ON schemes.description = '%s' AND " + \
                  " scheme_id = schemes.id ORDER BY loci.description"
            SQL = SQL % (scheme)
            cursor.execute(SQL)
            records = cursor.fetchall()
            allele_name_list = []
            for rec in records:
                allele_name_list.append(rec['al_name'])

            SQL = "SELECT  hash, sts.legacy_id AS lid, sts.id as sid,st_complex,lineage,subspecies FROM sts " + \
                  "INNER JOIN schemes ON schemes.description = '%s' AND sts.scheme_id = schemes.id"
            alleles = alleles.split(",");
            al_num = len(alleles)
            SQL = SQL % (scheme)
            cursor.execute(SQL)
            records = cursor.fetchall()
            st_dict = {}
            st_list = []
            if not st:
                max_mismatches = 0
            else:
                max_mismatches = al_num - int(min_match)

            for rec in records:
                if not rec['hash']:
                    continue
                alls = rec['hash'].split(",")
                mismatches = 0
                match = True
                for n in range(0, al_num):
                    if not alleles[n]:
                        continue
                    if alls[n] <> alleles[n]:
                        mismatches += 1
                    if mismatches > max_mismatches:
                        match = False
                        break
                if match:
                    st_al_dict = {}
                    st_al_dict["st"] = rec['lid']
                    st_al_dict['st_complex'] = rec["st_complex"]
                    st_al_dict['lineage'] = rec["lineage"]
                    st_al_dict['subspecies'] = rec["subspecies"]
                    for n in range(0, al_num):
                        st_al_dict[allele_name_list[n]] = alls[n]

                    st_dict[rec['sid']] = st_al_dict

            if len(st_dict) == 0:
                return {},[]
            SQL = "SELECT assembly_id,st_id FROM st_assembly WHERE st_id IN (%s)" % (",".join(map(str, st_dict.keys())))

            cursor.execute(SQL)
            records = cursor.fetchall()
            aid_to_st = {}
            aid_list = []
            for rec in records:
                aid_to_st[rec['assembly_id']] = (dict(st_dict[rec['st_id']]))
                aid_list.append(rec['assembly_id'])
            return aid_to_st, aid_list
        except Exception as e:
            app.logger.exception("Getting alleles by ST failed, error message: %s" % e.message)
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            return {}, []

    def get_strain_info_from_assembly_barcodes(self, field, barcodes):
        conn = None
        cursor=None
        try:
            bar_list = "','".join(barcodes)
            bar_list = "'" + bar_list + "'"
            SQL = "SELECT strains.%s as strain_info, assemblies.barcode as assembly_barcode FROM strains INNER JOIN assemblies on strains.best_assembly=assemblies.id WHERE assemblies.barcode IN (%s)" % (
            field, bar_list)
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(SQL)
            records = cursor.fetchall()
            barcode_to_name = {}
            for rec in records:
                barcode_to_name[rec['assembly_barcode']] = rec['strain_info']
            cursor.close()
            conn.close()
            return barcode_to_name
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error getting strain names from barcodes %s, error message: "%(bar_list, e.message))
            return {}

    def get_assembly_breakdown(self):
        conn = None
        cursor=None
        try:
            SQL = "SELECT status,COUNT(strains.id) AS num FROM strains LEFT JOIN assemblies ON strains.best_assembly=assemblies.id GROUP BY status ORDER BY num DESC"
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(SQL)
            records = cursor.fetchall()
            ret_list = []
            for rec in records:
                if not rec['status']:
                    rec['status'] = "Unassembled"
                ret_list.append([rec['status'], rec['num']])
            cursor.close()
            conn.close()
            return {"field_name": "status", "field_label": "Status", "list": ret_list}
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in getting assembly breakdown")
            return {}

    ''' given a comma delimited list of assembly ids, this method will return
       a dictionary of id:record within the record is a field extra_row_info which contains
       the assembly id'''

    def get_assembly_stats(self, query):
        if not query:
            return {}
        params = self.get_dataparam('assembly_stats')
        if len(params)==0:
            return
        param_list = []
        for param in params:
            param_list.append(param['name'])
            param_list.append('id')
            param_list.append('other_data')
        info = ",".join(param_list)
        conn = None
        cursor=None
        try:
            SQL = "SELECT %s FROM assemblies WHERE %s " % (info, query)
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(SQL)
            records = cursor.fetchall()
            adict = {}
            aids = []
            for rec in records:
                reason_failed = []
                other_data = None
                if rec['status'] == "Assembled" or rec['status'] == 'Failed QC':
                    rec['can_view'] = 'true'
                if rec['other_data']:
                    try:
                        other_data = json.loads(rec['other_data'])
                    except:
                        other_data = None
                    if other_data:
                        reason_failed = other_data.get('reason_failed')
                        if reason_failed:
                            rec['extra_row_info'] = [rec['id'], reason_failed]

                del rec['other_data']
                adict[rec['id']] = rec
                aids.append(rec['id'])
            cursor.close()
            conn.close()
            return adict, aids
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in getting assembly stats for sql %s, error message: %s"%(SQL,e.message))

    '''
    text - The WHERE statement for selecting strains/genomes e.g sample_accession = 'SRS23232'
    genome_field_list - the fields to be contained in each genome entry. There are five default
    values:- 'accession','seq_platform','seq_library','seq_insert' and 'status'
    Returns a list of strain records (dictionaries) containing strain field:value
    Within each dictionary there is an Accession key containing
    a list of dictionaries with genome (run) information that is linked to the strain.
    The fields (keys) in this dictionary can be specified in the genome_field_list
    If the record is owned, the record will have an extra_row_info field containing
    details of the owner
    '''

    def get_strains(self, text, owner_list=[], permission=None, show_substrains=False):
        if not text:
            return [], ""
        field_list = []
        DataParams = self.models.DataParam
        params = self.session.query(DataParams.name, DataParams.tabname).filter(
            DataParams.tabname.in_(['strains', 'traces'])).all()
        traces_field_list = []
        for param in params:
            field_list.append(param.tabname + "." + param.name)
            if param.tabname == 'traces':
                traces_field_list.append(param.name)

        field_list.append('strains.id AS id')
        field_list.append('assemblies.status AS assembly_status')
        field_list.append('best_assembly')
        field_list.append('owner')
        flist = ",".join(field_list)
        self.rollback_close_session()
        self.session.close()
        # search all strains
        if show_substrains:
            SQL = "SELECT strains.uberstrain AS uber FROM strains INNER JOIN traces ON (%s) AND strains.id = traces.strain_id LEFT JOIN assemblies on strains.best_assembly=assemblies.id ORDER BY strains.id" \
                  % (str(text))
            if text.startswith('default'):
                limit = 200
                args = text.split(":")
                if not len(args) == 1:
                    try:
                        limit = int(args[1])
                    except ValueError:
                        limit = 200

                SQL = "SELECT strains.uberstrain AS uber FROM strains INNER JOIN traces ON strains.id = traces.strain_id LEFT JOIN assemblies on strains.best_assembly=assemblies.id ORDER BY strains.id DESC LIMIT %i" % (
                    limit)
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            uber_string = ""
            try:
                cursor.execute(SQL)
                uber_list = set()
                records = cursor.fetchall()
                for rec in records:
                    if rec['uber']:
                        uber_list.add(str(rec['uber']))
                if len(uber_list) == 0:
                    return [], ""
                uber_string = ",".join(uber_list)
                text = 'uberstrain IN (' + uber_string + ")"
            except Exception as e:
                app.logger.exception("Problem Querying Uber Strain, error message: %s"%e.message)
                rollback_close_connection(conn, cursor)
                # conn.close()
                return [], ""
        elif not text.startswith("default"):
            ###error from text
            text = "(" + text + ") AND strains.id = strains.uberstrain"

        SQL = "SELECT %s FROM strains INNER JOIN traces ON (%s) AND strains.id = traces.strain_id LEFT JOIN assemblies on strains.best_assembly=assemblies.id ORDER BY strains.id" \
              % (flist, str(text))
        if text.startswith('default'):
            limit = 200
            args = text.split(":")
            if not len(args) == 1:
                try:
                    limit = int(args[1])
                except ValueError:
                    limit = 200
            extra = ""
            if not show_substrains:
                extra = "WHERE strains.id = strains.uberstrain"

            SQL = "SELECT %s FROM strains INNER JOIN traces ON strains.id = traces.strain_id LEFT JOIN assemblies on strains.best_assembly=assemblies.id %s ORDER BY strains.id DESC LIMIT %i" % (
            flist, extra, limit)
        return self._processSQL(SQL, traces_field_list, owner_list, permission)

    def _processSQL(self, SQL, traces_field_list, owner_list, permission=None):
        conn=None
        cursor=None

        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            cursor.execute(SQL)
            records = cursor.fetchall()

            data = []
            first = True

            owner_set = set()
            traces_list = []
            previous_res = {}
            for res in records:
                if first:
                    first = False
                elif res['id'] <> previous_res['id']:
                    previous_res['Accession'] = traces_list
                    # delete the genome fields in the strain
                    for field in traces_field_list:
                        del previous_res[field]
                    traces_list = []
                    # I have moved dates format to query_functions
                    # cannot be jsonified therefore format
                    #if previous_res['created']:
                    #    previous_res['created'] = previous_res['created'].strftime('%Y-%m-%d')
                    #else:
                    #    previous_res['created'] = ""
                    #if previous_res['release_date']:
                    #    previous_res['release_date'] = previous_res['release_date'].strftime('%Y-%m-%d')
                    #else:
                    #    previous_res['release_date'] = ""
                    owner_set.add(previous_res['owner']);
                    data.append(previous_res)

                t_dic = {}
                for t_field in traces_field_list:
                    t_dic[t_field] = res[t_field]
                traces_list.append(t_dic)
                previous_res = res

            if first:
                return [], ""
            # add the last record
            previous_res['Accession'] = traces_list
            for field in traces_field_list:
                del previous_res[field]
            traces_list = []
            #if previous_res['created']:
            #    previous_res['created'] = previous_res['created'].strftime('%Y-%m-%d')
            #else:
            #    previous_res['created'] = ""
            #if previous_res['release_date']:
            #    previous_res['release_date'] = previous_res['release_date'].strftime('%Y-%m-%d')
            #else:
            #    previous_res['release_date'] = ""
            owner_set.add(previous_res['owner']);
            data.append(previous_res)

            # tidy up
            cursor.close()
            conn.close()

            # Need to know email\username of each record
            users = User.query.filter(User.id.in_(owner_set)).all();
            user_id_to_details = {}
            for user in users:
                user_id_to_details[user.id] = user.username + " at " + user.email
            ass_list = []
            #not_editable check has been moved to query_function script
            for record in data:
                #record['not_editable'] = True
                if record.get('owner'):
                    if user_id_to_details.get(record['owner']):
                        record['extra_row_info'] = user_id_to_details[record['owner']]
                    #if record['owner'] in owner_list:
                    #    record['not_editable'] = False
                #if permission:
                #    record['not_editable'] = False
                if record['best_assembly']:
                    ass_list.append(str(record['best_assembly']))

            return data, ",".join(ass_list)
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #cursor.close()
            app.logger.exception("Problem getting strains query %s, error message: %s" % (SQL, e.message))
            return [],''
            #raise

    def insert_single_scheme(self, desc, userid, st_complex=None, lineage=None, subspecies=None):
        try:
            Schemes = self.models.Schemes
            existing = self.session.query(Schemes).filter(Schemes.description == desc).first()
            if existing == None:
                new_scheme = Schemes(description=desc, lastmodified=datetime.datetime.now(),
                                     created=datetime.datetime.now(), lastmodifiedby=userid)
                if st_complex:
                    new_scheme.st_complex_label = st_complex
                if lineage:
                    new_scheme.lineage_label = lineage
                if subspecies:
                    new_scheme.subspecies_label = subspecies
                self.session.add(new_scheme)
                self.session.commit()
                new_scheme.barcode = self.encode(int(new_scheme.id), self.DATABASE_NAME, 'schemes')
                self.session.add(new_scheme)
                self.session.commit()
                return new_scheme.id
            else:
                return existing.id

        except  Exception as e:
            app.logger.exception('Error in insert_single_scheme %s, error message: %s' % (desc,e.message))
            self.rollback_close_session()

    def insert_alleles(self, entries):
        try:
            entries = self.clean_data(entries)
            table = self.models.metadata.tables['alleles']
            # Prepare std fields for entry
            clean_rows = entries
            keys = []
            for row in clean_rows:
                check = table.select().where(table.c.sequence == row['sequence'])
                results = self.session.execute(check)
                if results.scalar() == None:
                    result = self.session.execute(table.insert(), row)
                    keys += (result.inserted_primary_key)
                    app.logger.info('Inserted allele ID:%i in %s\n sequence:%s' % (
                    result.inserted_primary_key[0], self.DATABASE_NAME, row['sequence']))
            self.session.commit()
            return keys
        except  Exception as e:
            app.logger.exception('Error in insert_alleles %s, error message: %s' % (entries,e.message))
            self.rollback_close_session()
            return []

    def update_barcodes(self, table_name, ids):
        try:
            table = self.models.metadata.tables[table_name]
            for row in ids:
                if table_name == 'strains':
                    update = table.update().where(table.c.id == row).values(barcode=self.encode(
                        row,
                        self.DATABASE_NAME,
                        table_name), uberstrain=row)
                else:
                    update = table.update().where(table.c.id == row).values(barcode=self.encode(
                        row,
                        self.DATABASE_NAME,
                        table_name))
                results = self.session.execute(update)
            self.session.commit()

        except  Exception as e:
            self.rollback_close_session()
            app.logger.exception('Error in update_barcodes %s for these ids: %s, error message: %s' % (table_name,ids, e.message))

    def getStrainNumber(self):

        # from sqlalchemy import func
        # try :
        # query =  self.session.query(func.count(func.distinct(self.models.Strains.uberstrain)))
        # result = self.session.execute(query).scalar()
        # except :
        # query =  self.session.query(func.count(self.models.Strains.id))
        # result = self.session.execute(query).scalar()
        # self.session.rollback()
        # self.session.close()
        conn=None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=DictCursor)
            SQL = "SELECT COUNT(strains.id) AS num FROM strains INNER JOIN assemblies ON strains.best_assembly = assemblies.id WHERE strains.id = strains.uberstrain AND assemblies.status in ('Assembled', 'legacy')"
            cursor.execute(SQL)
            records = cursor.fetchall()
            conn.close()
            return records[0]['num']
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in getStrainNumber for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return None

    def getAssembledNumber(self):
        conn=None
        cursor=None
        try:
            conn = self._conn()#psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=DictCursor)
            SQL = "SELECT assemblies.status AS stat, COUNT(strains.id) AS num FROM strains INNER JOIN assemblies ON strains.best_assembly = assemblies.id WHERE strains.id = strains.uberstrain GROUP BY assemblies.status ORDER BY assemblies.status"
            cursor.execute(SQL)
            records = cursor.fetchall()
            assembled = 0
            queued = 0
            legacy = 0
            for rec in records:
                status = rec.get('stat')
                if status == 'Assembled':
                    assembled = rec['num']
                elif status == 'Queued':
                    queued = rec['num']
                elif status == 'legacy':
                    legacy = rec['num']
            conn.close()

            # Not all queued assemblies are associated with a strain at the moment
            return assembled + legacy, assembled, queued, legacy
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in getAssembledNumber for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return 0, 0, 0, 0

    def getSchemeNumber(self, public_only=False):
        conn=None
        cursor=None
        if public_only:
            SQL = "SELECT schemes.name AS name, COUNT(assembly_lookup.id) AS num FROM assembly_lookup INNER JOIN strains ON strains.best_assembly = assembly_lookup.assembly_id INNER JOIN schemes ON assembly_lookup.scheme_id = schemes.id WHERE strains.id = strains.uberstrain AND assembly_lookup.st_barcode LIKE '%_ST'  AND (schemes.param ->> 'display') = 'public'  AND  (schemes.param ->> 'scheme') IS NOT NULL GROUP BY schemes.name"
        else:
            SQL = "SELECT schemes.name AS name, COUNT(assembly_lookup.id) AS num FROM assembly_lookup INNER JOIN strains ON strains.best_assembly = assembly_lookup.assembly_id INNER JOIN schemes ON assembly_lookup.scheme_id = schemes.id WHERE strains.id = strains.uberstrain AND assembly_lookup.st_barcode LIKE '%_ST' GROUP BY schemes.name"
        try:
            conn =self._conn()# psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=DictCursor)
            cursor.execute(SQL)
            records = cursor.fetchall()
            assembled = 0
            queued = 0
            legacy = 0
            scheme_num = {}
            for rec in records:
                scheme_num[rec.get('name')] = rec.get('num')
            conn.close()
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                print( "Errrrrrr: ", e.message)
                #conn.close()
            app.logger.exception("Error obtaining scheme info, error message: %s"%e.message)
            return {}
        return scheme_num

    def insert_strains(self, entries, commit=True):
        try:
            entries = self.clean_data(entries)
            table = self.models.metadata.tables['strains']
            # Prepare std fields for entry
            clean_rows = entries
            keys = []
            for row in clean_rows:
                result = self.session.execute(table.insert(), row)
                keys.append(result.inserted_primary_key)
                app.logger.info('Inserted Strain ID:%i, name:%s, accession:%s into %s'
                                % (result.inserted_primary_key[0], row.get('strain'), row.get('sample_accession'),
                                   self.DATABASE_NAME))
            if commit: self.session.commit()
            return keys
        except  Exception as e:
            self.rollback_close_session()
            app.logger.exception('Error in insert strains %s, error message: %s'%(entries, e.message))
            return []

    def update_strains(self, entries, commit=True):
        try:
            entries = self.clean_data(entries)
            table = self.models.metadata.tables['strains']
            # Prepare std fields for entry
            clean_rows = entries
            keys = []
            for row in clean_rows:
                result = self.session.execute(
                    table.update().where(table.c.secondary_sample_accession == row['secondary_sample_accession']), row)
                app.logger.info('Updated Strain, name:%s, accession:%s into %s'
                                % (row.get('strain'), row.get('sample_accession'), self.DATABASE_NAME))
            if commit: self.session.commit()
            return [0]

        except  Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in update_strains for %s, error message: %s"%(entries, e.message))
            return[]

    def __as_dict(self, results):
        data = []
        for res in results:
            alldata = res[0].as_dict().copy()
            alldata.update(res[1].as_dict())
            data.append(alldata)
        return data

    def get_auto_correct_list(self, field, text, limit=15):
        """gets all values in the specified field containing the specified text (case insensitive)
               :param field: the field to query
               :param text: The text to search
               :param limit: The maximum number of values to return (default 15)
               :type arg1: string
               :type arg2: string.
               :type arg3: int
               :returns: a list of values for the given field matching the
               :rtype: list
         """
        # which database is it in
        record=None
        conn=None
        cursor=None
        try:
            record = self.session.query(self.models.DataParam).filter_by(name=field).first()
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_auto_correct_list for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return []
        self.session.close()
        if not record:
            return []
        table = record.tabname
        # why changing the table name
        # it generates error when the field is status as strains has not such column
        if table == 'assembly_stats':
            table = 'strains'
        conn = psycopg2.connect(self.DATABASE_BIND);
        cursor = conn.cursor(cursor_factory=DictCursor)
        SQL = "SELECT %s as rec FROM %s WHERE text(%s) ILIKE '%%%s%%' GROUP BY %s LIMIT %i" % (field, table, field, text, field, limit)
        try:
            cursor.execute(SQL)
            records = cursor.fetchall()
            list = []
            for rec in records:
                list.append(rec['rec'])
            cursor.close()
            conn.commit()
            conn.close()
            return list
        except Exception as e:
            app.logger.exception("Error in get_auto_correct_list -text: %s, field: %s database: %s, error messgae:%s" % (text, field, self.DATABASE_NAME, e.message))
            if conn:
                rollback_close_connection(conn, cursor)
            return []



    def get_scheme_loci(self, scheme):
        conn=None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            SQL = "SELECT loci.description as loc FROM loci INNER JOIN  scheme_loci ON  loci.id = scheme_loci.loci_id INNER JOIN schemes ON  schemes.id = scheme_loci.scheme_id AND schemes.description = '%s' ORDER BY loc" % (
                scheme)
            cursor.execute(SQL)
            records = cursor.fetchall()
            col_list = []
            for rec in records:
                col_list.append(rec['loc'])
            SQL = "SELECT st_complex_label AS st_complex, lineage_label AS lineage, subspecies_label AS subspecies FROM schemes WHERE schemes.description = '%s'" % (
                scheme)
            cursor.execute(SQL)
            labels = cursor.fetchone()
            cursor.close()
            conn.close()
            return col_list, labels
        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error in get scheme loci, error message: %s" % e.message)
            return [], []

    def get_assemblies_for_strain(self, strain_id):
        from entero.ExtraFuncs.query_functions import query_function_handle
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            fields = "assemblies.pipeline_version AS apv,traces.accession AS acc, traces.id AS tid,traces.barcode AS tbc, strains.id AS sid,assemblies.id AS aid, assemblies.barcode AS abc"
            SQL = "SELECT %s FROM assemblies INNER JOIN trace_assembly ON assemblies.id = trace_assembly.assembly_id INNER JOIN traces ON traces.id = trace_assembly.trace_id " + \
                  "INNER JOIN strains ON traces.strain_id = strains.id WHERE strains.id = %i  ORDER BY assemblies.id"
            SQL = SQL % (fields, int(strain_id))
            prev_assembly = None
            cursor.execute(SQL)
            results = labels = cursor.fetchall()
            assembly_list = []
            trace_num = 1
            exps = self.get_experiment_details([])
            for res in results:

                if res['aid'] <> prev_assembly:
                    assembly_dict = {}
                    assembly_dict['text'] = res['abc'] + "(Version " + str(res['apv']) + ")"
                    traces_children = []
                    experiment_children = []
                    assembly_dict['children'] = [{
                        "text": "Traces",
                        "type": "trace",
                        "id": "ass_trace:" + str(res['aid']),
                        "children": traces_children
                    }, {
                        "text": "Experiments",
                        "type": "experiment",
                        "id": "ass_experiment:" + str(res['aid']),
                        "children": experiment_children

                    }
                    ]

                    for exp in exps:
                        exp_values = []

                        values, ids = query_function_handle[self.DATABASE_NAME][exp](self.DATABASE_NAME, exp,
                                                                                     res['aid'], "assembly_ids")
                        if len(ids) == 0:
                            continue
                        vals = values[res['aid']]
                        for fields in exps[exp]['fields']:
                            if not fields.get('sub_fields'):
                                text = vals.get(fields['name'])
                                if not text:
                                    text = ""
                                exp_values.append({
                                    "text": fields['label'] + ":" + str(text),
                                    "id": str(res['aid']) + ":" + exp + ":" + fields['name']
                                })
                            else:
                                sub_children = []
                                for sub_fields in fields['sub_fields']:
                                    text = vals.get(sub_fields['name'])
                                    if not text:
                                        text = ""
                                    sub_children.append({
                                        "text": sub_fields['label'] + ":" + str(text),
                                        "id": str(res['aid']) + ":" + exp + ":" + fields['group_name'] + ":" +
                                              sub_fields['name']
                                    })
                                exp_values.append({
                                    "text": fields['group_name'],
                                    "id": str(res['aid']) + ":" + exp + ":" + fields['group_name'],
                                    "children": sub_children

                                })

                        experiment_children.append({

                            "text": exps[exp]['label'],
                            "id": exp + ":" + str(res['aid']),
                            "children": exp_values

                        })
                    assembly_dict['id'] = "assembly:" + str(res['aid'])
                    assembly_dict['type'] = "assembly"
                    assembly_list.append(assembly_dict)

                trace_dict = {"text": res['acc'], "id": "trace:" + str(res['tid']) + "_" + str(trace_num)}
                trace_num += 1
                traces_children.append(trace_dict)
                prev_assembly = res['aid']
            conn.close();
            return assembly_list

        except Exception as ex:
            if conn:
                rollback_close_connection(conn,cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error obtaining assembly info for strain, error message: %s"%ex.message)
            return []

    def get_scheme_names(self):
        """This function returns a list of all scheme names in the database
        :returns: A list of all scheme names
        """
        ret_list = []
        try:
            schemes = self.session.query(self.models.Schemes).all();
            for scheme in schemes:
                ret_list.append(scheme.description)
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in get_scheme_names for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
        return ret_list

    def get_experiment_details(self, allowed_schemes):
        '''Gets all experiments and their field names
        :returns: A dictionary experiment names to experiment information
        e.g   { "scheme1":{
                           label: "MLST Scheme1",
                           js_grid:"SchemeGrid",
                           fields:[
                                  {
                                    name:"st",
                                    label:"ST,
                                    display_order:1
                                    datatype:integer
                                   },
                                   {
                                    group_name:"Locus",
                                    display_order:2,
                                    sub_fields:
                                        [
                                             name:"aroC",
                                             label:"aroC,
                                             display_order:1,
                                             datatype:integer
                                        ],
                                        [
                                             name:"dnaN",
                                             label:"dnaN,
                                             display_order:1,
                                             datatype:integer
                                        ]
                                   }

                                  ]
                            }
              }
        '''
        conn = None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=RealDictCursor)

            SQL = "SELECT schemes.description AS exp_name, schemes.name AS exp_label, schemes.param->>'pipeline' AS send_job, schemes.param->>'js_grid' AS js_grid," + \
                  "schemes.param->>'scheme' AS mlst , schemes.param->>'display' AS display,data_param.name AS field_name, label AS field_label, display_order,nested_order,datatype,group_name,schemes.param as param, " + \
                  "data_param.param->>'not_searchable' AS not_searchable FROM schemes INNER JOIN data_param ON schemes.description = data_param.tabname  AND group_name IS NULL ORDER BY tabname,display_order,nested_order"
            cursor.execute(SQL)
            results = cursor.fetchall()
            list_index = 0
            all_exp = {}
            prevExp = ""
            exp_details = {}
            include = True
            show_alleles_list = []

            for row in results:
                if row['exp_name'] <> prevExp:

                    exp_details = {}
                    include = True
                    display = row['display']

                    if display == 'none' or not display:
                        include = False
                        prevExp = row['exp_name']
                        continue
                    if display == 'private' and not row['exp_name'] in allowed_schemes:
                        include = False
                        prevExp = row['exp_name']
                        continue
                    all_exp[row['exp_name']] = exp_details
                    exp_details['label'] = row['exp_label']
                    if not row['js_grid']:
                        row['js_grid'] = "BaseExperimentGrid"
                    exp_details['js_grid'] = row["js_grid"]
                    exp_details['fields'] = []
                    exp_details['nserv'] = "false"
                    if row['send_job']:
                        exp_details['nserv'] = "true"
                    if row['mlst']:
                        if not 'CRISP' in row['mlst']:
                            exp_details['mlst'] = "true"
                            if row['js_grid'] == 'SchemeGrid':
                                exp_details['show_alleles'] = "true"
                                show_alleles_list.append(row['exp_name'])
                            # append dummy loci
                            else:
                                exp_details["fields"].append({
                                    "group_name": "Locus",
                                    "sub_fields": []
                                })

                if not include:
                    prevExp = row['exp_name']
                    continue

                info_dict = {
                    "name": row['field_name'],
                    "label": row['field_label'],
                    "display_order": row['display_order'],
                    "datatype": row['datatype']
                }
                if row.get('not_searchable'):
                    info_dict['not_searchable'] = "true"
                exp_details['fields'].append(info_dict)
                prevExp = row['exp_name']

            # only get loci for those that are going to be displayed
            if len(show_alleles_list) > 0:
                in_text = "','".join(show_alleles_list)
                SQL = "SELECT tabname,name,label,group_name,nested_order,datatype,display_order FROM data_param WHERE tabname  IN ('%s') AND group_name = 'Locus' ORDER BY tabname ,display_order,nested_order" % in_text
                cursor.execute(SQL)
                results = cursor.fetchall()
                prev_exp = ""
                sub_fields = None
                for row in results:
                    if row['tabname'] <> prev_exp:
                        sub_fields = []
                        all_exp[row['tabname']]['fields'].append({"group_name": row['group_name'],
                                                                  "display_order": row['display_order'],
                                                                  "sub_fields": sub_fields
                                                                  })

                    sub_fields.append({
                        "name": row['name'],
                        "label": row['label'],
                        "display_order": row['nested_order'],
                        "datatype": row['datatype']
                    })
                    prev_exp = row['tabname']
            conn.close()
            return all_exp


        except Exception as ex:
            if conn:
                rollback_close_connection(conn, cursor)
                #conn.rollback()
                #conn.close()
            app.logger.exception("Error obtaining Experimental details, error message: %s"%ex.message)
            return {}

    # looks through strains and current users looking for matching emails

    # @abstractmethod
    # def insert_custom_fields(self):
    # raise NotImplementedError

    # @abstractmethod
    # def delete_custom_fields(self):
    # raise NotImplementedError

    # @abstractmethod
    # def create_db(self):
    # raise NotImplementedError

    # @abstractmethod
    # def destroy_db(self):
    # raise NotImplementedError

    # @abstractmethod
    # def populate_db(self):
    # raise NotImplementedError

    # @abstractmethod
    # def insert_strains(self):
    # raise NotImplementedError

    # def delete_strains(self):
    # raise NotImplementedError

    # @abstractmethod
    # def modify_strains(self):
    # raise NotImplementedError

    # @abstractmethod
    # def insert_genomes(self):
    # raise NotImplementedError

    # @abstractmethod
    # def modify_genomes(self):
    # raise NotImplementedError

    # @abstractmethod
    # def delete_genomes(self):
    # raise NotImplementedError

    # Helper functions

    # Extra Database debugging functions
    def table_insert(self, table_name, rows):
        ''' Bulk inserts row data into a given table '''
        # Get table handle from metadata
        try:
            table = self.model.Base.metadata.tables[table_name]
            # Produce valid insert statements for each row
            # Validate and populate columns based off data params.
            clean_rows = rows
            # If extra column data, check if a user field and add if need be.
            # raise exception if incorrect type is added
            # raise exception if extra information is added
            # ins = table.insert().values(clean_rows)
            result = self.session.execute(table.insert(), clean_rows)
            self.session.commit()
            return result
        except Exception as e:
            app.logger.exception("Error in table_insert for table %s, error message: %s"%(table_name,e.message))
            self.rollback_close_session()
            return "Failed"

    def table_select_all(self, table_name):
        ''' Strictly for testing and debugging purposes'''
        # I have added self.model.
        table = self.model.Base.metadata.tables[table_name]
        return table.select()

    def modify_strains(self, strain):
        raise NotImplementedError

    def delete_strains(self, accessions):
        try:
            accessions = self.clean_data(accessions)
            # Get table handle from metadata
            # I have added self.model.
            table = self.model.Base.metadata.tables['strains']
            clean_rows = []
            for acc_value in accessions:
                clean_rows.append(dict(accession=acc_value))
            status = self.session.execute(table.delete(), clean_rows)
            self.session.commit()
            return status

        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in delete_strains %s, error message: %s"%(accessions, e.message))

    def select_strains(self, dbname):
        raise NotImplementedError

    def select_abstract_strain(self, field, values):
        from sqlalchemy import or_
        try:
            Traces = self.models.Traces
            Strains = self.models.Strains
            clauses = or_(*[getattr(Traces, field) == x for x in values])
            results = self.session.query(Traces, Strains) \
                .join(Strains) \
                .filter(clauses).all()
            dat = []
            for res in results:
                alldata = res[0].as_dict().copy()
                alldata.update(res[1].as_dict())
                dat.append(alldata)
                if alldata[field] in values: values.remove(alldata[field])
            return dat, values
        except Exception as e:
            self.rollback_close_session()
            app.logger.exception("Error in select_strains for %s database, error message: %s"%(self.DATABASE_NAME, e.message))
            return [], values

    def delete_traces(self, accessions):
        try:
            accessions = self.clean_data(accessions)
            # Get table handle from metadata
            # I have added self.model.
            table = self.model.Base.metadata.tables['traces']
            clean_rows = []
            for acc_value in accessions:
                clean_rows.append(dict(accession=acc_value))
            status = self.session.execute(table.delete(), clean_rows)
            self.session.commit()
            return status
        except Exception as e:
            app.logger.exception("Error in insert_custom_fields, error message: %s" % (e.message))
            self.rollback_close_session()
            return "Failed"

    # Custom fields
    def insert_custom_fields(self, field_names, userid):
        keys = []
        try:
            field_names = self.clean_data(field_names)
            # Get table handle from metadata
            # I have added self.model.
            table = self.model.Base.metadata.tables['user_fields']
            clean_rows = []
            for name in field_names:
                clean_rows.append(dict(
                    field=name,
                    lastmodified=datetime.datetime.now(),
                    created=datetime.datetime.now(),
                    lastmodifiedby=userid
                ))

            for row in clean_rows:
                result = self.session.execute(table.insert(), row)
                keys.append(result.inserted_primary_key)
            self.session.commit()
        except Exception as e:
            app.logger.exception("Error in insert_custom_fields, error message: %s" % (e.message))
            self.rollback_close_session()
        return keys

    # Custom fields
    def custom_fields_list(self):
        fields_list = []
        try:
            # I have added self.model.
            table = self.model.Base.metadata.tables['user_fields']
            result = self.session.execute(table.select())

            for r in result:
                fields_list.append(r['field'])
        except Exception as e:
            app.logger.exception("Error in custom_fields_list, error message: %s" % ( e.message))
            self.rollback_close_session()
        return fields_list

    def delete_custom_fields(self, field_names):
        try:
            field_names = self.clean_data(field_names)
            # Get table handle from metadata
            #I have moodified it and added self.models.
            table = self.models.Base.metadata.tables['user_fields']
            clean_rows = []
            for name in field_names:
                clean_rows.append(dict(field=name))
            status = self.session.execute(table.delete(), clean_rows)
            self.session.commit()
            return status
        except Exception as e:
            app.logger.exception("Error in delete_custom_fields for %s, error message: %s" %(field_names,e.message))
            self.rollback_close_session()
            return "Failed"

    def delete_schemes(self, desc):
        try:
            desc_names = self.clean_data(desc)
            #I have moodified it and added self.models.
            table = self.model.Base.metadata.tables['schemes']
            clean_rows = []
            for name in desc_names:
                clean_rows.append(dict(description=name))

            status = self.session.execute(table.delete(), clean_rows)
            self.session.commit()
            return status
        except Exception as e:
            app.logger.exception("Error in delete_schemes, error message: %s"%e.message)
            self.rollback_close_session()
            return "Failed"


    def add_allele(self, dbname):
        raise NotImplementedError

    def insert_single_loci(self, loci, schemeid):
        currentuser = 1
        Schemes = self.models.Schemes
        Loci = self.models.Loci
        try:
            scheme = self.session.query(Schemes).filter(Schemes.id == schemeid).first()
            existing = self.session.query(Loci).filter(Loci.description == loci).first()
            if existing == None:
                new_locus = Loci(description=loci, type='CDS_fragment',
                                 accepted=0, checked=0, lastmodified=datetime.datetime.now(),
                                 lastmodifiedby=currentuser)
                new_locus.schemes.append(scheme)
                self.session.add(new_locus)
                self.session.commit()
                new_locus.barcode = self.encode(new_locus.id, self.DATABASE_NAME, 'loci')
                self.session.add(new_locus)
                self.session.commit()
                return new_locus.id
            else:
                return existing.id

        except Exception as e:
            app.logger.excetion("Errir in insert_single_loci, error message: %s"%e.message)
            self.rollback_close_session()
            return  -1


    # This method has error and is not used anywhere inside  the code

    def insert_single_allele(self, legacy, locus, sequence):
        currentuser = 1
        # I am not sure what Alleles is
        try:
            new_allele = Alleles(legacy_id=legacy, locus_id=locus, sequence=sequence,
                                 sequencelength=len(sequence),
                                 accepted=0, checked=0, lastmodified=datetime.datetime.now(),
                                 indels=0, lastmodifiedby=currentuser
                                )
            # I have added self.
            self.session.add(new_allele)
            self.session.commit()
        except Exception as e:
            app.logger.exception("Error message: %s"%e.message)
            self.rollback_close_session()
            return None
        return new_allele.id

    def get_versions(self, tablename, index_id, current_version):
        output = []
        table = self.models.Base.metadata.tables[tablename + '_archive']
        try:
            result = self.session.query(table).filter(table.c.index_id == index_id).order_by("version desc").all()

            if result == None:
                return None
            elif len(result) < (int(current_version) - 1):
                # That means some of the rows have been archived, retrive from archive database
                pass
            else:
                # result should be an array of dictionaries, each dictionary being a row.
                for res in result:
                    output.append(dict(zip(res.keys(), res)))
            # Add current record
            table = self.models.Base.metadata.tables[tablename]
            result = self.session.query(table).filter(table.c.index_id == index_id).all()
            for res in result:
                output.insert(0, dict(zip(res.keys(), res)))

        except Exception as e:
            app.logger.exception("Error in get_versions, error message: %s"%e.message)
            self.rollback_close_session()
        return output

    def getStrainInfo(self, field):
        conn=None
        cursor=None
        try:
            conn = psycopg2.connect(self.DATABASE_BIND);
            cursor = conn.cursor(cursor_factory=DictCursor)
            sql = "SELECT %s,COUNT(strains.id) AS num FROM strains GROUP BY %s ORDER BY num DESC" % (field, field)

            cursor.execute(sql)
            records = cursor.fetchall()
            cursor.close()
            conn.close()
            return records

        except Exception as e:
            if conn:
                rollback_close_connection(conn, cursor)
            self.session.close()
            app.logger.exception("Error in get Strain Info for %s,  error message: %s" % (field, e.message))
            return {}

    #in case or errors, heis method is used to rollback and close the session
    def rollback_close_session(self):
        try:
            self.session.rollback()
            self.session.close()
        except Exception as e:
            print "ERROR MESAGE WHILE CLOSING THE CONNECTION IS: %s" % e.message
            pass


    
