tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain		STRAIN															
strains	contact		SOURCE_LAB															
strains	source_type		ISOLATED_FROM															
strains	collection_year		YEAR															
strains	continent		CONTINENT															
strains	country		COUNTRY															
strains	city		LOCATION															
strains	uberstrain			-1000	0	Uberstrain	integer										
strains	species	Sample.Metadata,Species		5	0	Species	text										
bionumerics	strains.strain			1	0	Strain_Name	text
bionumerics	strains.country			1	0	Country	text
bionumerics	assembly_lookup.barcode			1	0	assembly_barcode	text
bionumerics	assembly_lookup.st_barcode			1	0	st_barcode	text
bionumerics	assembly_lookup.version			1	0	st_version	text
bionumerics	assembly_lookup.scheme_name			1	0	scheme_name	text
bionumerics	assemblies.barcode			1	0	assembly_barcode	text
bionumerics	assemblies.status			1	0	Assembly_status	text
bionumerics	assemblies.version			1	0	assembly_version	text
bionumerics	strains.collection_day			1	0	Collection_day	text
bionumerics	strains.collection_year			1	0	Collection_year	text
bionumerics	strains.collection_month			1	0	Collection_month	text
bionumerics	strains.admin1			1	0	Region	text
bionumerics	strains.admin2			1	0	County	text
bionumerics	strains.antigenic_formulas			1	0	antigenic_formulas	text
bionumerics	strains.barcode			1	0	strain_barcode	text
bionumerics	strains.city			1	0	City	text
bionumerics	strains.contact			1	0	Lab_contact	text
bionumerics	strains.email			1	0	Email	text
bionumerics	strains.release_date			1	0	release_date	text
bionumerics	strains.sample_accession			1	0	sample_accession	text
bionumerics	strains.secondary_sample_accession			1	0	secondary_sample_accession	text
bionumerics	strains.secondary_study_accession			1	0	secondary_study_accession	text
bionumerics	strains.source_details			1	0	Source_details	text
bionumerics	strains.source_niche			1	0	Source_niche	text
bionumerics	strains.source_type			1	0	Source_type	text
bionumerics	strains.strain			1	0	Strain_name	text
bionumerics	strains.study_accession			1	0	study_accession	text
bionumerics	strains.version			1	0	metadata_version	text
bionumerics	assembly_lookup.scheme_name			1	0	scheme_name	text
bionumerics	assembly_lookup.scheme_id			1	0	scheme_id	text
api	assembly_lookup.barcode			1	0	assembly_barcode	text
api	assembly_lookup.st_barcode			1	0	ST_barcode	text
api	assembly_lookup.version			1	0	ST_version	text
api	assembly_lookup.scheme_name			1	0	scheme_name	text
api	assembly_lookup.st_barcode_link			1	0	ST_barcode_link	text
api	assembly_lookup.scheme_link			1	0	scheme_link	text
api	assemblies.barcode			1	0	assembly_barcode	text
api	assemblies.status			1	0	assembly_status	text
api	assemblies.version			1	0	assembly_version	text
api	strains.admin1			1	0	region	text
api	strains.admin2			1	0	county	text
api	strains.antigenic_formulas			1	0	antigenic_formulas	text
api	strains.barcode			1	0	strain_barcode	text
api	strains.city			1	0	strain_city	text
api	strains.contact			1	0	lab_contact	text
api	strains.country			1	0	country	text
api	strains.email			1	0	email_contact	text
api	strains.release_date			1	0	release_date	text
api	strains.sample_accession			1	0	sample_accession	text
api	strains.secondary_sample_accession			1	0	secondary_sample_accession	text
api	strains.secondary_study_accession			1	0	secondary_study_accession	text
api	strains.serotype			1	0	serovar	text
api	strains.source_details			1	0	source_details	text
api	strains.source_niche			1	0	source_niche	text
api	strains.source_type			1	0	source_type	text
api	strains.strain			1	0	strain_name	text
api	strains.study_accession			1	0	study_accession	text
api	strains.version			1	0	metadata_version	text