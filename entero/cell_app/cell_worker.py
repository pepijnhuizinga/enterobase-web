#!/usr/bin/env python
import os
from entero import celery, create_app

app = create_app('production')
app.app_context().push()