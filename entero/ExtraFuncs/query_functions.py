'''A module which contains methods used to retreive and query experimental data. All methods conform to the
   The following
   
   Args:
            **database**: The name of the database
            
            **exp**: The name (description) of experiment/sceheme e.g.rMLST
            
            **query**: The actual query in SQL form e.g. name='bug1'
            
            **query_type**:The type of query. The default value id 'query' specifying  an SQL like query. Other possible values depend
            on the experiment, but two possible generic values are
            
            * assembly ids  - in which case, query should be a string of comma delimited assembly ids
            * summary_breakdown - The query should contain the filed used for the summary breakdown (st by default for MLST schemes)
            
            **extra_data**: Optional data to help with the query

   Returns:
        A dictionary of assembly_ids  to a dictionary of field_name:values
        The field names should correlate to those in dataparams for that particular tabname
        e.g 
        ```
        {234:{"ST":65,"allele1:232},4545:{"ST":88,"allele1:565}}
        ```
        
        Also a list of genome IDs should be returned
        
        If summary_breakdown is the query type a dictionary of the following:-
        
        * field_name: name of the field used for the summary
        * field_label:label of the summary field
        * list: A list of two items arrays containing the name of the category and the number
        of that category

'''
from entero import dbhandle,db,app
from entero.databases.system.models import User,UserPreferences, BuddyPermissionTags, check_permission
import ujson
import requests
import sys,re
import numpy as np
from datetime import datetime
from dateutil import parser    







def combine_strain_and_exp_data(strains,exp,options,jsonify=True):
    '''Given strain data and experimental data this method will sunchronize the two 
    and filter out any data depending on the parameters given.
    
    Args:
         **strains**: A list of dictonaries containing strain information
         
         **exp**: A dictionary of assembly_id to experimental data
         
         **options** A dictionary of the following with either True of False
         
         * show_non_assemblies - If True failed,unassembled records will be returned
         * no-legacy - If True no records with just 7 gene MLST will be returned
         * only-editable If True only records editable by the user will be returned
         
         **jsonify**:  determines whether the retruned dictionary is jsonified (default True)
    
    Returns:
        A dictionary of of 'strains' and 'experiment' , both pointing to a list of dictionaries of either strain (metadata)
        or experimental data. The id for both is the strain id and both lists are in the same order. 
    '''
    
    strain_list=[]
    exp_list=[]
    show_non_assemblies = False
    allowed_assemblies=["Assembled","Queued","legacy"]
    if options['show_non_assemblies'] == "true":
        show_non_assemblies= True
    for strain in strains:        
        if options['no_legacy'] and strain['Accession'][0]['seq_platform']=='7GeneMLST':
            continue
        if options["only_editable"] and strain['not_editable']:
            continue
        if not strain['assembly_status'] in allowed_assemblies and not show_non_assemblies:
            continue
        st_id = strain['id']     
        exp_rec = exp.get(strain['best_assembly'])
        #no experimental data -do we want to include it?
        if not exp_rec:
            if options['only_with_data']:
                continue
            exp_rec = {}
        exp_rec['id'] = st_id
        exp_list.append(exp_rec)
        strain_list.append(strain);
    return_data= {"strains":strain_list,'experiment':exp_list}
    if jsonify:
        return ujson.dumps(return_data) 
    else:
        return return_data


def load_main_workspace(user_id,database,ws_id,options):
    ''' returns a jsonified dictionary containing all the information about the supplied workspace
        
        Args:
             **user_id**: The id of the user
             
             **database**: The name of the database
             
             **ws_id** The id of the database
             
             **options** see :meth:`combine_strain_and_exp_data`
            
        
        Returns:
            A jsonified dictionary containing the strain and experimental data, as well as information
            of the MS and SNP trees associated with the workspace and the grid layout
    '''   
    record =db.session.query(UserPreferences).filter_by(id=ws_id,database=database
                                                ,type="main_workspace").first()
    if not record:
        return ujson.dumps({"strains":[],'experiment':[]})
    data = ujson.loads(record.data)
    sids  = data['data']['strain_ids']
    strain_data,aids = process_strain_query(database,','.join(map(str,sids)),"ids",user_id,show_substrains=True)
    exp = data['data']['experimental_data']
    if not aids:
        exp_data={}
    else:
        #clumsy way of checking whether last view was a custom view
        try:
            cview = int(exp)
            exp_data,aids = process_custom_view_query(database,"",aids,"assembly_ids",cview) 
        except:
            #normal query
            exp_data,aids = query_function_handle[database][exp](database,exp,aids,"assembly_ids")           

    return_data = combine_strain_and_exp_data(strain_data,exp_data,options,jsonify=False)
    del data['data']['strain_ids']
    return_data['data'] = data['data']
    return_data['grid_params'] = data.get('grid_params')
    snp_projects = data['data'].get('snp_projects')
    if snp_projects:
        return_data['snp_projects']=snp_projects;
    return ujson.dumps(return_data)

def process_strain_query(species,query,query_type,user_id,aids=None,operand="AND",show_substrains=False):
    '''Processes the query returning the metadata and a corresponding list of genome IDs
    
    Args:
         **query**: An SQL WHERE clause e.g. name = 'bug1'  or a comma delimited list of accessions or 
         ids depending on the query type.  In addition the following special values are allowed
         
         * my_strains - Only records the user owns (or has buddy permissions to do so will be returned)
         * all - Get all records
         * default -Get last 200 records in order
                
         **species**: The name of the database
         
         **query_type**: Can be one of the following
         
         * query - A normal SQL WHERE clause 
         * accessions - A comma delimited list of accessions
         * ids - A comma delimited list of strain IDs
         
         **user_id**: Required to work out which records the user has permissions for
         
         **aids**: A comma delimited string of assembly ids which will be added to the query depernding on the operand (optional)
         
         **operand**: Either 'AND' or 'OR'. Used to include the assembly ids in the query
         
    Returns: 
         A list of metadata for the strain in form of a list of dictionaries of field:value
         Also returns a list of assembly ids corresponding to the returned strains
        
    '''
    owner_list = set()
    permission = None
    if user_id <> -1:
        current_user = db.session.query(User).filter_by(id=user_id).one()
        owner_list,permission = current_user.get_edit_meta_permissions(species)
    #normally the text to send is just the query   
    text = query
    #can be special values or it may alter depending on the query type
    if query =="my_strains":
        if len(owner_list) == 0:
            return [],[]
        owners = ",".join(map(str,owner_list))
        text = 'strains.owner IN (%s)' % (owners)       
    elif query=='all':
        text ="strains.id>0"           
    elif query_type == "accessions":
        accs = query
        text = "accession IN ('"+accs.replace(",","','")+"')"
    elif query_type == "ids":
        if not query:
            return [],[]
        text = "strains.id IN (%s)" % query
    #id aids have been supplied need to do AND or OR 
    if aids:
        text = "("+text+") "+operand +" best_assembly IN ("+aids+")"
    data,idsg = dbhandle[species].get_strains(text,owner_list=owner_list,permission=permission,show_substrains=show_substrains)
    strains_ids=[]    
    from workspace import get_access_permission     
    permissions=get_access_permission(species, user_id,strains=data,  return_all_strains_permissions=True) 
    #not editable check has been removed from database.py 
    for record in data: 
        #K.M 16/09/2019
        #check the collection time and format it 
        if record.get('collection_time'):
            record['collection_time']=record['collection_time'].strftime("%H:%M:%S")
       
        if record['created']:
            record['created'] = record['created'].strftime('%Y-%m-%d')
        else:
            record['created'] = ""
        if record['release_date']:
            record['release_date'] = record['release_date'].strftime('%Y-%m-%d')
        else:
            record['release_date'] = ""        
        if permissions.get(record['id'])==0:
            record['is_downloadable'] = False
        elif permissions.get(record['id']) >0:
            record['is_downloadable'] = True  
        if permissions.get(record['id']) ==3:
            record['not_editable']=False
        else:
            record['not_editable']=True            
    return data,idsg

def __nserv_get_st_matches(database,scheme,st,matches,alleles=True,alleleMatch = None):
    '''Helper method which gets st matches
    :param database: the name of the database
    :param scheme: the name of the scheme
    :param st: The st to search for
    :param matches: The max number of differences for an ST match
    :param alleles: if true then all alleles will be returned
    :param alleleMatch : A string of comma delimited allele numbers in order (0 being wild card) if present
    then st and matches will bw ignored
    '''
    dbase = dbhandle[database]
    resp=None
    try:
        ret_val = __get_nserv_params(database,scheme,False)
        if len(ret_val)==0:
            raise Exception("Error obtaining parameters in nserv query")
        params = ret_val['params']  
        param_to_path = ret_val['param_to_path']
        arr = ret_val['species_db']
          
        #match a set of alleles
        if alleleMatch:
            allele_values = alleleMatch.split(",")
            query_list =  []
            DataParam = dbase.models.DataParam
            data_params = dbase.session.query(DataParam.name).\
            filter_by(group_name='Locus',tabname=scheme).\
            order_by(DataParam.nested_order).\
            all()
            for index,param in enumerate(data_params):
                if allele_values[index]:
                    query_list.append(param.name+"="+allele_values[index])     
#            url="%s/match.api/%s/%s/type_search?%s" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1],"&".join(query_list)) 
            url="%s/match.api/%s/%s/type_search?%s&distmax=%i" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1],"&".join(query_list),int(matches))            
        #standard st match
        else:
            if st and matches and matches.isdigit():
                url ="%s/match.api/%s/%s/type_search?type=%i&distmax=%i" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1],int(st),int(matches))
            else:
                return {}, [] 
            
        #now add what fields we want returned
        field_names =set();
        if alleles:
            field_names.add("value_indices")
        for key in params:        
            field_names.add(params[key].split(",")[0])
        url=url + "&fieldnames=" + ",".join(field_names)
        
        resp = requests.get(url=url,timeout=app.config['NSERV_TIMEOUT'])
        if not resp.text:
            raise Exception("Error in NSERV ADDRESS server response, field names: %s, response is: %s"%(",".join(field_names), resp.text))
        try:
            data = ujson.loads(resp.text)
        except Exception as e:
            raise Exception ("field names: %s, could not load data from valid json, resp text is: %s"%(",".join(field_names), resp.text))
        
        #response is error message
        if isinstance(data, str) or isinstance(data,unicode):
            raise TypeError("field names: %s, request to this url: %s returned invalid repsonse, server response: %s " %(",".join(field_names), url, resp.text))
     
        #add the differences
        for record in data:
            record['differences']=record['query']-record['matches'] 
        param_to_path['differences'] = ["differences"]
        
        return __process_nserv_data(data, alleles, param_to_path,database,ret_val['scheme_id'])

    #xcept TypeError as e:
    #    app.logger.exception("__nserv_get_st_matches failed, error message: %s"%e.message)
    #    return {}, []

    except Exception as e:
        #K.M 21/08/2019
        #Raise exception in case of erro rather than returing empty results set        
        response = 'None'
        if resp:
            response=resp.text
        errMessage="Error in nserv scheme: %s database:%s  matches: %s\nresponse: %s, response type is: %s, error message: %s" % (scheme,database,matches,response,type(response),e.message)
        app.logger.exception(errMessage)
        raise Exception(errMessage)
        #return {}, []

#the basic nserv query based on sql like text   
def __nserv_q(database,scheme,query,alleles=True,barcode_to_aid=False, barcodes=None):
    '''Given an SQL like query this method will return the a dictonary of assembly ids to the info and
    a list of assembly ids  which can be returned from a query method
    
    :param database: the name of the database
    :param scheme: the name of the scheme
    :param query: The actual SQL like query. e.g
        ..code-block:: JSON
            "serotype_pred" LIKE '%Para%'
                             or
            "dnaN" = '4' AND "serotype_pred" = 'Angona' 
            
    :param alleles: Can be True whereby all alleles are returned, False(default) where none are,or
        a list of particular alleles to be returned.
    :param barcode_to_aid: a dictionary of ST barcode to a list of assembly ids. If present then the query will
        not be parsed as it is assumed the query is 'barcode IN (.........)'. This is used to link returned results
        with assembly ids and thus saves a database lookup.
    
    The query (if no barcode_to_aid is supplied) will be altered to account for nested json paramaters and 
    punctuation. In addition any allele queries will result in itermediate calls to produce the final query
    sent to NServ  e.g.
    .. code-block:: JSON
    
        "dnaN" = '4' AND "serotype_pred" LIKE '%Para%' 
        converted to 
        ST_id IN (6,42,...) AND text(info #> '{predict,serotype,0,0}') LIKE '"%%Para%%"'
    '''
            
    resp = None
    try:
        get_loci_list=False
        if not barcode_to_aid:
            get_loci_list = True
        ret_val = __get_nserv_params(database,scheme,get_loci_list)
        if len(ret_val)==0:
            raise Exception("Error obtaining parameters in nserv query")
        params = ret_val['params']
        loci_list = ret_val['loci_list']
        param_to_path = ret_val['param_to_path']
        arr = ret_val['species_db']
        if not barcode_to_aid:
            for op in ["=","<",">"]:
                query=query.replace(op,' %s ' % op)
            query=query.replace("<  >","<>")
            query=query.replace("(","( ").replace(")"," )")
            query= query.replace("\"","")
            query= query.replace("%","%%")
            #replace spaces between single quotes
            for quoted_part in re.findall(r"'(.+?)'", query):
            # For each part that is within quotes replace the spaces with the pattern *#*#*#* or whatever you want
                query= query.replace(quoted_part, quoted_part.replace(" ", "###"))            
            word_list_ = query.split()
            word_list=[]
            #word_list=[u'st_complex', u'NOT LIKE', u"'%%155%%'"]
            # adjust the word lists to account for the case of NOT LIKE operator
            for i in range (0, len(word_list_)):
                if i < len(word_list_) and word_list_[i] =='NOT' and 'LIKE' in  word_list_[1+i]:
                    word_list.append('NOT LIKE')
                elif i>0 and word_list_[i-1]=='NOT' and 'LIKE' in word_list_[i]:
                    continue
                else:
                    word_list.append(word_list_[i])

            pos = 0
            # convert the sql query to a Zhemin query
            for word in word_list:
                if word in params:
                    mapping = params.get(word)
                    if mapping:
                        if "," in mapping:
                            param_arr =mapping.split(",")
                            field = param_arr[0]
                            json = ",".join(param_arr[1:])
                            word_list[pos]="%s #>> '{%s}'" % (field,json)
                            n = pos+2
                            if word_list[n]=="(":
                                n=pos+3
                            if word_list[n].startswith("'"):
                                temp = word_list[n][1:-1]
                                temp=temp.replace("','","','")
                                temp = "'"+temp+"'"
                                word_list[n]=temp
                            else:
                                temp = word_list[n]
                                temp=temp.replace(",","','")
                                temp = "'"+temp+"'"
                                word_list[n]=temp                            
                        else:
                            word_list[pos]=mapping
                    
                #allele queries are slightly different  - need a separate subquery      
                if word in loci_list:
                    if word_list[pos+1]=='LIKE':
                        word_list[pos+1]="="
                        word_list[pos+2]=word_list[pos+2].replace("%","").replace("'","").replace('"',"")
                    allele_query = word_list[pos]+word_list[pos+1]+word_list[pos+2]
                    allele_query=allele_query.replace("'","")
                    url="%s/match.api/%s/%s/type_search?%s" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1],allele_query)
                    resp = requests.get(url=url,timeout=app.config['NSERV_TIMEOUT'])
                    data = ujson.loads(resp.text)
                    if  isinstance(data, str) or isinstance(data,unicode):
                        raise Exception("%s invalid  : %s " %(url, resp.text))              
                    st_list = ",".join(list(str(item['ST_id']) for item in data))
                    if st_list:
                        word_list[pos]= "ST_id"
                        word_list[pos+1]="IN"
                        word_list[pos+2]="(%s)" % st_list
                    else:
                        word_list[pos]= ""
                        word_list[pos+1]=""
                        word_list[pos+2]=""                   
                pos=pos+1
                
            query = ' '.join(word_list)
            query = query.replace("###"," ")
        
        #somtimes the query will dissappear during processing
        if not query and not barcodes:
            return {},[]
       
#        url ="%s/search.api/%s/%s/types" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1])
        if barcodes :
            url ="%s/retrieve.api" % ( app.config['NSERV_ADDRESS'],)
            param = dict(barcode=barcodes)
        else :
            url ="%s/search.api/%s/%s/types" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1])
            param = dict(filter=query)
        if  alleles == None:
            #get only the fields that are required
            field_list = ["barcode"]
            for key in params:
                field_list.append(params[key].split(",")[0])
            param['simple']=1
            param['fieldnames'] = " , ".join(set(field_list))
        elif isinstance(alleles, list):
            param['sub_fields']=",".join(alleles)
        
        resp = requests.post(url=url,data=param,timeout=app.config['NSERV_TIMEOUT'])
        try:
            data = ujson.loads(resp.text)
        except:
            raise Exception("Could not load json from resp.text %s"%resp.text)
        #error message returned instead of data structure
        if  isinstance(data, str) or isinstance(data,unicode):
            raise Exception("search parameter %s maybe is invalid  : %s " %(param, resp.text))
        return __process_nserv_data(data, alleles, param_to_path,database,ret_val['scheme_id'],barcode_to_aid)
    
    except Exception as e:
        #K.M 21/08/2019
        #Raise exception in  case of erros rather than returing empty results set
        retvalue= 'None'
        if resp:
            retvalue = resp.text
        errMessage="Error in nserv scheme: %s database:%s  matches: %s\nresponse: %s, response type is: %s, error message: %s" % (scheme,database,matches,response,type(response),e.message)
        app.logger.exception(errMessage)
        raise Exception (errMessage)
        #return {}, []

 
 
def __get_nserv_params(database,scheme,loci_list_required=False):
    ''''Helper method for nserve queries,gets the fields that are to be returned, depending on the scheme
    :param database: the name of the database
    :param scheme: the name of the scheme
    :param loci_list_required: If True a list of all loci will be present in the returned dictionary
    
    Returns a dictionary of the following
    * params -  a list of all paremters 
    * loci_list -  a list of all loci (if specified)
    * param_to_path - a dictionary of the parameter name to its json path in serve
    e.g. 
    .. code-block:: JSON
    
        {
            "serotype_pred":["predict","serotype",0,0],
            "st":["ST_id"]
        }
    * species_db - a list containing the nserv species and scheme eg ["Salmonella","UoW"]
    * scheme_id - the id of the scheme
    '''
    dbase = dbhandle[database]
    try:
        Schemes = dbase.models.Schemes
        
        scheme =  dbase.session.query(Schemes).filter_by(description=scheme).first()
        scheme_name = scheme.param.get('scheme')
        if not scheme_name:
            scheme_name=scheme.pipeline_scheme
        
        arr = scheme_name.split("_")
        if arr[0] == 'Senterica':
            arr[0]= "Salmonella"
        if arr[0] == 'Ecoli':
            arr[0]= "Escherichia"         
        DataParam = dbase.models.DataParam
        if  loci_list_required:
            data_params = dbase.session.query(DataParam.name,DataParam.mlst_field,DataParam.group_name).filter_by(tabname=scheme.description).all()
        else:
            data_params = dbase.session.query(DataParam.name,DataParam.mlst_field,DataParam.group_name).filter(DataParam.tabname==scheme.description,DataParam.group_name == None).all()
        params ={}
        loci_list=[]
        param_to_path={}
        for param in data_params:
            if param.mlst_field:
                #comma delimited path
                params[param.name]=param.mlst_field
                #list with the correct types
                p_arr = param.mlst_field.split(",")
                t_list=[]
                for item in p_arr:
                    try:
                        t_list.append(int(item))
                    except Exception as e:
                        t_list.append(item)
                param_to_path[param.name]=t_list
            elif param.group_name == 'Locus':
                loci_list.append(param.name)
        return {"params":params,"loci_list":loci_list,"param_to_path":param_to_path,"species_db":arr,"scheme_id":scheme.id}
    except Exception as e:
        dbase.session.rollback()
        dbase.session.close()
        app.logger.exception("Error processing params for Nserv")
        return {}

 #helper method for nserve queries,processes the returned data to the correct format       
def __process_nserv_data(data,alleles,param_to_path,database,scheme_id,barcode_to_aid=False):
    ''''Helper method for nserve queries. Processes the data such that it can be returned from
    the query method
    :param data - the data returned from nserv
    :param alleles - If False or None no alleles will be pulled from the data
    :param param_to_path: see :meth:`__get_nserv_params`
    :paran database: the name of the database
    :param scheme_id : the idof the scheme 
    :barcode_to_aid: A dictionary of st barcode to a list of assembly ids (if provided saves a database call)
    '''
    st_dict = {}
  
  
    barcode_list = []
    for record in data:
        rec_dict={}      
        #get fields
        for key in param_to_path:
            dic=record
            result=""
            try:
                for field in param_to_path[key]:
                    dic=dic[field]
                result=dic
            except Exception as e:
                result=""              
            rec_dict[key] = result
                       
        #get barcodes
        barcode = record['barcode']
        rec_dict["extra_row_info"]=barcode
        #get alleles if needed
        if alleles and record.has_key('alleles'):
            for allele in record.get('alleles'):
                rec_dict[allele['locus']]=allele['allele_id']            
        st_dict[barcode]=rec_dict
        barcode_list.append(barcode)
        
    aid_dict = {}
    aid_list = []
    
    #need to look up assembly ids
    if not barcode_to_aid:
          
        dbase =dbhandle[database]
        Lookup = dbase.models.AssemblyLookup
       
        sts = dbase.session.query(Lookup.st_barcode,Lookup.assembly_id).filter(Lookup.st_barcode.in_(barcode_list),Lookup.scheme_id==scheme_id).all() 
        for st in sts:
            if st.st_barcode==None:
                continue
            info = st_dict.get(st.st_barcode)
            if not info:
                continue
            aid_dict[st.assembly_id]= dict(info)
            aid_list.append(st.assembly_id)
        dbase.session.close()   
    #assembly ids already linked to barcode
    else:
        for barcode in barcode_to_aid:
            info = st_dict.get(barcode)
            if not info:
                continue
            for aid in barcode_to_aid[barcode]:
                aid_dict[aid]= dict(info)
                aid_list.append(aid)                
            
      
    return aid_dict,aid_list    
    
  
def __get_field_value(dic,path):
    arr= path.split(",")
    try:
        for field in arr:
            try:
                field= int(field)
            except:
                pass
            dic=dic[field]
        return dic
    except Exception as e:
        return "";
        
def get_alleles_from_strain_ids(database,scheme_name,strain_ids,loci):
    chunk_size=2000
    sids = ",".join(map(str,strain_ids))
    lcs = ".".join(loci)
    dbase = dbhandle[database]
    scheme = dbase.session.query(dbase.models.Schemes).filter_by(description = scheme_name).one()
    sql = "SELECT strains.id AS sid, ST_barcode AS st_bar FROM strains INNER JOIN assembly_lookup ON best_assembly = assembly_id AND scheme_id = %i WHERE strains.id IN (%s)" % (scheme.id,sids)
    results = dbase.execute_query(sql)
    barcode_to_ids={}
    barcode_set = set()
    for result in results:
        bar =  result['st_bar']
        if not bar:
            continue
        barcode_set.add(bar)
        id_list = barcode_to_ids.get(bar)
        if not id_list:
            id_list=[]
            barcode_to_ids[bar]=id_list
        id_list.append(result['sid'])
    barcode_set = list(barcode_set)  
    arr= scheme.param['scheme'].split("_")
    url = "search.api/"+arr[0]+"/"+arr[1]+"/types"
    url = app.config['NSERV_ADDRESS'] +url
   
    all_dict={}
    for sid in strain_ids:
        all_dict[sid]={}
    chunks=[barcode_set[x:x+chunk_size] for x in xrange(0, len(barcode_set), chunk_size)]
    for chunk in chunks:
        resp = requests.post(url=url,data={"barcode":','.join(chunk),"sub_fields":lcs},timeout=app.config['NSERV_TIMEOUT'])
        data = ujson.loads(resp.text)
        for record in data:

            for allele in record['alleles']:
                locus = allele['locus']
                if locus in loci:
                    sids = barcode_to_ids[record['barcode']]
                    for sid in sids:
                        val = allele['allele_id']
                        if allele['allele_id']<0:
                            val =-1                       
                        all_dict[sid][locus]=val
    return all_dict

def __get_st_info(database,scheme,aids,alleles=True):
    barcode_to_aid= dbhandle[database].get_barcodes_from_aids(aids,scheme)
    #barcodes = "','".join(barcode_to_aid.keys())    
    barcodes = ','.join(barcode_to_aid.keys())
    if not barcodes:
        return {},[]
    #query = "barcode IN ('%s')" % barcodes
      
    return __nserv_q(database,scheme,'',alleles,barcode_to_aid=barcode_to_aid, barcodes=barcodes)
    #return __nserv_q(database,scheme,query,alleles,barcode_to_aid=barcode_to_aid)

#*****Mapped Methods*****

def process_small_scheme_query_nserv(database,exp,query,query_type,extra_data=None):
    '''As well as the ST (and other specified fields e.g.  eBG) this method will return all allele values
    
    Args:
        Same as normal except query_type can be st_match, in which case query can be
        
        * alleles:comma delimited list of alleles in order (0 being wild card)  
        e.g. alleles:1,56,2,0,12,12,32
        * st:st value:max number differences to allow e.g. st:131:100
    '''    
    #partial match on ST or alleles
    if query_type== "st_match":
        vals =  query.split(":")
        if len(vals)>=3:
            matches= vals[2]
        else:
            matches=0
        if vals[0]=="alleles":
            return __nserv_get_st_matches(database,exp,0,matches,alleles=True,alleleMatch=vals[1])
        else:
            #matches= vals[2]
            st = vals[1]  
            return __nserv_get_st_matches(database,exp,st,matches,alleles=True)
    #get the STs associated with assemblies
    elif query_type== "assembly_ids":        
        return __get_st_info(database,exp,query)
    #SQL like search e.g. st=39 AND aroC=209
    elif query_type== "summary_breakdown":
        return dbhandle[database].get_st_breakdown(exp, field="st")
    else:
        return __nserv_q(database,exp,query) 
    
def process_medium_scheme_query(database,exp,query,query_type,extra_data=None):
    '''Same as process_small_scheme_query_nserv except the parameter extra data can be a string of comma delimited
       loci whose allele values will be returned  as well as ST,eBG etc
    '''      
    if query_type== "assembly_ids":
        return __get_st_info(database,exp,query,alleles=extra_data)
    elif query_type== "st_match":
        vals =  query.split(":")
        if vals[0]=="alleles":
            return dbhandle[database].get_sts_by_alleles(exp,0,alleles=vals[1])
        else:
            matches= vals[2]
            st = vals[1]  
            return __nserv_get_st_matches(database,exp,st,matches,alleles=extra_data)
    elif query_type== "summary_breakdown":
        return dbhandle[database].get_st_breakdown(exp,field="st")    
        
    return __nserv_q(database,exp,query,alleles=extra_data)



def get_assembly_stats(database,exp,query,query_type,extra_data=None):
    '''Returns the assembly stats in the 
          
          Args:
              same as a generic query
              
          Returns:
               In addition to standard fields, if an assembly has failed, the record will contain
               the reason(s) in 'other_data'
              
    '''     
    if query_type== "assembly_ids":
        query = "id IN (%s)" % query
    elif query_type == 'summary_breakdown':
        return dbhandle[database].get_assembly_breakdown()
    return dbhandle[database].get_assembly_stats(query)


def process_custom_view_query(database,exp,query,query_type,extra_data=None):
    '''This method will only query custom columns. Therefore if query_type is 'query' the 
    query must only conatain SQL with custom columns e.g. "6762" = 'blue' AND "5666" = 'red'
    
      
      Args:
          Standard arguments except
          * **exp**:  this is not used, should be none
          * **extra data**: either the id of the custom view or a dictionary describing the custom view. 
          This way a custom view can be created on the fly
    
      Returns:
          For experimental fields, the field name in the returned dictionary will be *name_scheme*, in order to prevent clashes
          between different schemes e.g ST for rMLST would be st_rMLST   
    '''     
    try:
        custom_view_id=int(extra_data)
        view = db.session.query(UserPreferences).filter_by(id=custom_view_id).one()
        view_data= ujson.loads(view.data) 
    except:
        view_data=extra_data
    all_exp_data={}
    all_aids=set() 
    custom_columns = view_data.get("custom_columns")
    exp_columns = view_data.get("experiment_columns")
    if query_type=="assembly_ids":
        
       
        schemes={}
        if exp_columns:
            for col in exp_columns:
                columns = schemes.get(col['scheme'])
                if not columns:
                    columns=[]
                    schemes[col['scheme']]=columns
                columns.append(col)
     
        for scheme in schemes:
            alleles=[]   
            for column in schemes[scheme]:
                if not column.get("non_locus"):
                    alleles.append(column['name'])
            if len(alleles)==0:
                alleles=None
            exp_data,aids=  query_function_handle[database][scheme](database,scheme,query,"assembly_ids",extra_data=alleles)
            for aid in exp_data:
                all_aids.add(aid)
                existing_exp_data = all_exp_data.get(aid)
                if not existing_exp_data:
                    existing_exp_data={}
                    all_exp_data[aid]=existing_exp_data
                data = exp_data[aid]
                for column in schemes[scheme]:
                    field=column['name']
                    col_name = column['name']+"_"+column['scheme']
                    value = data.get(field)
                    if not value:   
                        if column['datatype']=='integer':
                            existing_exp_data[col_name]=0
                    else: 
                        existing_exp_data[col_name]=value
   
        if custom_columns and len(custom_columns)>0:
            fields=[]    
            for item in custom_columns:
                name =str(item['id'])
                fields.append("custom_columns->>'%s' AS \"%s\"" %(name,name))         
            sql = "SELECT %s,best_assembly FROM strains WHERE best_assembly IN (%s)" %(",".join(fields),query)
            results =dbhandle[database].execute_query(sql)
            for result in results:
                aid = result['best_assembly']
                all_aids.add(aid)
                existing_exp_data = all_exp_data.get(aid)
                if not existing_exp_data:
                        existing_exp_data={}
                        all_exp_data[aid]=existing_exp_data
                del result['best_assembly']
                for field in result:
                    if result[field]:
                        existing_exp_data[field]=result[field]
    else:
        if not custom_columns or len(custom_columns)==0:
            return {},[]
        fields = []
        for column in custom_columns:
            name = str(column['id'])
            query = query.replace("\"%s\"" % name," custom_columns->>'%s' " % name  )
            fields.append("custom_columns->>'%s' AS \"%s\"" % (name,name))
        sql = "SELECT %s,best_assembly FROM strains WHERE %s" % (",".join(fields),query)
        results =dbhandle[database].execute_query(sql)
        for result in results:
            aid = result['best_assembly']
            if not aid:
                continue
            all_aids.add(aid)
            existing_exp_data = all_exp_data.get(aid)
            if not existing_exp_data:
                    existing_exp_data={}
                    all_exp_data[aid]=existing_exp_data
            del result['best_assembly']
            for field in result:
                if result[field]:
                    existing_exp_data[field]=result[field]            
        ass_list = map(str, all_aids)
        ass_list = ",".join(ass_list)
        exp_columns = view_data.get("experiment_columns")
        schemes={}
        if exp_columns:
            for col in exp_columns:
                columns = schemes.get(col['scheme'])
                if not columns:
                    columns=[]
                    schemes[col['scheme']]=columns
                columns.append(col)
        if len(ass_list)==0:
            return {},[]
     
        for scheme in schemes:
            alleles=[]   
            for column in schemes[scheme]:
                if not column.get("non_locus"):
                    alleles.append(column['name'])
            if len(alleles)==0:
                alleles=None
            exp_data,aids=  query_function_handle[database][scheme](database,scheme,ass_list,"assembly_ids",extra_data=alleles)
            for aid in exp_data:
                all_aids.add(aid)
                existing_exp_data = all_exp_data.get(aid)
                if not existing_exp_data:
                    existing_exp_data={}
                    all_exp_data[aid]=existing_exp_data
                data = exp_data[aid]
                for column in schemes[scheme]:
                    field=column['name']
                    col_name = column['name']+"_"+column['scheme']
                    value = data.get(field)
                    if not value:   
                        if column['datatype']=='integer':
                            existing_exp_data[col_name]=0
                    else: 
                        existing_exp_data[col_name]=value        
                
    return all_exp_data,list(all_aids)


    


def CRISPR_query(database,exp,query,query_type,extra_data=None):
    if query_type== "assembly_ids":
        barcode_to_aid= dbhandle[database].get_barcodes_from_aids(query,exp)
        barcodes = "','".join(barcode_to_aid.keys())
        if not barcodes:
            return {},[]
        query = "barcode IN ('%s')" % barcodes
    elif query_type== "summary_breakdown":
        return dbhandle[database].get_st_breakdown(exp)
    else:
        query= query.replace("\"","")
        query= query.replace("="," = ")
   
    nserv_db = app.config['DB_CODES'][database][0].title()
    url ="%s/search.api/%s/%s/types" % ( app.config['NSERV_ADDRESS'],nserv_db,exp)
    resp = requests.post(url=url,data={"filter":query},timeout=app.config['NSERV_TIMEOUT'])
    try:
        data = ujson.loads(resp.text)
    except Exception as e:
        raise Exception("Could not load server response (%s), error message %s"%(resp.text, e.message))
    aid_to_rec={}
          
    for rec in data:
        barcode= rec['barcode']
        if exp=='CRISPR':
            crispr1=False
            crispr1_list = []
            crispr2=False
            crispr2_list = []              
            blocks = rec['blocks']
            for block in blocks:
                if block['block_type'] == 'locus' and block['block_id']==1:
                    crispr1 = True
                    continue
                if block['block_type'] == 'locus' and block['block_id']==2:
                    crispr2 = True
                    crispr1 = False
                    continue                
                if crispr1:
                    crispr1_list.append(block['block_type'][0:2]+str(block['block_id']))
                if crispr2:
                    crispr2_list.append(block['block_type'][0:2]+str(block['block_id']))
            
            rec_dict = {'extra_row_info':barcode,
                        'CRISPR1':"-".join(crispr1_list),
                        'CRISPR2':"-".join(crispr2_list),
                        'CRISPR_type':rec['CRISPR_type']
                        }
        elif exp=='CRISPOL':
            spacers= rec['spacers']
            spac = []
            for space in spacers:
                spac.append(space.get('oligo') or 'ND')
            try:
                rec_name = rec['info']['name']
            except:
                rec_name='ND'
            rec_dict = {'extra_row_info':barcode,
                        'spacers':"-".join(spac),
                        'name':rec_name,
                        'CRISPOL_type':rec['CRISPOL_type']
                       }
            
                
        #get the assembly ids associated with this barcode    
        aids  = barcode_to_aid[barcode]
        for aid in aids:
            aid_to_rec[aid]=dict(rec_dict)
    #stat_dict = {'FAILED':-1,'QUEUED':0}
    #for status in stat_dict:
    aids = barcode_to_aid.get('COMPLETE',[])
    for aid in aids:
        rec_dict={exp+"_type":-1}       
        aid_to_rec[aid]=rec_dict
            
            
        
    return aid_to_rec,query.split(",")



def process_cluster_query(database,exp,query,query_type,extra_data=None):
    
    dbase = dbhandle[database]
    Schemes=dbase.models.Schemes
    scheme = dbase.session.query(Schemes).filter_by(description=exp).first()
    base_scheme_id = scheme.param['base_scheme_id']
    DataParam = dbase.models.DataParam
  
   
    try:
        dp_dict={}
        mm = np.memmap(scheme.param['numpy_file'],dtype='int')
        size= mm.size
        cols = scheme.param['table_width']
        rows = size/cols
        mm=mm.reshape(rows,cols)
        dps= dbase.session.query(DataParam).filter_by(tabname=exp).all()
        levels = []
        level_names=[]
        ret_dict={}
        ret_list=[]
        for item in dps:
            level = int(item.name[1:])
            level += 1
            level_names.append(item.name)

            levels.append(level)

        st_col = mm[:,0]        
        if query_type== "assembly_ids":
            #get all the st_ids
            sql = "SELECT assembly_id, (other_data#>>'{results,st_id}')::integer AS st FROM assembly_lookup WHERE scheme_id = %i AND assembly_id IN(%s)"  % (base_scheme_id,query)
            st_list=[]
            st_mapping={}
            results = dbase.execute_query(sql)
            for item in results:
                st = item.get('st')
                if not st:
                    continue
                aid_list=st_mapping.get(st)
                if not aid_list:
                    aid_list=[]
                    st_mapping[st]=aid_list
                aid_list.append(item['assembly_id'])
                st_list.append(st)
        
       
            #st_indexes = np.ravel([np.where(st_col==i)[0] for i in st_list])
            #st_indexes = [np.where(st_col==i )for i in st_list]
            sorter=np.argsort(st_col)
            sorted_indexes = np.searchsorted(st_col,st_list,sorter=sorter)
            non_existant = np.where(sorted_indexes==rows)[0]
            sorted_indexes= np.delete(sorted_indexes,non_existant)
            for index in sorted(non_existant,reverse=True):
                del st_list[index]            
            
            st_indexes=sorter[sorted_indexes]
            
          
            
            for b  in range(0,len(st_list)):
                #if len(st_indexes[b][0])==0:
                #    continue
                #index = st_indexes[b][0][0]
                index = st_indexes[b]
                row = mm[index,:]
                values = row[levels]
                record={}
                for a in range(0,len(values)):
                    record[str(level_names[a])]=values[a]
                aids=st_mapping[st_list[b]]
                for aid in aids:
                    ret_dict[aid]=dict(record)
                    ret_list.append(aid)

        else:
            query = query.replace("'","").replace('"',"")
            arr = query.split("=")
            level = int(arr[0][1:])
            if exp != 'filtered_cluster_hierarchy':
                level += 1            
            if not arr[1] or not arr[1].isdigit():
                raise Exception("Error while getting value from arr: %s, which is determined from query: %s "%(arr, query))
            value =int (arr[1])
            col = mm[:,level]
            indexes = np.where(col==value)[0]
            st_to_record={}
            sts=[]
            
            for index in indexes:
                row = mm[index,:]
                values = row[levels]
                record={}
                for a in range(0,len(values)):
                    record[str(level_names[a])]=values[a]
                st_to_record[row[0]]=record
                sts.append(str(row[0]))
            sts= ",".join(sts)
            sql = "SELECT assembly_id AS aid,  (other_data#>>'{results,st_id}')::integer  AS st  FROM assembly_lookup WHERE scheme_id = %i AND   (other_data#>>'{results,st_id}')::integer  IN (%s)" % (base_scheme_id,sts)
            data = dbase.execute_query(sql)
            
            for item in data:
                ret_list.append(item['aid'])
                ret_dict[item['aid']]=dict(st_to_record[item['st']])

    except Exception as e:
        app.logger.exception("unable to process generic job query. error message: %s "%e.message)
        raise
    return ret_dict,ret_list



  
def process_generic_query(database,exp,query,query_type,extra_data=None):
    '''This method assumes that the data for the scheme is in a dictionary in the assembly lookup table
    in the other_data column under ther key 'results'.    
        Args:
             The same as all the query methods
             
        Returns: 
             The same as all query methods
            
    '''    
    if query_type== "summary_breakdown":
        return dbhandle[database].get_generic_scheme_breakdown(exp,query)
    
    dbase = dbhandle[database]
    Schemes=dbase.models.Schemes
    DataParam = dbase.models.DataParam
    r_dict={}
    r_list=[]
    try:
        if query_type== "assembly_ids":
                query = "assembly_id IN (%s)" % query
        dp_dict={}
        dps= dbase.session.query(DataParam).filter_by(tabname=exp).all()
        for item in dps:
            name = item.name      
            query = query.replace('"'+name+'"',"other_data#>>'{results,%s}'"%name)
        
        scheme = dbase.session.query(Schemes).filter_by(description=exp).first()
        query = "SELECT assembly_id,other_data FROM assembly_lookup WHERE (%s) AND scheme_id =%i AND status='COMPLETE'" % (query,int(scheme.id))
        results = dbase.execute_query(query)
        
        
        for record in results:
            data= record['other_data'].get('results')
            if not data:
                continue
            r_dict[record['assembly_id']]=data
            r_list.append(record['assembly_id'])
    except Exception as e:
        app.logger.exception("unable tp process generic job query ")
        raise
    return r_dict,r_list




def process_project_info_query(database,exp,query,query_type,extra_data=None):
    '''A toy method to demonstrate how to write a query methods. The arguments and
    return values are standard
    '''    
    search_url= "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"
    summary_url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
    study_to_aid={}
    return_data={}
    return_list=[]
    
    #need to get project information from the assembly_ids supplied
    if query_type== "assembly_ids":
        #get all the study accessions from the strain database and link them to assembly id
        study_accessions=[]
        sql = 'SELECT study_accession AS  sa, best_assembly AS aid FROM strains WHERE best_assembly in (%s)' % query
        results = dbhandle[database].execute_query(sql)
        for item in results:
            study= item['sa']
            if not study:
                continue
            study_accessions.append(study)
            aids = study_to_aid.get(study)
            if not aids:
                aids=[]
                study_to_aid[study]=aids
            aids.append(item['aid'])
        #params for ncbi search are just all the project accessions
        params={"db":"bioproject","term":" OR ".join(study_accessions),"retmode":"json","retmax":1000}
        
    #need to search ncbi projects based on query and retreive any relevant records in Enterobase 
    else: 
        #Try and change the query into an api call to ncbi. The query is in SQL like text, so this can be a bit
        #tricky. Here , simply took the third word, removed any punctuation and used this as the term to
        #search ncbi
        arr=query.split()
        term = arr[2].replace("'","").replace("%","")
        #Another hack to narrow down search results - this should have been a value from the current database,
        #but since this example is in the test MIU database, this is not possible
        term=term+" AND Salmonella"
        params={"db":"bioproject","term":term,"retmode":"json"}
    
    #actually do the search and retreive record ids
    results = requests.post(search_url,data=params)
    data = ujson.loads(results.text)
    ids =data['esearchresult']['idlist']
    
    #use the ids to actually get the data
    params={"db":"bioproject","retmode":"json","id":",".join(ids),"retmax":1000}
    results=requests.post(summary_url,data=params)
    data=ujson.loads(results.text)
    
    #for all the project info, link it to assembly id using project id and the study_to_aid dictionary
    if query_type=='assembly_ids':
        for uid in data['result']:
            if uid == 'uids':
                continue
            info =data['result'][uid]
            aids =study_to_aid[info['project_acc']]
            for aid in aids:
                return_data[aid]={"project_name":info["project_name"],"project_description":info['project_description']}
                return_list.append(aid)
                
    # need to link the project  info obtained in the search to assembly ids
    else:
        project_to_info={}
        project_list=[]
        #go through returned data and get all project accessions and link them to project info
        for uid in data['result']:
            if uid == 'uids':
                continue
            info =data['result'][uid]
            project_list.append(info['project_acc'])
            project_to_info[info['project_acc']] ={"project_name":info["project_name"],"project_description":info['project_description']}
        #search strain database for the project accessions and get assembly ids
        projects= "('"+"','".join(project_list)+"')"
        sql= "SELECT study_accession AS  sa, best_assembly AS aid FROM strains WHERE study_accession in %s" % projects
        results = dbhandle[database].execute_query(sql)
        #link project info to assembly ids using the project to info dictionary
        for item in results: 
            return_data[item['aid']]=dict(project_to_info[item['sa']])
            return_list.append(item['aid'])
            
                
    return return_data,return_list
           
        
#Hard coded query methods - used for testing (the production server has no knowledge of them)
query_function_handle={}
""" A dictionary for each database which contains the experiment name poiinting to
the method e.g to query rMLST in ecoli
``
query_function_handle['ecoli']['rMLST']()
``
"""


#Query methods are pulled from the scheme table
#if the app is run out of context e.g. sphinx, the config has not been initialised
if app.config.get('ACTIVE_DATABASES'):

    for key in app.config['ACTIVE_DATABASES']:
        
        if app.config['ACTIVE_DATABASES'][key][3] == 0:
            continue
        dbase=dbhandle[key]
        query_function_handle[key]={}
        #hard code query functions for all databases
        query_function_handle[key]['assembly_stats']=get_assembly_stats
        query_function_handle[key]["custom_view"]=process_custom_view_query
        query_function_handle[key]["cluster_hierarchy"]=process_cluster_query
        query_function_handle[key]["filtered_cluster_hierarchy"]=process_cluster_query
        query_function_handle[key]["project_info"]=process_project_info_query  
        try:
            if not hasattr(dbase.models,"Schemes"):
                continue
            try: 
                Schemes = dbase.models.Schemes
                schemes =dbase.session.query(Schemes.param,Schemes.description).all()
            except:
                continue
            for scheme in schemes:
                param = scheme.param
                if param:
                    query_method = (param).get("query_method")
                    #query method specified and is not already hard coded
                    if query_method and not query_function_handle[key].get(scheme.description):
                        #does the method exist (maybe only in development version)
                        if hasattr(sys.modules[__name__],query_method):
                            query_function_handle[key][scheme.description]=getattr(sys.modules[__name__],query_method)
            dbase.session.close()
        except Exception as e:
            app.logger.exception("Problem with database:-%s" % (key))
            dbase.session.rollback()
            dbase.session.close()


