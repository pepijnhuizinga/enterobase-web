Presentations and Lectures about EnteroBase
==============================

ASM2018
-------

* **Poster:** `Understanding genomic landscapes in EnteroBase with cgMLST & GrapeTree <https://bitbucket.org/enterobase/enterobase-web/raw/efedf5458c047a69d488b37f44332084e61f3d9b/docs/images/asm2018_poster.pdf>`_


5th ASM Conference on *Salmonella*
----------------------------------

* **Presentation:** `Genomic population structure of Salmonella <https://figshare.com/articles/ASM_Salmonella_5th_by_zhemin/4004745>`_
* **Poster:** `Using MLST to decipher the population structure of Salmonella enterica <https://figshare.com/articles/5th_ASM_Sal_Genomic_population_structure_of_Salmonella_enterica_by_Zhemin/4004748>`_


EnteroBase Training 2017
------------------------
* **Intermediate**
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 70%; height: auto;">
        <iframe src="//www.youtube.com/embed/sROP9eeSXpk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 70%; height: 50%;"></iframe>
    </div>



* **Advanced**
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 70%; height: auto;">
        <iframe src="//www.youtube.com/embed/8pyHK5TIwKw" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 70%; height: 50%;"></iframe>
    </div>

