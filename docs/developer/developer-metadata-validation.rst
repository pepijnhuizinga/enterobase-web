Metadata Validation
===================

Fields for strains (and experimental data) are specified in the data_param table. Each field can have the following columns

* **tabname** strains (for experimental data, should be the name of the scheme)
* **name** The progmatic name of the filed (used in the the python and JavaScript code)
* **sra_field** The path in the json data returned from CRobort which corresponds to the field. e.g. For Source Niche *Sample,Metadata,Source Niche* would retrieve the Environment from the the following data

.. code-block:: json

    {
        
        "Sample":{
           "Metadata":{
                 "Source Niche":"Environment",
                  ........,
            },
            .......
        },
        ......
    }


* **mlst_field** Obsolete (the name of the field in the legacy MLST database)
* **display_order** The order of the column in the grid (can be negative)
* **nested_order** 0 for normal fields. If anything else then this will be displayed as a compound field e.g. continent, country etc. The number is the order in the compound field
* **label** The label that will be displayed to the user
* **datatype** Describes how the datatype will be validated. Can be 
    * text
    * combo - only values specified in *vals* are allowed
    * suggest - values in *vals* will be suggested but not enforced
    * integer
    * double
    * custom - validation is complex and is carried out by a method in the grid (see below)
* **min_value** If the datatype is an integer/double than this is the minimum value allowed
* **max_value** If the datatype is an integer/double than this is the maximum value allowed
* **required** 1 if a value has to given for the field, otherwise 0. If it is not essential but is present, it must still conform to any validation criteria
* **default_value** If no value is present, then this value will be entered into the grid
* **allow_pattern** If the field is text then it will have to conform to this Standard pattern matching
* **description** A brief description of the field
* **vals** A lit of values (separated by |), which are either enforced (datatype is combo) or suggested (datatype is suggest) 
* **max_length** The maximum length (in characters) of the value
* **allow_multiple_values** If 1 then more than one value for a field is allowed. e.g Accession can have more than one value since many reads can be associated with the same strain.
* **no_duplicates** Not actually enforced

Adding a Custom Validator
-------------------------

The method accepts a function which takes the column name and row_id . From this the value of the cell can be obtained (or other values in the row). The appropriate message (if validation fails) or empty an string should be passed on to the updateErrors() method which will return true of false

.. code-block:: javascript

    grid.addCustomCellValidator("column_name",function(col_name,row_id){
        var col_index = grid.getColumnIndex(col_name);
        var row_index = grid.getRowIndex(row_id)
        var value = grid.getValueAt(row_index,col_index);
        var msg ="";
        //validate value - change msg to reason failed if appropriate
        return grid.updateErrors(colName,rowID,msg)
    
    });
