Using the Browser
=================

The front-end consists of a browser based on JScript, which can upload
metadata and short reads via HTTP5. The front-end also includes
EnteroUploader, a python-based executable that uses the S3 protocol for
faster uploading. We are also planning to implement a RESTful interface
for accessing an API by programmers.

The browser contains four panes: Methods, Metadata, Experimental data,
and Information. a left pane for metadata and a right pane for
experimental data. The width of columns and of the individual panes can
be changed dragging the borders. Clicking on the greyed area of the
central border between Metadata and Experimental Data, closes
Experimental Data. Clicking on the left or right borders closes the side
panes (Left: Methods; Right: Information).Clicking a second time opens
the closed panes.

### Methods Pane

Methods is intended to provide access to all available methods.
Currently it only redirects to either Strains->Browse / [Edit Strain
MetaData] or Add Data -> [Upload Reads] / [Metadata for Uploaded
Reads].

### Information Pane

Information pane contains both the overview of a database, and the data
and tasks that were associated with a user.

-   Overview:

Pie-charts of the proportions of strains from different countries or
institutes.

-   User Data

The number of strains that have been uploaded from the user that is
logged in.

-   User Tasks

The number of running or complete tasks that are associated with the
user.

### Select Column

Multiple rows can be selected for
bulk processing if the leftmost checkbox is ticked. A right click within
the browser offers the option of selecting or deselecting all rows from
the current query. Multiple rows can also be selected/unselected by
first selecting one row, and then a shift-click in a different row. Selected rows
are the target for assembling multiple genomes or downloading multiple
assemblies. By right clicking, a context menu will allow you to
select/unselect all rows.

### Edit Column

This column shows whether you have editing rights on the row. A pencil
icon means that the row can be edited either directly by clicking onto a
cell or by clicking on the pencil icon and editing all the records at
once, A red background shows that the row contains errors and a yellow
background indicates changes have been made in the row

  [Edit Strain MetaData]: Edit_strain_metadata
  [Upload Reads]: Upload_Reads#Upload_Reads
  [Metadata for Uploaded Reads]: Upload_Reads#Associate_Uploaded_Reads_with_Metadata