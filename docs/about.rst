About EnteroBase
================
**A Powerful, User-Friendly Online Resource for Analyzing and Visualizing Genomic 
Variation within Enteric Bacteria**

EnteroBase aims to establish a world-class, one-stop, user-friendly, 
backwards-compatible but forward-looking genome database, Enterobase --
together with a set of web-based tools, EnteroBase Backend Pipeline --
to enable bacteriologists to identify, analyse, quantify and visualise 
genomic variation principally within the genera: 

* `Salmonella <http://enterobase.warwick.ac.uk/species/index/senterica>`_
* `Escherichia/Shigella <http://enterobase.warwick.ac.uk/species/index/ecoli>`_
* `Clostridioides <http://enterobase.warwick.ac.uk/species/index/clostridium>`_
* `Vibrio <http://enterobase.warwick.ac.uk/species/index/vibrio>`_
* `Yersinia <http://enterobase.warwick.ac.uk/species/index/yersinia>`_
* `Helicobacter <http://enterobase.warwick.ac.uk/species/index/helicobacter>`_
* `Moraxella <http://enterobase.warwick.ac.uk/species/index/mcatarrhalis>`_

General Description
-------------------
EnteroBase_ is strain based. Each strain is associated with metadata and
genomic assemblies, as well as with deduced genotyping data. All
assemblies are performed *de novo* from Illumina/PacBio reads using a
standardised, versioned pipeline. Unless explicitly chosen, only
assemblies that match pre-defined criteria described in :doc:`/pipelines/backend-pipeline-qaevaluation` 
are displayed, and where multiple assemblies are associated with a strain, only 
the best assembly according to :doc:`/pipelines/backend-pipeline-qaevaluation` 
is displayed.

Genotyping data is deduced exclusively from assemblies. MLST data is
called by BLASTN_ and USEARCH_ against a dataset of allelic sequences that 
differ from each other by at least 2.5%. Other genotyping methods are under
development. Genotyping data is summarised in the Experimental Data
pane. The full data including assemblies can be downloaded freely (but
see **Fair Usage**)

All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. 
An explanation of this method is found in :doc:`/pipelines/backend-pipeline-qassembly`.

For a general description of the in silico typing method, see :doc:`/pipelines/backend-pipeline-nomenclature`. 
For details about the application of these methods for each species:

* :doc:`Salmonella <pipelines/salmonella-statistics>`
* :doc:`Escherichia <pipelines/escherichia-statistics>`
* :doc:`Yersinia <pipelines/yersinia-statistics>`
* :doc:`Moraxella <pipelines/moraxella-statistics>`

Fair Usage
----------
All public metadata, assemblies and genotyping data can be freely downloaded
for academic purposes. In order to allow users who upload unpublished
data sufficient time to perform their own analyses, Prior to the release date,
we restrict downloads of their
genomes, metadata, and genotypes to the owner who uploaded the data, or to their
self-declared 'buddies', as well as to curators and administrators. Other users
can see these data in the workspace browser or use them to generate a tree. However,
that tree cannot be used for publication by anybody who does not have explicit rights.

We would also consider it fair usage that users who wish to analyse very large
amounts of the data stored in EnteroBase also contribute software tools
to EnteroBase that facilitate the presentation and analysis of their
results. Downloading and analyses of data by commercial enterprises can
only be performed after explicit permission by the administrators, which
may involve legal agreements regarding material transfer.

Please see the :doc:`full terms of use for this web service </enterobase-terms-of-use>`.

Data Privacy
------------
EnteroBase users are encouraged to upload their own reads to the website, which 
will be assembled and genotyped like existing public data. Submitters should note
that raw data (sequence reads) will never be made public through the website to 
other users. The genome assembly will only be accessible to the data submitter
and their buddies for 6 months after uploads. Assembly data will then be made 
public, longer release dates can be negotiated by contacting us on 
enterobase@warwick.ac.uk. 
Genotyping results i.e. MLST, ribosomal MLST, core genome MLST, in silico 
serotyping, will be made public as soon as the uploaded data has been processed.
User passwords on the website are encrypted and no one, including administrators,
can easily access them. However, we would advise you NOT to use the same password
you would use for important accounts, such as internet banking.

Please see the :doc:`full terms of use for this web service </enterobase-terms-of-use>`.

Fund
-------
EnteroBase is funded by BBSRC research grant `BB/L020319/1 <http://gtr.rcuk.ac.uk/projects?ref=BB%2FL020319%2F1>`_.

Citation
--------
**If you use data/metadata from the website, or the analysis based on these
data, please cite EnteroBase as:**

 Alikhan NF, Zhou Z,Sergeant MJ, Achtman M (2018) "A genomic overview of the
 population structure of Salmonella." PLoS Genet 14 (4): e1007261.
 https://doi.org/10.1371/journal.pgen.1007261

**If you use GrapeTree please cite:**

 Zhou Z, Alikhan NF, Sergeant MJ, Luhmann N, Vaz C, Francisco AP, Carrico JA, 
 Achtman M (2018) "GrapeTree: Visualization of core genomic relationships 
 among 100,000 bacterial pathogens", Genome Research; https://doi.org/10.1101/gr.232397.117


3rd Party acknowledgements
--------------------------
If you use data generated by 3rd party tools in EnteroBase, please cite both 
EnteroBase and the paper describing the specific tool. 

* **rMLST** is Copyright 2010-2016, University of Oxford. rMLST is described in: `Jolley et al. 2012 Microbiology 158:1005-15`_.
* **Serovar predictions (SISTR)** have been calculated using the pipeline 
  developed by the `SISTR team`_ and is described in  `Yoshida et al. 2016 PLoS ONE 11(1): e0147101`_
* **ClermonTyping** is described in `Beghain et al. 2018 M Gen 4(7) doi:10.1099/mgen.0.000192`_
* **EzClermont** is described in: `Waters et al. 2018 BioRxiv 317610`_ 
* **FimTyper** is described in: `Roer et al. 2017 J.Clin.Microbiol. 2017; 55 (8):2538-2543`_ 

.. _`SISTR team`: https://lfz.corefacility.ca/sistr-app/
.. _`Yoshida et al. 2016 PLoS ONE 11(1): e0147101`: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518
.. _`Beghain et al. 2018 M Gen 4(7) doi:10.1099/mgen.0.000192`:  https://dx.doi.org/10.1099/mgen.0.000192
.. _`Waters et al. 2018 BioRxiv 317610`: https://doi.org/10.1101/317610
.. _`Roer et al. 2017 J.Clin.Microbiol. 2017; 55 (8):2538-2543`: https://doi.org/10.1128/JCM.00737-17

.. _EnteroBase: http://enterobase.warwick.ac.uk
.. _BLASTN: https://www.ncbi.nlm.nih.gov/pubmed/20003500
.. _USEARCH: https://www.ncbi.nlm.nih.gov/pubmed/20709691