GrapeTree Documentation
=======================
GrapeTree is a fully interactive, tree visualization program within EnteroBase,
which supports facile manipulations of both tree layout and metadata.
It generates GrapeTree figures using the neighbor-joining (NJ) algorithm,
the classical minimal spanning tree algorithm (MSTree) similar to
`PhyloViz <http://www.phyloviz.net/>`_, or an improved minimal spanning tree 
algorithm which we call MSTree V2. 

* GrapeTree is also available as a  `stand-alone version <https://github.com/achtman-lab/GrapeTree/releases>`_.
* :doc:`Installation instructions <grapetree-installing-stand-alone>`, :doc:`Manuals <grapetree-manual>` and :doc:`Tutorials <grapetree-tutorial-1>` are available on this site
* The source code for GrapeTree is `available <https://github.com/achtman-lab/GrapeTree>`_
* GrapeTree is also available as a `live online demo <https://achtman-lab.github.io/GrapeTree/MSTree_holder.html>`_

Here are materials to help you use GrapeTree:

.. toctree::
    :maxdepth: 1
    
    grapetree-installing-stand-alone
    grapetree-tutorial-1
    grapetree-tutorial-1-sa
    grapetree-api
    grapetree-manual

About GrapeTree 
---------------
GrapeTree is named after the clusters of related bacterial strains that tends 
to be presented in minimal spanning trees. Our GrapeTree GUI is available 
within EnteroBase once you have created a workspace or connected to somebody 
else's workspace. 
fire
It is also `available here <https://achtman-lab.github.io/GrapeTree/>`_
as a stand-alone version. The integrated EnteroBase version interacts directly 
with EnteroBase data. The stand alone version calculates trees from
character data, visualizes pre-calculated trees and annotates them with
information from supplied metadata.

GrapeTree aims to address two central issues:

* It is difficult to infer clusters from classical phylograms when showing
  large numbers of taxa.
* Tree construction methods based on Multi Locus Sequence Type (MLST) data (and
  extensions such as core genome MLST) do not correctly handle missing alleic
  information (See MSTreeV2 below).

GrapeTree is easy to integrate into existing web services. Since the release
of our source code, Keith Jolley has written a GrapeTree wrapper specific for
the `BigsDB website/database
<http://bigsdb.readthedocs.io/en/latest/data_analysis.html#grapetree>`_
environment, and thereby enabled GrapeTree functionality for all the
databases served by PubMLST.

About MSTreeV2
--------------
MSTree V2 is a novel minimum spanning tree which is better suited for handling
missing data than are classical MSTrees. First, a directed minimal
spanning arborescence (dMST) (`Edmond's algorithm <https://en.wikipedia.org/wiki/Edmonds%27_algorithm>`_)
is calculated from asymmetric (directional) distances with tiebreaking of
coequal branches based on allelic distances from a harmonic mean. Local branch
recrafting is subsequently performed to eliminate the spurious branches that
can arise within minimum spanning trees.

Citation
--------
If you use GrapeTree please cite the publication in Genome Research (in press):

Z Zhou, NF Alikhan, MJ Sergeant, N Luhmann, C Vaz, AP Francisco, JA Carrico,
M Achtman (2018) "GrapeTree: Visualization of core genomic relationships among 
100,000 bacterial pathogens", Genome Res. doi:
[https://doi.org/10.1101/gr.232397.117](https://doi.org/10.1101/gr.232397.117)
