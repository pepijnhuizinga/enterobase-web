EnteroBase
==========
**A Powerful, User-Friendly Online Resource for Analyzing and Visualizing Genomic 
Variation within Enteric Bacteria**

Gallery
-------
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| | GrapeTree of cgMLST (3,002 loci) from 1,082                           | | Core SNP phylogeny of ancient (red) and                                                     |
| | *Salmonella enterica* serovar Agona genomes                           | | modern genomes of Para C Lineage in                                                         |
| | in EnteroBase                                                         | | *Salmonella eneterica*                                                                      |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| |Agona|_                                                                | |ParaC|_                                                                                      |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| `Alikhan et al. 2018`_                                                  | `Zhou et al. 2018a`_                                                                          |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
|                                                                         |                                                                                               |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| | Visualization of 3,902 legacy *Salmonella*                            |                                                                                               | 
| | MLST STs from 99,722 genomic assemblies                               |                                                                                               |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| |7geneMLST|_                                                            |                                                                                               |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+
| `Zhou et al. 2018b`_                                                    |                                                                                               |
+-------------------------------------------------------------------------+-----------------------------------------------------------------------------------------------+

.. _`Alikhan et al. 2018`: https://doi.org/10.1371/journal.pgen.1007261
.. _`Zhou et al. 2018a`: https://www.ncbi.nlm.nih.gov/pubmed/30033331
.. _`Zhou et al. 2018b`: https://www.ncbi.nlm.nih.gov/pubmed/30049790

.. |Agona| image:: https://bytebucket.org/enterobase/enterobase-web/raw/a67b2769ab0deb12dda2138e58b6b9f434531380/docs/images/Agona.PNG
   :width: 310px

.. _Agona : http://enterobase.warwick.ac.uk/ms_tree?tree_id=9946
.. |ParaC| image:: https://bytebucket.org/enterobase/enterobase-web/raw/a67b2769ab0deb12dda2138e58b6b9f434531380/docs/images/ParaC.png
   :width: 310px

.. _ParaC : http://enterobase.warwick.ac.uk/ms_tree?tree_id=13495
.. |7geneMLST| image:: https://bytebucket.org/enterobase/enterobase-web/raw/09ce9e87e488645ef1c0d97a622eefaee8109385/docs/images/7geneMLST.PNG
   :width: 310px

.. _7geneMLST : http://enterobase.warwick.ac.uk/ms_tree?tree_id=6168

Databases
---------
EnteroBase aims to establish a world-class, one-stop, user-friendly, 
backwards-compatible but forward-looking genome database, Enterobase --
together with a set of web-based tools, EnteroBase Backend Pipeline --
to enable bacteriologists to identify, analyse, quantify and visualise 
genomic variation principally within the genera: 

* `Salmonella <http://enterobase.warwick.ac.uk/species/index/senterica>`_
* `Escherichia/Shigella <http://enterobase.warwick.ac.uk/species/index/ecoli>`_
* `Clostridioides <http://enterobase.warwick.ac.uk/species/index/clostridium>`_
* `Vibrio <http://enterobase.warwick.ac.uk/species/index/vibrio>`_
* `Yersinia <http://enterobase.warwick.ac.uk/species/index/yersinia>`_
* `Helicobacter <http://enterobase.warwick.ac.uk/species/index/helicobacter>`_
* `Moraxella <http://enterobase.warwick.ac.uk/species/index/mcatarrhalis>`_

Misc
----
Click here to `Post an issue or bug about EnteroBase <https://bitbucket.org/enterobase/enterobase-web/issues?status=new&status=open>`_

Funded by BBSRC research grant `BB/L020319/1 <http://gtr.rcuk.ac.uk/projects?ref=BB%2FL020319%2F1>`_.

Citation
--------
**If you use data/metadata from the website, or the analysis based on these
data, please cite EnteroBase as:**

 Alikhan NF, Zhou Z,Sergeant MJ, Achtman M (2018) "A genomic overview of the
 population structure of Salmonella." PLoS Genet 14 (4): e1007261. 
 https://doi.org/10.1371/journal.pgen.1007261

**If you use GrapeTree please cite:**

 Zhou Z, Alikhan NF, Sergeant MJ, Luhmann N, Vaz C, Francisco AP, Carrico JA, 
 Achtman M (2018) "GrapeTree: Visualization of core genomic relationships 
 among 100,000 bacterial pathogens", Genome Research; https://doi.org/10.1101/gr.232397.117


3rd Party acknowledgements
--------------------------
If you use data generated by 3rd party tools in EnteroBase, please cite both 
EnteroBase and the paper describing the specific tool. 

* **rMLST** is Copyright 2010-2016, University of Oxford. rMLST is described in: `Jolley et al. 2012 Microbiology 158:1005-15`_.
* **Serovar predictions (SISTR)** have been calculated using the pipeline 
  developed by the `SISTR team`_ and is described in  `Yoshida et al. 2016 PLoS ONE 11(1): e0147101`_
* **ClermonTyping** is described in `Beghain et al. 2018 M Gen 4(7) doi:10.1099/mgen.0.000192`_
* **EzClermont** is described in: `Waters et al. 2018 BioRxiv 317610`_ 
* **FimTyper** is described in: `Roer et al. 2017 J.Clin.Microbiol. 2017; 55 (8):2538-2543`_ 

.. _`SISTR team`: https://lfz.corefacility.ca/sistr-app/
.. _`Yoshida et al. 2016 PLoS ONE 11(1): e0147101`: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518
.. _`Beghain et al. 2018 M Gen 4(7) doi:10.1099/mgen.0.000192`:  https://dx.doi.org/10.1099/mgen.0.000192
.. _`Waters et al. 2018 BioRxiv 317610`: https://doi.org/10.1101/317610
.. _`Roer et al. 2017 J.Clin.Microbiol. 2017; 55 (8):2538-2543`: https://doi.org/10.1128/JCM.00737-17


Click one of the links below to read more:

.. toctree::
    :maxdepth: 1
    
    about
    faq
    enterobase-terms-of-use
    GDPR
    register-login
    enterobase-tutorials/tutorials
    grapetree/grapetree-about
    features/enterobase-features
    api/about-the-api
    developer/developer-webdatabase
    developer/developer-nservdb
    pipelines/enterobase-pipelines
    pipelines/enterobase-schemes
    talks
    mlst/mlst-legacy-info
    developer/developer-getting-started