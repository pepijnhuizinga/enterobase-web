Details of assembly methods and in silico genotyping for *Clostridioides*
=========================================================================

**For a general description of the in silico typing method used to apply these schemes on NGS data, [please click here](EnteroBase%20Backend%20Pipeline%3A%20nomenclature).**

**All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method [please click here](EnteroBase%20Backend%20Pipeline%3A%20QAssembly).**

[TOC]


|    MLST – Classic                   |    Ribosomal MLST   (Jolley, 2012)               |    Core Genome MLST                            |    Whole Genome MLST                                |
|-----------------------------------------------------|--------------------------------------------------|------------------------------------------------|-----------------------------------------------------|
|    7 Loci                                           |    53 Loci                                       |    2,556 Loci                                  |    11,490 Loci                                      |
|    Conserved Housekeeping genes                     |    Ribosomal   proteins                          |    Core genes                                  |    Any coding sequence                              |
|    Highly conserved;      Low resolution            |    Highly conserved;      Medium   resolution    |    Variable;      High resolution              |    Highly   variable;      Extreme   resolution     |
|    Different scheme for each    species/genus       |    Single scheme across tree of life             |    Different scheme for each  species/genus    |    Different scheme for each    species/genus       |


## 7 Gene MLST 

The classic MLST scheme for Clostridioides is described in 
[Griffiths et al (2010) J. Clin. Microbiol. 48, 770-778](http://dx.doi.org/10.1128%2FJCM.01796-09 "external link").

Genes included in 7 gene MLST (together with the length of sequence used for MLST taken from table 1 in the above cited paper):

| Gene | Length |
|----- | ------ |
| adk  | 501 |
| atpA | 555 |
| dxr  | 411 |
| glyA | 516 |
| recA | 564 |
| sodA | 450 |
| tpi  | 504 |

## Ribosomal MLST (rMLST)

* **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is described in [Jolley et al. 2012 Microbiology 158:1005-15](http://www.ncbi.nlm.nih.gov/pubmed/22282518 "external link").
